//
//  CCNumber.m
//  ALC
//
//  Created by Anh Tran on 8/18/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "CCNumber.h"

@implementation CCNumber
@synthesize longDescription, shortCCNumber;
-(id)init:(NSString*) shortCC withDescription:(NSString*) longDesc{
    self = [super init];
    if(self){
        self.longDescription = longDesc;
        self.shortCCNumber = shortCC;
    }
    return self;
}

@end
