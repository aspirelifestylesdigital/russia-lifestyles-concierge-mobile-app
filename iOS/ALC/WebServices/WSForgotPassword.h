//
//  WSForgotPassword.h
//  ALC
//
//  Created by Anh Tran on 8/29/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSBase.h"

@interface WSForgotPassword : WSBase
-(void)forgotPassword:(NSString*) email;

@end
