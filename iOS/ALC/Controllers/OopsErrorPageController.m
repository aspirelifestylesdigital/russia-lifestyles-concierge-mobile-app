//
//  OopsErrorPageController.m
//  ALC
//
//  Created by Hai NguyenV on 11/10/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "OopsErrorPageController.h"

@interface OopsErrorPageController ()
@property (weak, nonatomic) IBOutlet UILabel *titleErrorLbl;
@property (weak, nonatomic) IBOutlet UIButton *okBtn;

@end

@implementation OopsErrorPageController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self customButtonBack];
    
    _lbErrorMsg.text = _contentError;
}

-(void)setTextForViews
{
    [self addTitleToHeader:NSLocalizedString(@"ERROR", nil) withColor:HEADER_COLOR];
    self.titleErrorLbl.text = NSLocalizedString(@"Oops.", @"");
    self.lbErrorMsg.text = NSLocalizedString(@"That email address does not exist in our records. Please check if you have entered it correctly.", @"");
    [self.okBtn setTitle:NSLocalizedString(@"OK", @"") forState:UIControlStateNormal];
}
//------- Show Concierge button -----------
//---- return NO if you don't want to show it ----
//---- return YES if you want to show it -----------------
-(BOOL)isShowConciergeButton{
    return NO;
}
//--------------------------
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)actionOK:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
