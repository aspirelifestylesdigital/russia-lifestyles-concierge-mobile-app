//
//  GourmetRecommendRequestObj.h
//  ALC
//
//  Created by Hai NguyenV on 10/17/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PhotoResizedInfo.h"
#import "GouetmetRecommendDetailsObj.h"
#import "BaseBookingRequestObject.h"
@interface GourmetRecommendRequestObj : BaseBookingRequestObject

@property (strong, nonatomic) NSMutableArray* Occasion;
@property (strong, nonatomic) NSMutableArray* Cuisines;
@property (strong, nonatomic) NSString* MaximumPrice;
@property (strong, nonatomic) NSString* reservationDate;
@property (strong, nonatomic) NSString* reservationTime;
@property (strong, nonatomic) NSString* BookingId;
@property (strong, nonatomic) NSString* MobileNumber;
@property (strong, nonatomic) NSString* EmailAddress;
@property (strong, nonatomic) NSString* BookingItemId;
@property (strong, nonatomic) NSString* Country;
@property (strong, nonatomic) NSString* City;
@property (strong, nonatomic) NSString* State;
@property (strong, nonatomic) NSString* foodAllergies;
@property (strong, nonatomic) NSString* SpecialRequirements;
@property (strong, nonatomic) NSString* reservationName;

@property ( nonatomic) BOOL isContactPhone;
@property ( nonatomic) BOOL isContactEmail;
@property ( nonatomic) BOOL isContactBoth;

@property (strong, nonatomic) NSString* EditType;

@property ( nonatomic) NSInteger adultPax;
@property ( nonatomic) NSInteger kidsPax;

@property (strong, nonatomic) PhotoResizedInfo* photo;
-(id) initFromDetails:(GouetmetRecommendDetailsObj*)detailsObj;
@end
