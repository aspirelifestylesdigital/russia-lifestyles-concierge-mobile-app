//
//  CarRentalTransferView.h
//  ALC
//
//  Created by Anh Tran on 10/7/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OptionalDetailsView.h"
#import "ErrorToolTip.h"
#import "CarCreateRequestObject.h"
#import "CarRentalDetailsObject.h"
#import "CarTransferDetailsObject.h"
@protocol CarRentalTransferViewDelegate <NSObject>

@optional
-(void) submitRequest:(CarCreateRequestObject*) requestObject forType:(enum REQUEST_TYPE) type;
@end

@interface CarRentalTransferView : UIView<UITextFieldDelegate,  UIPickerViewDelegate, OptionalDetailsViewDelegate,  SubFilterControllerDelegate,ErrorToolTipDelegate>
@property (strong, nonatomic) IBOutlet UIView *viewDriverName;
@property (strong, nonatomic) IBOutlet UIView *viewTransportType;
@property (strong, nonatomic) IBOutlet UIView *viewAgeOfDriver;
@property (strong, nonatomic) IBOutlet UIView *viewInternationalLiciense;
@property (strong, nonatomic) IBOutlet UIStackView *viewDropOffDate;
@property (strong, nonatomic) IBOutlet UIStackView *viewPickupDate;
@property (strong, nonatomic) IBOutlet UIStackView *viewPickupTranfer;
@property (strong, nonatomic) IBOutlet UIStackView *viewNumberPassengers;

@property (strong, nonatomic) IBOutlet UIView *view;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIButton *btSubmit;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pickupLocationTop;



@property (unsafe_unretained) id<CarRentalTransferViewDelegate> delegate;
@property (strong, nonatomic) NSString* bookingId;
-(void) applyRequestType:(enum REQUEST_TYPE) type;
-(void) showRentalDetails:(CarRentalDetailsObject*) rentalDetails;
-(void) showTransferDetails:(CarTransferDetailsObject*) transferDetails;
-(void) resetTimeValue;
-(void) setMyPreference:(NSDictionary*)dict;
-(void)closAllkeyboard;
@end
