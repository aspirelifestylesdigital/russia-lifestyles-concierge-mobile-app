//
//  HGWBaseRequest.m
//  HungryGoWhere
//
//  Created by Linh Le on 11/9/12.
//  Copyright (c) 2012 Linh Le. All rights reserved.
//

#import "HGWBaseRequest.h"

@implementation HGWBaseRequest

@synthesize contentType;
@synthesize requestUrl;
@synthesize dictKeyValues;

-(id) init
{
    self = [super init];
    return self;
}

-(void) dealloc
{
    
}

-(NSString*) requestString
{
    return nil;
}

-(NSString*) getSignature
{
    return nil;
}

@end
