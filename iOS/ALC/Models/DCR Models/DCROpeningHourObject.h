//
//  DCROpeningHourObject.h
//  ALC
//
//  Created by Anh Tran on 3/15/17.
//  Copyright © 2017 Sunrise Software Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DCROpeningHourObject : NSObject
@property (nonatomic, strong) NSString* openingFromDay;
@property (nonatomic, strong) NSString* openingToDay;
@property (nonatomic, strong) NSString* openingFromHour;
@property (nonatomic, strong) NSString* openingToHour;
-(id) initFromDict:(NSDictionary*) dict;
-(NSString*) fullValue;
@end
