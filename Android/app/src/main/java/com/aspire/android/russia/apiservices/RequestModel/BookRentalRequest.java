package com.aspire.android.russia.apiservices.RequestModel;


import com.google.gson.annotations.Expose;

import java.util.ArrayList;

public class BookRentalRequest
        extends BaseRequest {
    @Expose
    private String BookingId = "";
    @Expose
    private String UserID = "";
    @Expose
    private String UploadPhoto = "";
    @Expose
    private boolean Phone = false;
    @Expose
    private Boolean Email = false;
    @Expose
    private Boolean Both = false;
    @Expose
    private String MobileNumber = "";
    @Expose
    private String EmailAddress = "";
    @Expose
    private String DriverName = "";
    @Expose
    private String DriverAge = "";
    @Expose
    private Boolean InternationalLicense = false;
    @Expose
    private String EpochPickupDate = "";
    @Expose
    private String PickupTime = "";
    @Expose
    private String PickupLocation = "";
    @Expose
    private String EpochDropOffDate = "";
    @Expose
    private String DropOffTime = "";
    @Expose
    private String DropOffLocation = "";
    @Expose
    private ArrayList<String> PreferredVehicles;
    @Expose
    private ArrayList<String> PreferredCarRentals;
    @Expose
    private String MinimumPrice = "";
    @Expose
    private String MaximumPrice = "";
    @Expose
    private String SpecialRequirements = "";



    public String getDriverAge() {
        return DriverAge;
    }

    public void setDriverAge(final String driverAge) {
        DriverAge = driverAge;
    }
    public String getSpecialRequirements() {
        return SpecialRequirements;
    }

    public void setSpecialRequirements(final String specialRequirements) {
        SpecialRequirements = specialRequirements;
    }

    public String getBookingId() {
        return BookingId;
    }

    public void setBookingId(final String bookingId) {
        BookingId = bookingId;
    }

    public String getUserID() {
        return UserID;
    }

    public void setUserID(final String userID) {
        UserID = userID;
    }

    public String getDriverName() {
        return DriverName;
    }

    public void setDriverName(final String driverName) {
        DriverName = driverName;
    }

    public String getUploadPhoto() {
        return UploadPhoto;
    }

    public void setUploadPhoto(final String uploadPhoto) {
        UploadPhoto = uploadPhoto;
    }

    public Boolean getPhone() {
        return Phone;
    }

    public void setPhone(final Boolean phone) {
        Phone = phone;
    }

    public Boolean getEmail() {
        return Email;
    }

    public void setEmail(final Boolean email) {
        Email = email;
    }

    public Boolean getBoth() {
        return Both;
    }

    public void setBoth(final Boolean both) {
        Both = both;
    }

    public String getMobileNumber() {
        return MobileNumber;
    }

    public void setMobileNumber(final String mobileNumber) {
        MobileNumber = mobileNumber;
    }

    public String getEmailAddress() {
        return EmailAddress;
    }

    public void setEmailAddress(final String emailAddress) {
        EmailAddress = emailAddress;
    }

    public Boolean getInternationalLicense() {
        return InternationalLicense;
    }

    public void setInternationalLicense(final Boolean internationalLicense) {
        InternationalLicense = internationalLicense;
    }

    public String getEpochPickupDate() {
        return EpochPickupDate;
    }

    public void setEpochPickupDate(final String epochPickupDate) {
        EpochPickupDate = epochPickupDate;
    }

    public String getPickupTime() {
        return PickupTime;
    }

    public void setPickupTime(final String pickupTime) {
        PickupTime = pickupTime;
    }

    public String getPickupLocation() {
        return PickupLocation;
    }

    public void setPickupLocation(final String pickupLocation) {
        PickupLocation = pickupLocation;
    }

    public String getEpochDropOffDate() {
        return EpochDropOffDate;
    }

    public void setEpochDropOffDate(final String epochDropOffDate) {
        EpochDropOffDate = epochDropOffDate;
    }

    public String getDropOffTime() {
        return DropOffTime;
    }

    public void setDropOffTime(final String dropOffTime) {
        DropOffTime = dropOffTime;
    }

    public String getDropOffLocation() {
        return DropOffLocation;
    }

    public void setDropOffLocation(final String dropOffLocation) {
        DropOffLocation = dropOffLocation;
    }

    public ArrayList<String> getPreferredVehicles() {
        return PreferredVehicles;
    }

    public void setPreferredVehicles(final ArrayList<String> preferredVehicles) {
        PreferredVehicles = preferredVehicles;
    }

    public ArrayList<String> getPreferredCarRentals() {
        return PreferredCarRentals;
    }

    public void setPreferredCarRentals(final ArrayList<String> preferredCarRentals) {
        PreferredCarRentals = preferredCarRentals;
    }

    public String getMinimumPrice() {
        return MinimumPrice;
    }

    public void setMinimumPrice(final String minimumPrice) {
        MinimumPrice = minimumPrice;
    }

    public String getMaximumPrice() {
        return MaximumPrice;
    }

    public void setMaximumPrice(final String maximumPrice) {
        MaximumPrice = maximumPrice;
    }
}
