package com.aspire.android.russia.apiservices.RequestModel;


import com.google.gson.annotations.Expose;

import java.util.List;

public class UpdategGourmetRequest
        extends BaseRequest {
    @Expose
    private String UserID;
    @Expose
    private List<String> Cuisines;
    @Expose
    private String OtherCuisine;
    @Expose
    private List<String> LoyaltyPrograms;

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String userID) {
        UserID = userID;
    }

    public List<String> getCuisines() {
        return Cuisines;
    }

    public void setCuisines(List<String> cuisines) {
        Cuisines = cuisines;
    }

    public String getOtherCuisine() {
        return OtherCuisine;
    }

    public void setOtherCuisine(String otherCuisine) {
        OtherCuisine = otherCuisine;
    }

    public List<String> getLoyaltyPrograms() {
        return LoyaltyPrograms;
    }

    public void setLoyaltyPrograms(List<String> loyaltyPrograms) {
        LoyaltyPrograms = loyaltyPrograms;
    }
}
