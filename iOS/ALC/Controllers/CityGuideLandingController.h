//
//  CityGuideLandingController.h
//  ALC
//
//  Created by Anh Tran on 9/12/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "MainViewController.h"
#import "WSGetCityGuideList.h"
@interface CityGuideLandingController : MainViewController<DataLoadDelegate, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>

@end
