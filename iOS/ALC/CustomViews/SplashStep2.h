//
//  SplashStep2.h
//  ALC
//
//  Created by Hai NguyenV on 8/17/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UpdatingTextSubUIView.h"

@interface SplashStep2 : UpdatingTextSubUIView
@property (weak, nonatomic) IBOutlet UILabel *enjoyLbl;
@property (weak, nonatomic) IBOutlet UILabel *enjoyMessageLbl;

@end
