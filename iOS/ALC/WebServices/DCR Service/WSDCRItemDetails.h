//
//  WSDCRItemDetails.h
//  ALC
//
//  Created by Anh Tran on 3/14/17.
//  Copyright © 2017 Sunrise Software Solutions. All rights reserved.
//

#import "WSDCRBase.h"
#import "DCRItemDetails.h"
@interface WSDCRItemDetails : WSDCRBase
@property (nonatomic, strong) DCRItemDetails* item;

- (void) findItem:(NSString*) item;
@end
