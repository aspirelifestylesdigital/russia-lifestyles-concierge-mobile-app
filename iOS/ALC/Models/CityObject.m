//
//  CityObject.m
//  ALC
//
//  Created by Anh Tran on 10/6/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "CityObject.h"

@implementation CityObject
-(id) initFromDict:(NSDictionary*) dict{
    self = [super init];
    _country = [[CountryObject alloc] initFromDict:[dict objectForKey:@"Country"]];
    _cityName = [dict stringForKey:@"CityName"];
    _states = [dict stringForKey:@"States"];
    return self;
}
@end
