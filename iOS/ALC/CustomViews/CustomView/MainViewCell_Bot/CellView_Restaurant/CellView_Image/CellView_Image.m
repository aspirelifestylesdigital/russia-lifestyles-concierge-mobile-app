//
//  CellView_Restaurant.m
//  demoChatBot
//
//  Created by Nhat Huy Truong  on 9/12/16.
//  Copyright © 2016 Nhat Huy. All rights reserved.
//

#import "CellView_Image.h"
#import "UIImageView+AFNetworking.h"
//#import "GourmetDetailsController.h"

@implementation CellView_Image

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void) loadView
{
    [self.vwFrameContent.layer setCornerRadius:8.0];
    [self.imvRestaurant setImageWithURL:[NSURL URLWithString:self.linkImage]];
}


@end
