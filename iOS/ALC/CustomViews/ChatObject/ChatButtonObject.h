//
//  ChatButtonObject.h
//  ALC
//
//  Created by Nhat Huy Truong  on 9/14/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChatButtonObject : NSObject

@property (strong, nonatomic) NSString* textButton;
@property (strong, nonatomic) NSString* action;
@property (strong, nonatomic) NSString* type;
@property (strong, nonatomic) NSString* phone;
@property (strong, nonatomic) NSString* linkeURL;

+ (id) createObjectWithDic:(NSDictionary*)dicData;
@end
