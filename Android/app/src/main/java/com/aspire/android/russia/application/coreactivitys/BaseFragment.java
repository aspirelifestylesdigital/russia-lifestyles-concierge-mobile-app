package com.aspire.android.russia.application.coreactivitys;

import android.animation.ObjectAnimator;
import android.app.Instrumentation;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.Toast;

import com.aspire.android.russia.R;
import com.aspire.android.russia.activitys.HomeActivity;
import com.aspire.android.russia.fragment.AboutWebFragment;
import com.aspire.android.russia.utils.AndroidDevice;
import com.aspire.android.russia.utils.CommonUtils;
import com.aspire.android.russia.utils.EntranceLock;
import com.aspire.android.russia.utils.NetworkUtil;
import com.aspire.android.russia.utils.SoftKeyboardListener;

import org.greenrobot.eventbus.EventBus;

import butterknife.ButterKnife;

public abstract class BaseFragment
        extends Fragment {
    /**
     * push & pop none animation
     */
    public static boolean isDisableFragmentAnimations = false;
    //
    protected View view = null;
    //
    protected boolean firstLoad = true;

    /**
     * Define all abstract method in here
     * get layout id for create this fragment
     */
    protected abstract int layoutId();

    /**
     * initial view layout as textColor, fontSize, Padding,.. before display.
     * It's called first make screen.
     */
    protected abstract void initView();

    /**
     * bind data into view: textview, listview, imageview...
     * It's called every screen reload.
     */
    protected abstract void bindData();

    /**
     * Event handling for the back button.
     *
     * @return true if the back button was handled by this fragment. Otherwise return false.
     * @since v0.1
     */
    protected abstract boolean onBack();

    static EntranceLock entranceLock = new EntranceLock();
    public static boolean isTakeMoreAction = false; // After 600ms

    SoftKeyboardListener softKeyboardListener;
    private boolean isKeyboardEnabled;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {

        setSoftInputModeAdjustResize();
        //
        if (firstLoad) {
            int layout_id = layoutId();
            if (layout_id == 0) {
                return null;
            }
            view = inflater.inflate(layout_id,
                                    container,
                                    false);

            // Line spacing for all text views
            CommonUtils.setLineSpacingExtraForTextViewRecursive(view);

            ButterKnife.bind(this,
                             view);

            //
            view.setClickable(true);

            // Prevent multi touch
            view.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v,
                                       MotionEvent e) {
                    // True means the event is ignored by the overlayed views
                    return e.getPointerCount() > 1 ?
                           true :
                           false;
                }
            });
            //
            initView();

            //
            firstLoad = false;
            isTakeMoreAction = false;

            CommonUtils.enableDisableView(view,
                                          false);
            new Handler().postDelayed(new Runnable() {
                                          @Override
                                          public void run() {

                                              isTakeMoreAction = true;
                                              CommonUtils.enableDisableView(view,
                                                                            true);
                                          }
                                      },
                                      600);
        }

        //
        bindData();

        return view;
    }

    @Override
    public void onViewCreated(View view,
                              @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view,
                            savedInstanceState);
        if (!NetworkUtil.getNetworkStatus(getContext())) {
            showDialogMessage(getString(R.string.text_title_dialog_error),
                              getString(R.string.text_message_dialog_error_no_internet));
        }
        /*softKeyboardListener = new SoftKeyboardListener();
        softKeyboardListener.attachKeyboardListeners((ViewGroup)view, new SoftKeyboardListener.OnSoftKeyboardListener() {
            @Override
            public void onKeyboardShow(int height) {
                isKeyboardEnabled = true;
            }

            @Override
            public void onKeyboardHide(int height) {
                isKeyboardEnabled = false;
            }
        });*/
    }

    @Override
    public void onPause() {
        super.onPause();
        View focusView = getActivity().getCurrentFocus();
        if (focusView != null) {
            CommonUtils.hideSoftKeyboard(getContext(),
                                         focusView);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        //softKeyboardListener.detachKeyboardListener();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        registerEventBus();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        unRegisterEventBus();
    }

    protected void registerEventBus() {
        try {
            EventBus.getDefault()
                    .register(this);
        } catch (Exception e) {

        }
    }

    protected void unRegisterEventBus() {
        try {
            EventBus.getDefault()
                    .unregister(this);
        } catch (Exception e) {

        }
    }
    /*@Override
    public Animator onCreateAnimator(int transit, boolean enter, int nextAnim) {
        Logger.sout( transit + "; " + enter + "; " + nextAnim);
        if(isDisableFragmentAnimations){
            ObjectAnimator animator = new ObjectAnimator();
            return animator;
        }

//        ObjectAnimator obj;
//        if(nextAnim==R.animator.slide_in_right)
//        {
//            Logger.sout("in right");
//            obj = rightIn();
//        }
//        else if(nextAnim==R.animator.slide_out_left){
//            Logger.sout("out left");
//            obj = leftOut();
//        }
//        else if(nextAnim==R.animator.slide_in_left){
//            Logger.sout("in left");
//            obj = leftIn();
//        }
//        else {
//            Logger.sout("out right");
//            obj = rightOut();
//        }
//        return obj;

        return super.onCreateAnimator(transit, enter, nextAnim);
    }*/

    @Override
    public Animation onCreateAnimation(int transit,
                                       boolean enter,
                                       int nextAnim) {
        if (isDisableFragmentAnimations) {
            //out
//            if(!enter){
//                Animation a = AnimationUtils.loadAnimation(getContext(), R.anim.fade_out);
//                a.setDuration(0);
//                return a;
//            }
//
//            Animation a = AnimationUtils.loadAnimation(getContext(), R.anim.fade_in);
//            //a.setDuration(1700);
//            return a;

            Animation a = new Animation() {
            };
            a.setDuration(0);
            return a;
        }
        return super.onCreateAnimation(transit,
                                       enter,
                                       nextAnim);
    }

    private ObjectAnimator rightIn() {

        int[] size = AndroidDevice.getScreenSizeDemension(getActivity());

        //ObjectAnimator obj = new ObjectAnimator();
        ObjectAnimator obj = ObjectAnimator.ofFloat(this,
                                                    "translationX",
                                                    size[0],
                                                    0);
        obj.setDuration(android.R.integer.config_mediumAnimTime);
//        obj.setPropertyName("translationX");
//        obj.setFloatValues(size[0], 0);

        //.ofFloat(v, "translationY",v.getY(), targetY);

        return obj;
    }

    private ObjectAnimator leftOut() {

        int[] size = AndroidDevice.getScreenSizeDemension(getActivity());

        //ObjectAnimator obj = new ObjectAnimator();
        ObjectAnimator obj = ObjectAnimator.ofFloat(this,
                                                    "translationX",
                                                    0,
                                                    -size[0]);
        obj.setDuration(android.R.integer.config_mediumAnimTime);
//        obj.setPropertyName("translationX");
//        obj.setFloatValues(0, -size[0]);
        //.ofFloat(v, "translationY",v.getY(), targetY);

        return obj;
    }

    private ObjectAnimator leftIn() {

        int[] size = AndroidDevice.getScreenSizeDemension(getActivity());

        //ObjectAnimator obj = new ObjectAnimator();
        ObjectAnimator obj = ObjectAnimator.ofFloat(this,
                                                    "translationX",
                                                    -size[0],
                                                    0);
        obj.setDuration(android.R.integer.config_mediumAnimTime);
//        obj.setPropertyName("translationX");
//        obj.setFloatValues(size[0], 0);
        //.ofFloat(v, "translationY",v.getY(), targetY);

        return obj;
    }

    private ObjectAnimator rightOut() {

        int[] size = AndroidDevice.getScreenSizeDemension(getActivity());

        //ObjectAnimator obj = new ObjectAnimator();
        ObjectAnimator obj = ObjectAnimator.ofFloat(this,
                                                    "translationX",
                                                    0,
                                                    size[0]);
        obj.setDuration(android.R.integer.config_mediumAnimTime);
//        obj.setPropertyName("translationX");
//        obj.setFloatValues(0, -size[0]);
        //.ofFloat(v, "translationY",v.getY(), targetY);

        return obj;
    }

    /**
     * This func call on back in Activity.
     */
    public void onBackPress() {
        AbstractActivity activity = (AbstractActivity) getActivity();
        if (activity != null) {
            activity.onBackPressed();
        }
    }

    /**
     * This func send back key to system
     */
    public void onBackWithCode() {

        new Thread() {
            @Override
            public void run() {
                try {
                    Instrumentation m_Instrumentation = new Instrumentation();
                    m_Instrumentation.sendKeyDownUpSync(KeyEvent.KEYCODE_BACK);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }.start();

    }

    protected void showToast(String msg) {
        Toast.makeText(getActivity(),
                       msg,
                       Toast.LENGTH_SHORT)
             .show();
    }

    public void showDialogProgress() {
        //TODO: call to activity show dialog progress.
        AbstractActivity activity = (AbstractActivity) getActivity();
        if (activity != null) {
            activity.showDialogProgress();
        }
    }

    public void hideDialogProgress() {
        //TODO: call to activity show dialog progress.
        Log.d("ThuNguyen",
              "Hide dialog progress");
        AbstractActivity activity = (AbstractActivity) getActivity();
        if (activity != null) {
            activity.hideDialogProgress();
        }
    }

    public void showDialogMessage(String title,
                                  String message) {
        AbstractActivity activity = (AbstractActivity) getActivity();
        if (activity != null) {
            activity.showDialogMessage(title,
                                       message);
        }
    }

    public void showDialogMessage(String title,
                                  String message,
                                  DialogInterface.OnClickListener onClickListener,
                                  DialogInterface.OnDismissListener onDismissListener) {
        AbstractActivity activity = (AbstractActivity) getActivity();
        if (activity != null) {
            activity.showDialogMessage(title,
                                       message,
                                       onClickListener,
                                       onDismissListener);
        }
    }

    public void showInternetProblemDialog() {
        showDialogMessage("",
                          getString(R.string.network_fail_message));
    }

    public void hideDialogMessage() {
        AbstractActivity activity = (AbstractActivity) getActivity();
        if (activity != null) {
            activity.hideDialogMessage();
        }
    }

    public boolean isDialogMessageShowing() {
        AbstractActivity activity = (AbstractActivity) getActivity();
        if (activity != null) {
            return activity.isDialogMessageShowing();
        }
        return false;
    }

    public void pushFragment(BaseFragment fragment,
                             boolean addBackStack) {
        pushFragment(fragment,
                     addBackStack,
                     true);
    }

    public void pushFragment(BaseFragment fragment,
                             boolean addBackStack,
                             boolean animation) {
        if (!entranceLock.isClickContinuous()) {
            AbstractActivity activity = (AbstractActivity) getActivity();
            if (activity != null) {
                activity.pushFragment(fragment,
                                      false,
                                      addBackStack,
                                      animation);
            }
        }
    }

    public void openNavMenu() {
        //TODO: call activity close navigation menu
        if (getActivity() instanceof AbstractActivity) {
            ((AbstractActivity) getActivity()).openNavMenu();
        }
    }

    public void closeNavMenu() {
        //TODO: call activity close navigation menu
        if (getActivity() instanceof AbstractActivity) {
            ((AbstractActivity) getActivity()).closeNavMenu();
        }
    }

    public void finishFragment() {
        if (getActivity() instanceof AbstractActivity) {
            ((AbstractActivity) getActivity()).closeFragment(this);
        }
    }

    public void finishFragment(int inAnimator,
                               int outAnimator) {
        if (getActivity() instanceof AbstractActivity) {
            ((AbstractActivity) getActivity()).closeFragment(this,
                                                             inAnimator,
                                                             outAnimator);
        }
    }

    public void hideToolbarMenuIcon() {
        if (getActivity() instanceof BaseActivityNavigationView) {
            ((BaseActivityNavigationView) getActivity()).hideToolbarMenuIcon();
        }
    }

    public void showToolbarMenuIcon() {
        if (getActivity() instanceof BaseActivityNavigationView) {
            ((BaseActivityNavigationView) getActivity()).showToolbarMenuIcon();
        }
    }

    public void showRightText(String text,
                              View.OnClickListener onClickListener) {
        if (getActivity() instanceof BaseActivityNavigationView) {
            ((BaseActivityNavigationView) getActivity()).showRightText(text,
                                                                       onClickListener);
        }
    }

    public void hideRightText() {
        if (getActivity() instanceof BaseActivityNavigationView) {
            ((BaseActivityNavigationView) getActivity()).hideRightText();
        }
    }

    /**
     * ====================================
     */
    public void setSoftInputModeAdjustResize() {
        getActivity().getWindow()
                     .setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
    }

    public void setSoftInputModeAdjustPan() {
        getActivity().getWindow()
                     .setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
    }

    public void setSoftInputModeAdjustNothing() {
        getActivity().getWindow()
                     .setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
    }

    public void setTitle(String title) {
        if (getActivity() instanceof BaseActivityNavigationView) {
            ((BaseActivityNavigationView) getActivity()).setTitle(title);
        }
    }

    public void showLogoApp(boolean isShowLogo) {
        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).showLogoApp(isShowLogo);
        }
    }

    public void showFloatingActionButton() {
        if (getActivity() instanceof AbstractActivity) {
            ((AbstractActivity) getActivity()).showFloatingActionButton();
        }
    }

    public void hideFloatingActionButton() {
        if (getActivity() instanceof AbstractActivity) {
            ((AbstractActivity) getActivity()).hideFloatingActionButton();
        }
    }
    public void openMapApp(String lat,
                        String lng,
                        String address) {
        if (CommonUtils.isStringValid(lat) && CommonUtils.isStringValid(lng) && CommonUtils.isStringValid(address)) {
            Uri gmmIntentUri = Uri.parse("geo:" + lat + "," + lng + "?q=" + address+ "?z=14");
            Intent mapIntent = new Intent(Intent.ACTION_VIEW,
                                          gmmIntentUri);
            mapIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mapIntent.setPackage("com.google.android.apps.maps");
            if (mapIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivity(mapIntent);
            }
        } else {
            if (CommonUtils.isStringValid(address)) {
                Uri gmmIntentUri = Uri.parse("?q=" + address+ "?z=14");
                Intent mapIntent = new Intent(Intent.ACTION_VIEW,
                                              gmmIntentUri);
                    /*Intent mapIntent = new Intent(Intent.ACTION_VIEW,
                                                  Uri.parse("http://maps.google.com/maps?z=14?q=" + address));*/
                mapIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mapIntent.setPackage("com.google.android.apps.maps");
                if (mapIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                    startActivity(mapIntent);
                }
            } else {
                if (CommonUtils.isStringValid(lat) && CommonUtils.isStringValid(lng)) {
                    Uri gmmIntentUri = Uri.parse("geo:" + lat + "," + lng);
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW,
                                                  gmmIntentUri);
            /*    Intent mapIntent = new Intent(Intent.ACTION_VIEW,
                                              Uri.parse("http://maps.google.com/maps?geo:" + lat + "," + lng + "?z=14"));*/
                    mapIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    if (mapIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                        startActivity(mapIntent);
                    }
                }
            }
        }
    }

    public void openMap(String lat,
                        String lng,
                        String address) {
        //webview.loadUrl("http://maps.google.com/maps?" + "saddr=43.0054446,-87.9678884" + "&daddr=42.9257104,-88.0508355");
//https://www.google.com/maps?q=loc:55.741915,37.600657&zoom=14
        String urlMap ="https://www.google.com/maps?";
      /*  if (CommonUtils.isStringValid(lat) && CommonUtils.isStringValid(lng) && CommonUtils.isStringValid(address)) {
            urlMap+=*//*"ll=" + lat + "," + lng + *//*"q=" + address*//*+ "?z=14"*//*;

        } else {
            if (CommonUtils.isStringValid(address)) {
                urlMap+="q=" + address*//*+ "?z=14"*//*;

            } else {*/
                if (CommonUtils.isStringValid(lat) && CommonUtils.isStringValid(lng)) {
                  //  urlMap+="ll=" + lat + "," + lng;
                    urlMap+="q=loc:"+lat+","+lng+"&zoom=14";
                    Bundle bundle = new Bundle();
                    bundle.putString(AboutWebFragment.AboutWebTitle,
                                     getString(R.string.privileges_detail_title_location));
                    bundle.putString(AboutWebFragment.AboutWebLink,urlMap);
                    bundle.putBoolean(AboutWebFragment.IS_MAP,true);
                    AboutWebFragment fragment = new AboutWebFragment();
                    fragment.setArguments(bundle);

                    pushFragment(fragment,
                                 true,
                                 true);

                }
           /* }
        }*/

    }

}