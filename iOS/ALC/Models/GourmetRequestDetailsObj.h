//
//  GourmetRequestDetailsObj.h
//  ALC
//
//  Created by Hai NguyenV on 10/7/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "BaseBookingDetailsObject.h"

@interface GourmetRequestDetailsObj : BaseBookingDetailsObject

@property (strong, nonatomic) NSString* reservationName;
@property (strong, nonatomic) NSString* restaurantName;
@property (strong, nonatomic) NSDate* reservationDate;
@property (strong, nonatomic) NSDate* reservationsTime;
@property (strong, nonatomic) NSArray* occasion;
@property (strong, nonatomic) NSString* foodAllergies;

-(id) initFromDict:(NSDictionary*) dict;

@end
