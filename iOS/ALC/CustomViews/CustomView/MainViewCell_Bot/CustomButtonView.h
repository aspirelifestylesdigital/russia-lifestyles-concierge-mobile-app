//
//  CustomButtonView.h
//  demoChatBot
//
//  Created by Nhat Huy on 9/10/16.
//  Copyright © 2016 Nhat Huy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChatButtonObject.h"

@protocol CustomButtonViewDelegate <NSObject>

@optional

- (void) clickAtButtonObject:(ChatButtonObject*)currentButton;

@end
@interface CustomButtonView : UIView

@property (strong, nonatomic) IBOutlet UIButton *btnShoe;
@property (strong,nonatomic) NSString* valueText;
@property (strong, nonatomic) ChatButtonObject* currentButtonObject;

@property (strong, nonatomic) IBOutlet UILabel *lblText;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *layoutHeightText;

@property (assign,nonatomic) id <CustomButtonViewDelegate> delegate;

- (void) loadView;
- (IBAction)clickActiveButton:(id)sender;
- (void) returnNormal;

@end
