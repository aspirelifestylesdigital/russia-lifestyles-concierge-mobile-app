package com.aspire.android.russia.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.aspire.android.russia.R;
import com.aspire.android.russia.adapter.ChatbotAdapter;
import com.aspire.android.russia.apiservices.ResponseModel.AddWishListResponse;
import com.aspire.android.russia.apiservices.ResponseModel.ChatbotResponse;
import com.aspire.android.russia.apiservices.ResponseModel.RemoveWishListResponse;
import com.aspire.android.russia.apiservices.WSAddToWishList;
import com.aspire.android.russia.apiservices.WSChatbot;
import com.aspire.android.russia.apiservices.WSRemoveFromWishList;
import com.aspire.android.russia.application.AppConstant;
import com.aspire.android.russia.application.coreactivitys.BaseFragment;
import com.aspire.android.russia.model.BotDataArray;
import com.aspire.android.russia.model.BotSay;
import com.aspire.android.russia.model.Restaurant;
import com.aspire.android.russia.model.UserItem;
import com.aspire.android.russia.utils.CommonUtils;
//import com.aspire.android.russia.utils.GPSTrackerService;
//import com.aspire.android.russia.utils.GpsTrackerManager;
import com.aspire.android.russia.utils.IntentUtil;
import com.aspire.android.russia.utils.Logger;
import com.aspire.android.russia.utils.SelfPermissionUtils;
import com.aspire.android.russia.utils.SoftKeyboardListener;
import com.aspire.android.russia.utils.StringUtil;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nga.nguyent on 10/17/2016.
 */

public class ChatbotFragment extends BaseFragment implements ChatbotAdapter.OnChatbotButtonClickListener,
        ChatbotAdapter.OnRestaurantInfoClickListener,
Callback{

    @BindView(R.id.rclView)
    RecyclerView rclView;
    @BindView(R.id.edtMessage)
    EditText edtMessage;
    @BindView(R.id.imgSend)
    ImageView imgSend;
    @BindView(R.id.imgCategory)
    ImageView imgCategory;

    ChatbotAdapter adapter;

    private String chatMessage;
    private String convoId = "";
    private CountDownTimer countdownTimer;
    private boolean cancelCountdown = false;
    Handler handler = new Handler();
   // GpsTrackerManager gpsManager;
    SoftKeyboardListener keyboardListener;

    BroadcastReceiver receiverLocationChange = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
        //    Location location = intent.getExtras().getParcelable(GPSTrackerService.LOCATION_KEY);
            /*if(location!=null){
                GpsTrackerManager gps = GpsTrackerManager.getInstance(getContext());
                String locationString = gps.convertLocationToString(location);
                //String locationString2 = location.toString();
                System.out.println(locationString);
            }*/
        }
    };

    @Override
    protected int layoutId() {
        return R.layout.fragment_chatbot;
    }

    @Override
    protected void initView() {
        showLogoApp(false);
        setTitle(getString(R.string.text_title_chatbot));

        adapter = new ChatbotAdapter(getContext());
        adapter.setChatbotClick(this);
        adapter.setRestaurantInfoClickListener(this);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rclView.setLayoutManager(layoutManager);
        rclView.setAdapter(adapter);
        //
        edtMessage.setText(chatMessage);
        rclView.setClickable(true);
        rclView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonUtils.hideSoftKeyboard(getContext(), view);
            }
        });
        view.setClickable(true);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonUtils.hideSoftKeyboard(getContext(), view);
            }
        });

        edtMessage.setEnabled(false);
        imgSend.setClickable(false);
        edtMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                cancelCountdown = true;
            }

            @Override
            public void afterTextChanged(Editable s) {
                startCountDownTime();
            }
        });

        //
        openChat();
     //   gpsManager = GpsTrackerManager.getInstance(getContext());
        keyboardListener = new SoftKeyboardListener();
    }

    @Override
    protected void bindData() {
        hideFloatingActionButton();
    }

    @Override
    protected boolean onBack() {
        return false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        showFloatingActionButton();
    }

    @Override
    public void onResume() {
        super.onResume();
        starAnimationRotate();
     /*   getActivity().registerReceiver(receiverLocationChange, new IntentFilter(GPSTrackerService.ACTION_BROADCAST_GPSTRACKER));
        if(SelfPermissionUtils.getInstance().checkOrRequestLocationPermission(getActivity(), AppConstant.PERMISSION_REQUEST_LOCATION))
            gpsManager.initialize();*/
        keyboardListener.attachKeyboardListeners((ViewGroup)view, new SoftKeyboardListener.OnSoftKeyboardListener() {
            @Override
            public void onKeyboardShow(int height) {
                rclView.smoothScrollBy(0, rclView.getBottom() + 270);
            }

            @Override
            public void onKeyboardHide(int height) {
                //System.out.println("keyboar hide");
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(receiverLocationChange);
       // gpsManager.stopGpsTrackerService();
        keyboardListener.detachKeyboardListener();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode){
            case AppConstant.PERMISSION_REQUEST_LOCATION:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
             //       gpsManager.initialize();
                }
                break;
            case AppConstant.PERMISSION_REQUEST_CALL_REQUEST:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    callPhone();
                }
                break;
        }

    }

    public String getChatMessage() {
        return chatMessage;
    }

    public void setChatMessage(String chatMessage) {
        this.chatMessage = chatMessage;
    }

    @OnClick({R.id.llKeyboard, R.id.llCategory})
    void onInputTypeClick(View v){
        if(v.getId()==R.id.llKeyboard){
            if(edtMessage.requestFocus()){
                CommonUtils.showSoftKeyboard(getContext(), edtMessage);
            }
        }
        else{
            sendMessage("recommend", true);
        }
    }

    @OnClick(R.id.imgSend)
    void onSendClick(){
        sendMessage(false);
    }

    private void addUserMessage(String msg){
        BotSay bot = new BotSay();
        bot.setUserMessage(msg);
        adapter.addData(bot);
        //adapter.notifyItemInserted(adapter.getItemCount()-1);
        adapter.notifyDataSetChanged();
        rclView.smoothScrollToPosition(adapter.getItemCount()-1);
    }

    private void sendMessage(boolean isAutoMessage){
        String text = "";
        if(!isAutoMessage) {
            text = edtMessage.getText().toString().trim();
            if (StringUtil.isEmpty(text)) {
                hideDialogProgress();
                return;
            }

            showDialogProgress();
            //
            edtMessage.setText("");

            addUserMessage(text);

            sendMessage(text, true);
        }
        else{
            text = "olala";
            sendMessage(text, false);
        }
    }

    private void sendMessage(String message, boolean showProgess){

        if(showProgess)
            showDialogProgress();
        //
        cancelCountdown = true;

        WSChatbot chatbot = new WSChatbot();
        chatbot.setConvoid(convoId);
        if(UserItem.isLogined()){
            chatbot.setUsname(UserItem.getLoginedFullName());
            chatbot.setUid(UserItem.getLoginedId());
            chatbot.setCountries(UserItem.getLoginedCountry());
            chatbot.setCities(UserItem.getLoginedCity());
        }
        chatbot.setSay(message);
     /*  Location location = gpsManager.getLastLocation();
        if(location!=null){
            chatbot.setLati(location.getLatitude()+"");
            chatbot.setLongi(location.getLongitude()+"");
            chatbot.setLocation("1");
        }*/

        chatbot.run(new Callback<ChatbotResponse>() {
            @Override
            public void onResponse(Call<ChatbotResponse> call, Response<ChatbotResponse> response) {
                if(response.body() instanceof ChatbotResponse){
                    ChatbotResponse rsp = response.body();
                    if(rsp.isSuccess()){
                        convoId = rsp.getConvo_id();
                        BotSay bot = rsp.getBotsay();
                        if(bot.isImagesData()){
                            if(bot.isEmptyImagesData()) {
                                hideDialogProgress();
                                if(!adapter.lastItemIsConsultantMessage())
                                    startCountDownTime();

                                return;
                            }
                        }
                        adapter.addData(bot);
                        adapter.notifyItemInserted(adapter.getItemCount()-1);
                        view.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                rclView.smoothScrollToPosition(adapter.getItemCount()-1);
                            }
                        }, 300);

                        //
                        if(bot.isFaceWithConsultantMessage()){
                            cancelCountdown = true;
                        }
                        else{
                            startCountDownTime();
                        }
                    }
                }
                hideDialogProgress();
            }

            @Override
            public void onFailure(Call<ChatbotResponse> call, Throwable t) {
                hideDialogProgress();
                if(!StringUtil.isEmpty(t.getMessage())) {
                    Logger.sout(t.getMessage());
                    //showDialogMessage("", t.getMessage());
                }
            }
        });
    }

    private void openChat(){
        showDialogProgress();
        WSChatbot chatbot = new WSChatbot();
        if(UserItem.isLogined()){
            chatbot.setUsname(UserItem.getLoginedFullName());
            chatbot.setUid(UserItem.getLoginedId());
            chatbot.setCountries(UserItem.getLoginedCountry());
            chatbot.setCities(UserItem.getLoginedCity());
        }
        chatbot.setSay("Hello");
        chatbot.run(new Callback<ChatbotResponse>() {
            @Override
            public void onResponse(Call<ChatbotResponse> call, Response<ChatbotResponse> response) {
                if(response.body() instanceof ChatbotResponse){
                    ChatbotResponse rsp = response.body();
                    if(rsp.isSuccess()){
                        convoId = rsp.getConvo_id();
                        if(rsp.getBotsay().isOpenChatOk()){
                            adapter.addData(rsp.getBotsay());
                            adapter.notifyItemInserted(adapter.getItemCount()-1);
                            view.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    startCountDownTime();
                                    //send default message
                                    sendMessage(false);
                                }
                            }, 800);
                        }
                        else {
                            hideDialogProgress();
                        }
                    }
                    else{
                        hideDialogProgress();
                    }
                    //
                    edtMessage.setEnabled(true);
                    imgSend.setClickable(true);
                }
                else{
                    hideDialogProgress();
                }
            }

            @Override
            public void onFailure(Call<ChatbotResponse> call, Throwable t) {
                hideDialogProgress();
                edtMessage.setEnabled(true);
                imgSend.setClickable(true);
            }
        });
    }

    @Override
    public void onButtonClick(View v) {
        switch (v.getId()){
            case R.id.btnYes:
                edtMessage.setText("Yes");
                sendMessage(false);
                break;
            case R.id.btnNo:
                edtMessage.setText("No");
                sendMessage(false);
                break;
            case R.id.btnCallUs:
                if(SelfPermissionUtils.getInstance().checCallPhonePermission(getActivity(), AppConstant.PERMISSION_REQUEST_CALL_REQUEST)) {
                    callPhone();
                }
                break;
            case R.id.btnLiveChat:
                //showToast("Open livechat");
                pushFragment(new LiveChat(), true, true);
                break;
            case R.id.btnAvailableTables:
                addUserMessage(((TextView)v).getText().toString());
                sendMessage(v.getTag().toString(), true);
                break;
            case R.id.btnAuthenticVietnameseFood:
                addUserMessage(((TextView)v).getText().toString());
                sendMessage(v.getTag().toString(), true);
                break;
            case R.id.btnConciergeRecommend:
                addUserMessage(((TextView)v).getText().toString());
                //Logger.sout(v.getTag().toString().trim());
                sendMessage(v.getTag().toString().trim(), true);
                break;
            case R.id.btnMichelinStars:
                addUserMessage(((TextView)v).getText().toString());
                sendMessage(v.getTag().toString(), true);
                break;
            case R.id.btnNearbyRestaurant:
                addUserMessage(((TextView)v).getText().toString());
                sendMessage(v.getTag().toString(), true);
                break;
            case R.id.btnNewRestaurant:
                addUserMessage(((TextView)v).getText().toString());
                sendMessage(v.getTag().toString(), true);
                break;
        }
    }

    private void startCountDownTime(){
        if(adapter.lastItemIsConsultantMessage())
            return;

        cancelCountdown = false;
        long time = 1 * 60 * 1000;
        countdownTimer = new CountDownTimer(time, 200) {
            @Override
            public void onTick(long millisUntilFinished) {
                //System.out.println("time tick");
                if(cancelCountdown){
                    cancel();
                }
            }

            @Override
            public void onFinish() {
                if(cancelCountdown)
                    return;

                //System.out.println("time finish");
                if(!adapter.lastItemIsConsultantMessage())
                    sendMessage(true);
            }
        };
        countdownTimer.start();
    }

    private void starAnimationRotate(){
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(getActivity()==null) {
                    return;
                }

                Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.rotate_spoon);
                imgCategory.startAnimation(animation);
                //
                handler.postDelayed(this, 5000);
            }
        }, 5000);
    }

    @Override
    public void onFavoriteClick(int chatItemIndex, int restaurantIndex) {
        BotSay bot = adapter.getItemAtIndex(chatItemIndex);
        if(bot==null || !bot.isImagesData())
            return;

        BotDataArray data = bot.getData();
        Restaurant rest = data.getListData().get(restaurantIndex);

        String userId = UserItem.getLoginedId();
        String restId = rest.getIDStr();

        showDialogProgress();
        if(rest.isVoted()){
            //remove favourite
            rest.setAddWishListStatusToCSS("");
            adapter.notifyDataSetChanged();

            WSRemoveFromWishList add = new WSRemoveFromWishList();
            add.setUserId(userId);
            add.setItemId(restId);
            add.run(this);
        }
        else {
            //add
            rest.setAddWishListStatusToCSS(Restaurant.VOTED);
            adapter.notifyDataSetChanged();

            WSAddToWishList add = new WSAddToWishList();
            add.setUserId(userId);
            add.setItemId(restId);
            add.run(this);
        }
    }

    @Override
    public void onShareClick(int chatItemIndex, int restaurantIndex) {
        IntentUtil util = IntentUtil.getInstance(getContext());
        util.shareAll("aaaaaaaaaaaaaaaaa");
    }

    @Override
    public void onReadMoreClick(int chatItemIndex, int restaurantIndex) {
        BotSay bot = adapter.getItemAtIndex(chatItemIndex);
        if(bot==null || !bot.isImagesData())
            return;
        BotDataArray data = bot.getData();
        Restaurant rest = data.getListData().get(restaurantIndex);

        Bundle bundleDetail = new Bundle();
        bundleDetail.putSerializable(ExperiencesDetailFragment.EXP_DETAIL_TYPE, ExperienceSearchFragment.ApiRequest.GOURMET);
        //bundleDetail.putParcelable(ExperiencesDetailFragment.EXP_DETAIL_DATA, rest);
        bundleDetail.putString(ExperiencesDetailFragment.EXP_DETAIL_ID_RESTAURANT, rest.getIDStr());

        ExperiencesDetailFragment detailFr = new ExperiencesDetailFragment();
        detailFr.setArguments(bundleDetail);
        pushFragment(detailFr, true, true);
    }

    @Override
    public void onBookNowClick(int chatItemIndex, int restaurantIndex) {
        BotSay bot = adapter.getItemAtIndex(chatItemIndex);
        if(bot==null || !bot.isImagesData())
            return;
        BotDataArray data = bot.getData();
        Restaurant rest = data.getListData().get(restaurantIndex);

        ConciergeGourmetFragment fragment = new ConciergeGourmetFragment();
        fragment.setRequestFrom(ConciergeGourmetFragment.GOURMETREQUEST.CHATBOT);
        fragment.setRestaurant(rest);
        pushFragment(fragment, true, true);
    }

    @Override
    public void onResponse(Call call, Response response) {
        if(response.body() instanceof AddWishListResponse){
            AddWishListResponse rsp = (AddWishListResponse) response.body();
            if(rsp.isSuccess()){
                //good
                Logger.sout("add favorite success");
                //adapter.notifyDataSetChanged();
            }
            else{
                if(rsp.getData()!=null) {
                    Logger.sout("add favorite fail");
                }
            }
        }
        else if(response.body() instanceof RemoveWishListResponse){
            RemoveWishListResponse rsp = (RemoveWishListResponse) response.body();
            if(rsp.isSuccess()){
                //good
                Logger.sout("remove favorite success");
                //adapter.notifyDataSetChanged();
            }
            else{
                if(rsp.getData()!=null) {
                    String itemId = rsp.getData().getItemID();
                    //reset favourite value
                    Logger.sout("remove favorite success");
                }
            }
        }
        hideDialogProgress();
    }

    @Override
    public void onFailure(Call call, Throwable t) {
        hideDialogProgress();
    }

    private void callPhone(){
        IntentUtil call = IntentUtil.getInstance(getContext());
        call.openCall(getString(R.string.default_phone_number));
    }

}