package com.aspire.android.russia.apiservices.b2c;

import com.aspire.android.russia.apiservices.apis.appimplement.b2c.B2CApiProviderClient;
import com.aspire.android.russia.apiservices.apis.appimplement.b2c.B2CICallback;
import com.aspire.android.russia.apiservices.b2c.RequestModel.B2CGetPreferenceRequest;
import com.aspire.android.russia.apiservices.b2c.ResponseModel.B2CGetPreferenceResponse;
import com.aspire.android.russia.application.AppConstant;
import com.aspire.android.russia.utils.SharedPreferencesUtils;

import java.util.List;
import java.util.Map;

import retrofit2.Callback;

public class B2CWSGetPreference
        extends B2CApiProviderClient<List<B2CGetPreferenceResponse>>{

    public B2CWSGetPreference(B2CICallback callback){
        this.b2CICallback = callback;
    }
    private B2CGetPreferenceRequest request = new B2CGetPreferenceRequest();
    public void setRequest(B2CGetPreferenceRequest b2CGetPreferenceRequest){
        request = b2CGetPreferenceRequest;
    }
    @Override
    public void run(Callback<List<B2CGetPreferenceResponse>> cb) {
        this.callback = cb;
        super.checkAuthenticateAndRun();
    }
    @Override
    protected boolean isUseAuthenticateInApi() {
        return true;
    }

    @Override
    protected void runApi() {
        request.setAccessToken(SharedPreferencesUtils.getPreferences(AppConstant.PRE_B2C_ACCESS_TOKEN, ""));
        serviceInterface.getPreferences(request).enqueue(this);
    }

    @Override
    protected Map<String, String> prepareHeaders() {
        return null;
    }

    @Override
    protected void postResponse(List<B2CGetPreferenceResponse> response) {
    }
}
