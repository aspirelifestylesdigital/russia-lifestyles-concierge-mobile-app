//
//  GolfRequestController.h
//  ALC
//
//  Created by Hai NguyenV on 10/10/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "MainViewController.h"
#import "OptionalDetailsView.h"
#import "GolfRequestObj.h"
#import "WSCreateGolfRequest.h"
#import "WSGetGolfRequestDetails.h"
#import "WSGetMyPreferences.h"

@interface GolfRequestController : MainViewController<UITextFieldDelegate,DataLoadDelegate,OptionalDetailsViewDelegate,SubFilterControllerDelegate,ErrorToolTipDelegate>
{
    NSInteger paxNumber;
    NSInteger ballersNumber;
    NSString* selectedCountry;
    NSString* selectedCity;
    NSString* selectedState;
    UIDatePicker *datepickerGolf;
    UIDatePicker *timepickerGolf;
    
    WSCreateGolfRequest* wsCreateRequest;
    WSGetGolfRequestDetails* wsGetGolfDetails;
    WSGetMyPreferences* wsGetMyPreference;
}

@property (strong, nonatomic) NSString* bookingId;
@property (strong, nonatomic) NSString* handicap;

@property (strong, nonatomic) IBOutlet OptionalDetailsView *optionalDetailsView;

@property (strong, nonatomic) IBOutlet UITextField *txtGolfCourse;
@property (strong, nonatomic) IBOutlet UITextField *txtGolfDate;
@property (strong, nonatomic) IBOutlet UITextField *txtGolfTimes;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet CustomTextField *txtCity;
@property (strong, nonatomic) IBOutlet UIButton *btCountry;
@property (strong, nonatomic) IBOutlet UIButton *btSubmit;
@property (strong, nonatomic) IBOutlet UIView *viewSubmitButton;
@property (strong, nonatomic) IBOutlet UIButton *btRadio9Hole;
@property (strong, nonatomic) IBOutlet UIButton *btRadio18Hole;
@property (strong, nonatomic) IBOutlet UITextField *txtBallers;
@property (weak, nonatomic) IBOutlet UIView *pvState;
@property (strong, nonatomic) IBOutlet UIButton *btState;


- (IBAction)actionCoutryClicked:(id)sender;
//- (IBAction)actionCityClicked:(id)sender;
- (IBAction)actionSubmitRequest:(id)sender;

@end
