//
//  GourmetLandingViewCell.m
//  ALC
//
//  Created by Anh Tran on 8/23/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "GourmetLandingViewCell.h"
#import "UIImageView+AFNetworking.h"

@implementation GourmetLandingViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) showTitle:(NSString*)title andImage:(NSString*)img{
    _lbTitle.text = title;
    
    if([img hasPrefix:@"http"]){
        [_imvMainImg setImageWithURL:[NSURL URLWithString:img] placeholderImage:nil];
    } else {
        //get image from resouce
        [_imvMainImg setImage:[UIImage imageNamed:img]];
    }
}
@end
