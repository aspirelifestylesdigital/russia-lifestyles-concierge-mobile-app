//
//  WSB2CBase.h
//  ALC
//
//  Created by Anh Tran on 11/1/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSBase.h"

@interface WSB2CBase : WSBase < DataLoadDelegate>

@end
