package com.aspire.android.russia.dcr.api.RequestModel;


import com.google.gson.annotations.Expose;

public class DCRGetPrivilegeDetailRequest extends DCRBaseRequest {
    @Expose
    private String ids;
    public DCRGetPrivilegeDetailRequest() {
    }

    public String getLanguage() {
        return language;
    }

    public DCRGetPrivilegeDetailRequest setLanguage(String language) {
        this.language = language;
        return this;
    }

    public String getIds() {
        return ids;
    }

    public DCRGetPrivilegeDetailRequest setIds(String ids) {
        this.ids = ids;
        return this;
    }
}
