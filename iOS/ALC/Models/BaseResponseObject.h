//
//  BaseResponseObject.h
//  ALC
//
//  Created by Anh Tran on 8/19/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseDCRObject.h"
@interface BaseResponseObject : NSObject
@property (nonatomic) NSInteger task;
@property (nonatomic) NSInteger status;
@property (nonatomic) BOOL b2cStatus;
@property (nonatomic, strong) NSString* b2cErrorCode;
@property (strong, nonatomic) NSString* message;
-(id) initFromDict:(NSDictionary*) dict;
-(id) initFromB2CDict:(NSDictionary*) dict;
-(id) initFromDCR:(BaseDCRObject*) dcrResponse;
- (BOOL) isSuccess;
@end
