package com.aspire.android.russia.model.preference;

import com.aspire.android.russia.apiservices.b2c.ResponseModel.B2CGetPreferenceResponse;
import com.aspire.android.russia.application.AppConstant;

/**
 * Created by ThuNguyen on 12/13/2016.
 */

public class HotelPreferenceDetailData extends PreferenceData{
    private int rating;
    private String roomPreference;
    private String bedPreference;
    private boolean smokingPreference;
    private String loyaltyProgram;
    private String memberShipNo;
    private String additionalPreference;
    public HotelPreferenceDetailData(B2CGetPreferenceResponse response){
        super(response);
        preferenceType = AppConstant.PREFERENCE_TYPE.HOTEL;
        if(response != null){
            try {
                rating = Integer.parseInt(response.getVALUE());
            }catch (Exception e){}
            roomPreference = response.getVALUE1();
            bedPreference = response.getVALUE2();
            String value3 = response.getVALUE3();
            smokingPreference = value3 != null && (value3.equalsIgnoreCase("yes") || value3.equalsIgnoreCase("true"));
            loyaltyProgram = response.getVALUE4();
            memberShipNo = response.getVALUE5();
            additionalPreference = response.getVALUE6();
        }
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getRoomPreference() {
        return roomPreference;
    }

    public void setRoomPreference(String roomPreference) {
        this.roomPreference = roomPreference;
    }

    public String getBedPreference() {
        return bedPreference;
    }

    public void setBedPreference(String bedPreference) {
        this.bedPreference = bedPreference;
    }

    public boolean isSmokingPreference() {
        return smokingPreference;
    }

    public void setSmokingPreference(boolean smokingPreference) {
        this.smokingPreference = smokingPreference;
    }

    public String getLoyaltyProgram() {
        return loyaltyProgram;
    }

    public void setLoyaltyProgram(String loyaltyProgram) {
        this.loyaltyProgram = loyaltyProgram;
    }

    public String getMemberShipNo() {
        return memberShipNo;
    }

    public void setMemberShipNo(String memberShipNo) {
        this.memberShipNo = memberShipNo;
    }

    public void setAdditionalPreference(String additionalPreference) {
        this.additionalPreference = additionalPreference;
    }

    public String getAdditionalPreference() {
        return additionalPreference;
    }
}
