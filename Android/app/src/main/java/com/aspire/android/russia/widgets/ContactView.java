package com.aspire.android.russia.widgets;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aspire.android.russia.R;
import com.aspire.android.russia.application.AppConstant;
import com.aspire.android.russia.model.UserItem;
import com.aspire.android.russia.model.concierge.MyRequestObject;
import com.aspire.android.russia.utils.CommonUtils;
import com.aspire.android.russia.utils.FontUtils;
import com.aspire.android.russia.utils.SharedPreferencesUtils;
import com.aspire.android.russia.utils.StringUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import butterknife.OnTouch;

/**
 * @author anh.trinh
 * @since v0.1
 */
public class ContactView
        extends RelativeLayout {

    private static final int LAYOUT_RESOURCE_ID = R.layout.contact_view;
    @BindView(R.id.tv_title_reservation_name)
    TextView tvReservationNameTitle;
    @BindView(R.id.edt_reservation_name)
    EditText edtReservationName;
    @BindView(R.id.reservation_name_error)
    CustomErrorView reservationNameErrorView;
    @BindView(R.id.tv_i_would_like_to_be_contact)
    TextView mTvIWouldLikeToBeContact;
    @BindView(R.id.rdb_email)
    RadioButton mRdbEmail;
    @BindView(R.id.rdb_phone)
    RadioButton mRdbPhone;
    @BindView(R.id.rdb_both)
    RadioButton mRdbBoth;
    @BindView(R.id.group_contact)
    RadioGroup mGroupContact;
    @BindView(R.id.tv_alternate_email_contact_numbers)
    TextView mTvAlternateEmailContactNumbers;
    @BindView(R.id.tv_mobile_no)
    TextView mTvMobileNo;
    @BindView(R.id.tv_country_no)
    TextView mTvCountryNo;
    @BindView(R.id.edt_phone_number)
    EditText mEdtPhoneNumber;
    @BindView(R.id.phone_error)
    CustomErrorView mPhoneError;
    @BindView(R.id.layout_mobile)
    RelativeLayout mLayoutMobile;
    @BindView(R.id.tv_title_email)
    TextView mTvTitleEmail;
    @BindView(R.id.edt_email)
    EditText mEdtEmail;
    @BindView(R.id.email_error)
    CustomErrorView mEmailError;
    @BindView(R.id.layout_email)
    RelativeLayout mLayoutEmail;
    @BindView(R.id.layout_contain_email)
    LinearLayout mLayoutContainEmail;
    @BindView(R.id.layout_contain_contact)
    LinearLayout mLayoutContainContact;

    public interface ContactViewListener {
        void onSelectCountryClick();
    }

    ContactViewListener mContactViewListener;

    public void setContactViewListener(ContactViewListener listener) {
        this.mContactViewListener = listener;
    }

    /**
     * @param context
     *         The context which view is running on.
     * @return New ItemListCollections object.
     * @since v0.1
     */
    public static ContactView newInstance(final Context context) {
        if (context == null) {
            return null;
        }

        return new ContactView(context);
    }

    /**
     * The constructor with current context.
     *
     * @param context
     *         The context which view is running on.
     * @since v0.1
     */
    public ContactView(final Context context) {
        super(context);
        this.initialize();
    }

    /**
     * The constructor with current context.
     *
     * @param context
     *         The context which view is running on.
     * @param attrs
     *         The initialize attributes set.
     * @since v0.1
     */
    public ContactView(final Context context,
                       final AttributeSet attrs) {
        super(context,
              attrs);
        this.initialize();
    }

    /**
     * The constructor with current context.
     *
     * @param context
     *         The context which view is running on.
     * @param attrs
     *         The initialize attributes set.
     * @param defStyleAttr
     *         The default style.
     * @since v0.1
     */
    public ContactView(final Context context,
                       final AttributeSet attrs,
                       final int defStyleAttr) {
        super(context,
              attrs,
              defStyleAttr);

        this.initialize();
    }

    /**
     * @see View#setEnabled(boolean)
     * @since v0.1
     */
    @Override
    public void setEnabled(final boolean isEnabled) {
        this.setAlpha(isEnabled ?
                      1f :
                      0.5f);
        super.setEnabled(isEnabled);
    }

    /**
     * Reset injected resources created by ButterKnife.
     *
     * @since v0.1
     */
    public void resetInjectedResource() {
        ButterKnife.bind(this);
    }

    /**
     * Initialize UI sub views.
     *
     * @since v0.1
     */
    private void initialize() {

        final LayoutInflater layoutInflater = LayoutInflater.from(this.getContext());
        layoutInflater.inflate(LAYOUT_RESOURCE_ID,
                               this,
                               true);

        ButterKnife.bind(this,
                         this);
        CommonUtils.setFontForViewRecursive(mLayoutContainContact,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);
        CommonUtils.makeFirstCharacterUpperCase(edtReservationName);
        // Set default reservation name as login name
        edtReservationName.setText(UserItem.getLoginedFullName());
        mEdtEmail.setText(SharedPreferencesUtils.getPreferences(AppConstant.USER_EMAIL_PRE, ""));
        mEdtEmail.setSelection(mEdtEmail.getText().toString().length());
        setFullPhoneNumber(SharedPreferencesUtils.getPreferences(AppConstant.USER_MOBILE_NUMBER, ""));
        mRdbEmail.setChecked(true);

    }
    public void setPhoneNumberHint(int hintResId){
        mEdtPhoneNumber.setHint(hintResId);
    }
    public void updateContactView(MyRequestObject myRequestObject){
        if(!TextUtils.isEmpty(myRequestObject.getPrefResponse())){
             if(myRequestObject.getPrefResponse().equalsIgnoreCase("Phone")){
                mRdbPhone.setChecked(true);
            }else{
            /*if(myRequestObject.getPrefResponse().equalsIgnoreCase("Email")){*/
                mRdbEmail.setChecked(true);
            }/*else if(myRequestObject.getPrefResponse().equalsIgnoreCase("Email,Phone")){
                mRdbBoth.setChecked(true);
            }*/
        }

        mEdtEmail.setText(myRequestObject.getEmailAddress());
        mEdtEmail.setSelection(mEdtEmail.getText().toString().length());
        setFullPhoneNumber(myRequestObject.getMobileNumber());
       /* String reservationName = myRequestObject.getReservationName();
        if(!CommonUtils.isStringValid(reservationName)){
            reservationName = myRequestObject.getFullName();
        }
        if(!CommonUtils.isStringValid(reservationName)){
            reservationName = myRequestObject.getFullName();
        }*/
        edtReservationName.setText(myRequestObject.getReservationName()==null?myRequestObject.getFullName():myRequestObject.getReservationName());

    }
    @OnClick(R.id.tv_country_no)
    public void onClick() {
        mPhoneError.setVisibility(View.GONE);
        if (mContactViewListener != null) {
            mContactViewListener.onSelectCountryClick();
        }
    }
    @OnTouch({R.id.edt_phone_number,
              R.id.edt_email,
            R.id.edt_reservation_name})
   public boolean onInputTouch(final View v,
                         final MotionEvent event) {

        switch (v.getId()) {
            case R.id.edt_phone_number:
                mPhoneError.setVisibility(View.GONE);
                break;
            case R.id.edt_email:
                mEmailError.setVisibility(View.GONE);
                break;
            case R.id.edt_reservation_name:
                reservationNameErrorView.setVisibility(GONE);
                break;
        }
        return false;
    }
    @OnCheckedChanged({R.id.rdb_email,
                       R.id.rdb_phone,
                       R.id.rdb_both})
    public void onCheckedChange(CompoundButton buttonView,
                                boolean isChecked) {
        if (isChecked) {
            switch (buttonView.getId()) {
                case R.id.rdb_email:
                    mLayoutContainEmail.setVisibility(View.VISIBLE);
                    mLayoutMobile.setVisibility(View.GONE);
                    break;
                case R.id.rdb_phone:
                    mLayoutContainEmail.setVisibility(View.GONE);
                    mLayoutMobile.setVisibility(View.VISIBLE);
                    break;
                case R.id.rdb_both:
                    mLayoutContainEmail.setVisibility(View.VISIBLE);
                    mLayoutMobile.setVisibility(View.VISIBLE);
                    break;
            }
        }

    }
    public boolean validationContact() {
        boolean result = true;
        if(TextUtils.isEmpty(edtReservationName.getText().toString().trim())){
            result = false;
            reservationNameErrorView.fillData(getContext().getString(R.string.text_sign_up_error_required_field));
            reservationNameErrorView.setVisibility(VISIBLE);
        }else if(StringUtil.containSpecialCharacter(edtReservationName.getText().toString().trim())) {
            result = false;
            reservationNameErrorView.fillData(getContext().getString(R.string.special_requirement_error));
            reservationNameErrorView.setVisibility(VISIBLE);
        }else{
            String[] firstLastName = StringUtil.getKeyValueFromSplitor(edtReservationName.getText().toString().trim(), " ");
            if(TextUtils.isEmpty(firstLastName[1])){
                result = false;
                reservationNameErrorView.fillData(getContext().getString(R.string.firstname_lastname_missing_error));
                reservationNameErrorView.setVisibility(VISIBLE);
            }else {
            reservationNameErrorView.setVisibility(GONE);
        }
        }
        if(mRdbBoth.isChecked()){
            // Check email and phone
            String email = mEdtEmail.getText().toString().trim();
            if(email.length() == 0){
                mEmailError.fillData(getResources().getString(R.string.text_sign_up_error_required_field));
                mEmailError.setVisibility(VISIBLE);
                result = false;
            }else if(!CommonUtils.emailValidator(email)){
                mEmailError.fillData(getResources().getString(R.string.text_sign_up_please_enter_a_valid_email_address));
                mEmailError.setVisibility(View.VISIBLE);
                result = false;
            }else{
                mEmailError.setVisibility(GONE);
            }
            // Check phone
            String phone = mEdtPhoneNumber.getText().toString().trim();
            if(phone.length() == 0){
                mPhoneError.fillData(getResources().getString(R.string.text_sign_up_error_required_field));
                mPhoneError.setVisibility(VISIBLE);
                result = false;
            }else if(phone.length() < 4){
                mPhoneError.fillData(getResources().getString(R.string.text_sign_up_error_phone));
                mPhoneError.setVisibility(VISIBLE);
                result = false;
            }else{
                mPhoneError.setVisibility(GONE);
            }
        }else if(mRdbPhone.isChecked()){
            // Check phone
            String phone = mEdtPhoneNumber.getText().toString().trim();
            if(phone.length() == 0){
                mPhoneError.fillData(getResources().getString(R.string.text_sign_up_error_required_field));
                mPhoneError.setVisibility(VISIBLE);
                result = false;
            }else if(phone.length() < 4){
                mPhoneError.fillData(getResources().getString(R.string.text_sign_up_error_phone));
                mPhoneError.setVisibility(VISIBLE);
                result = false;
            }else{
                mPhoneError.setVisibility(GONE);
            }
        }else{
            // Check email and phone
            String email = mEdtEmail.getText().toString().trim();
            if(email.length() == 0){
                mEmailError.fillData(getResources().getString(R.string.text_sign_up_error_required_field));
                mEmailError.setVisibility(VISIBLE);
                result = false;
            }else if(!CommonUtils.emailValidator(email)){
                mEmailError.fillData(getResources().getString(R.string.text_sign_up_please_enter_a_valid_email_address));
                mEmailError.setVisibility(View.VISIBLE);
                result = false;
            }else{
                mEmailError.setVisibility(GONE);
            }
        }

        return result;
    }
    public void setEmail(String email){
        if(email== null){
            return;
        }
        else {
            mEdtEmail.setText(email);
        }
    }
    public void setFullPhoneNumber(String phoneNumber){
        String[] resource = getResources().getStringArray(R.array.country_arrays);
        String countrCode = "";
        if(!phoneNumber.contains("+")){
            phoneNumber="+"+phoneNumber;
        }
        for (String s : resource) {
            if (phoneNumber.startsWith(CommonUtils.getCountryCode(s))) {

                String countryNumber = CommonUtils.getCountryCode(s);
                if(countryNumber.length()>countrCode.length()){
                    countrCode = CommonUtils.getCountryCode(s);
                    mTvCountryNo.setText(countryNumber);
                    mTvCountryNo.setTextColor(ContextCompat.getColor(getContext(), R.color.text_color_active));
                    mEdtPhoneNumber.setText(phoneNumber.substring(countryNumber.length()));
                    mEdtPhoneNumber.setSelection(mEdtPhoneNumber.getText().toString().length());
                }

            }
        }
    }
    public void setReservationName(String reservationName){
        edtReservationName.setText(reservationName);
    }
    public void setCountryNo(String countryCode){
        if(CommonUtils.isStringValid(countryCode)) {
            mTvCountryNo.setText(CommonUtils.getCountryCode(countryCode));
            mTvCountryNo.setTextColor(ContextCompat.getColor(getContext(), R.color.text_color_active));
        }else{
            mTvCountryNo.setText(getResources().getString(R.string.text_sign_up_cc));
            mTvCountryNo.setTextColor(ContextCompat.getColor(getContext(),
                                                             R.color.gourmet_hint_text));
        }

    }
    public String getEmail(){
        return mEdtEmail.getText().toString().trim();
    }
    public String getFullPhone(){
        return mTvCountryNo.getText().toString().trim()+mEdtPhoneNumber.getText().toString().trim();
    }
    public String getReservationName(){return edtReservationName.getText().toString().trim();}
    public boolean isEmailChecked(){
        return mRdbEmail.isChecked();
    }
    public boolean isPhoneChecked(){
        return mRdbPhone.isChecked();
    }
    public boolean isBothChecked(){
        return mRdbBoth.isChecked();
    }
    public void setEmailChecked(Boolean checked){
        mRdbEmail.setChecked(checked);
    }
    public void setPhoneChecked(Boolean checked){
        mRdbPhone.setChecked(checked);
    }
    public void setBothChecked(Boolean checked){
        mRdbBoth.setChecked(checked);
    }
    public String getSelectedCountryCode(){
        String fullCountryCode = null;
        String[] countryCodeArr = getResources().getStringArray(R.array.country_arrays);
        for(String countryCode : countryCodeArr){
            String countryCodeInPara = countryCode;

            int openBracketStart = countryCode.indexOf("(");
            if(openBracketStart >= 0){
                countryCodeInPara = countryCode.substring(openBracketStart + 1, countryCode.length() - 1);
            }else{

            }
            if(countryCodeInPara.equalsIgnoreCase(mTvCountryNo.getText().toString().replace("+", ""))){
                fullCountryCode = countryCode;
            }
        }
        return fullCountryCode;
    }
}
