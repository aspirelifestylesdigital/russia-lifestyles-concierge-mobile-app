//
//  HomeViewController.h
//  ALC
//
//  Created by Hai NguyenV on 8/19/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "MainViewController.h"
#import "HomeGourmetItem.h"
#import "ErrorToolTip.h"
#import "WSCreateOrthersRequest.h"
@interface HomeViewController : MainViewController<DataLoadDelegate,UITextFieldDelegate,UIScrollViewDelegate,HomeItemDelegate>
{
    BOOL isConnecting;
    NSMutableArray* pArrRecommended;
    NSMutableArray* pRecommendedViews;
    int positionY;
}

@property (weak, nonatomic) IBOutlet UILabel *pLbUserName;
@property (weak, nonatomic) IBOutlet UIView *pvMainView;
@property (weak, nonatomic) IBOutlet UIView *pvRecommendView;
@property (weak, nonatomic) IBOutlet UIView *pvRecommendItemView;

@property (weak, nonatomic) IBOutlet UILabel *pLbWelcome;
@property (weak, nonatomic) IBOutlet UIScrollView *pScrollView;
@property (weak, nonatomic) IBOutlet UITextField *tfChatAsk;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pPositionY;
@property (weak, nonatomic) IBOutlet UIButton *btnArrow;
@property (weak, nonatomic) IBOutlet ErrorToolTip *icRequestMsgError;
@property (weak, nonatomic) IBOutlet UILabel *lbPlaceHolder;
@property (weak, nonatomic) IBOutlet UILabel *greetingMsgLbl;
@property (weak, nonatomic) IBOutlet UIButton *askUsBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UILabel *introduceMsgLbl;

- (IBAction)actionSubmit:(id)sender;
- (IBAction)actionCancel:(id)sender;
- (IBAction)actionShowRecom:(id)sender;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pHeightItemRecom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pPositionRecommen;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pPositionArrow;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pHeightMain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pHeightRecomm;
@end
