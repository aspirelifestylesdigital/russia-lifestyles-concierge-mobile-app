//
//  DCRItemDetailsController.h
//  ALC
//
//  Created by Anh Tran on 3/14/17.
//  Copyright © 2017 Sunrise Software Solutions. All rights reserved.
//

#import "MainViewController.h"
#import "WSDCRItemDetails.h"
#import "DCRItemDetails.h"
#import "DCRSubItemWithIconView.h"

@interface DCRItemDetailsController : MainViewController <DataLoadDelegate, UIWebViewDelegate>{
}
@property (strong, nonatomic) IBOutlet UIStackView *stackSubItems;

@property (nonatomic, strong) NSString* itemId;

-(void) goToRequestForm:(DCRItemDetails*) item;
- (void) displayItem:(DCRItemDetails*) item;
- (void) addWebSiteView:(NSString*) text;
- (void) addLocationView;
- (void) addFooterText;
- (void) addOpenHoursView;
- (void) addPrivilegeView;
- (void) addSubView:(NSString*)iconName withTitle:(NSString*)title value:(NSString*)value actionOnClicked:(void(^)())clicked;
- (void) hideSummaryView;

//Load Spa local data
extern NSMutableArray* initSpaList();
@end
