//
//  MMWebService.h
//  MobileMap
//
//  Created by Huy Tran on 5/10/12.
//  Copyright (c) 2012 S3Corp. All rights reserved.
//

#import "HGWBaseRequest.h"

@protocol WebServiceDelegate <NSObject>
-(void) processResults:(NSData*)data forTask:(NSInteger)task forSubTask:(NSInteger)subTask returnFormat:(NSInteger)returnFormat;
-(void) processResultWithError:(NSError *)errorCode;
@end

@interface HGWWebService : NSObject<NSURLConnectionDelegate,NSURLSessionDelegate>

{
    
	NSURLConnection* conn;
	NSMutableData* resultData;
    
	BOOL isActive;
	NSInteger task;
    NSInteger subTask;
    NSInteger contentType;
    NSURLSessionDataTask *sessionDataTask;
}

@property(assign) id<WebServiceDelegate> delegate;
@property(assign) NSInteger task;
@property(assign) NSInteger contentType;
@property(assign) NSInteger subTask;

-(void) invokeMethod:(NSString*)httpVerb andObject:(HGWBaseRequest*)obj forTask:(NSInteger)aTask forSubTask:(NSInteger)aSubTask;
-(void) invokeMethodImageUpload:(NSString*)httpVerb andObject:(HGWBaseRequest*)obj forTask:(NSInteger)aTask forSubTask:(NSInteger)aSubTask withData:(NSData*)dataImage;
-(BOOL) isRunning;
-(void) cancelRequest;


@end
