package com.aspire.android.russia.apiservices.b2c;

import com.aspire.android.russia.apiservices.apis.appimplement.b2c.B2CApiProviderClient;
import com.aspire.android.russia.apiservices.apis.appimplement.b2c.B2CBaseResponse;
import com.aspire.android.russia.apiservices.apis.appimplement.b2c.B2CICallback;
import com.aspire.android.russia.apiservices.b2c.RequestModel.B2CLoginRequest;
import com.aspire.android.russia.apiservices.b2c.ResponseModel.B2CLoginResponse;
import com.aspire.android.russia.application.AppConstant;
import com.aspire.android.russia.utils.CommonUtils;
import com.aspire.android.russia.utils.SharedPreferencesUtils;

import java.util.List;
import java.util.Map;

import retrofit2.Callback;

public class B2CWSLogin
        extends B2CApiProviderClient<B2CLoginResponse>{

    B2CLoginRequest loginRequest;

    public B2CWSLogin(B2CICallback callback){
        this.b2CICallback = callback;
        isCallbackInstantly = false;
    }
    @Override
    public void run(Callback<B2CLoginResponse> cb) {
        this.callback = cb;
        super.checkAuthenticateAndRun();
    }
    public void setLoginRequest(B2CLoginRequest loginRequest){
        this.loginRequest = loginRequest;
    }
    @Override
    protected boolean isUseAuthenticateInApi() {
        return false;
    }

    @Override
    protected void runApi() {
        serviceInterface.login(loginRequest).enqueue(this);
    }

    @Override
    protected Map<String, String> prepareHeaders() {
        return null;
    }

    @Override
    protected void postResponse(B2CLoginResponse response) {
        // Save online member id here
        SharedPreferencesUtils.setPreferences(AppConstant.PRE_B2C_ONLINE_MEMBER_ID, response.getOnlineMemberID());

        // Begin authentication
        B2CWSAuthenticate wsAuthenticate = new B2CWSAuthenticate();
        wsAuthenticate.run(new B2CICallback() {
            @Override
            public void onB2CResponse(B2CBaseResponse response) {
                if (b2CICallback != null) {
                    if(response != null) {
                        if(response.isSuccess()) {
                            // Get user detail
                            B2CWSGetUserDetails b2CWSGetUserDetails = new B2CWSGetUserDetails(new B2CICallback() {
                                @Override
                                public void onB2CResponse(B2CBaseResponse response) {
                                    if (response != null) {
                                        if (response.isSuccess()) {
                                            b2CICallback.onB2CResponse(callbackResponse);
                                        } else {
                                            b2CICallback.onB2CFailure(response.getMessage(), response.getErrorCode());
                                        }
                                    } else {
                                        b2CICallback.onB2CFailure(getMessageErrorFromLog(), getErrorCodeFromLog());
                                    }
                                }

                                @Override
                                public void onB2CResponseOnList(List<B2CBaseResponse> responseList) {

                                }

                                @Override
                                public void onB2CFailure(String errorMessage, String errorCode) {
                                    b2CICallback.onB2CFailure(errorMessage, errorCode);
                                }
                            });
                            b2CWSGetUserDetails.run(null);
                        }else{
                            // Reset all manage user preference
                            CommonUtils.resetManageUserPreference();
                            b2CICallback.onB2CFailure(getMessageErrorFromLog(), getErrorCodeFromLog());
                        }
                    }else{
                        // Reset all manage user preference
                        CommonUtils.resetManageUserPreference();
                        b2CICallback.onB2CFailure(getMessageErrorFromLog(), getErrorCodeFromLog());
                    }
                }
            }

            @Override
            public void onB2CResponseOnList(List<B2CBaseResponse> responseList) {

            }

            @Override
            public void onB2CFailure(String errorMessage, String errorCode) {
                b2CICallback.onB2CFailure(errorMessage, errorCode);
            }
        });
    }
}
