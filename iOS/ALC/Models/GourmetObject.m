//
//  GourmetObject.m
//  ALC
//
//  Created by Anh Tran on 8/23/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "GourmetObject.h"
#import "GourmetMenuObject.h"
#import "BranchObject.h"
#import "GalleryObject.h"
#import "NSString+HTML.h"

@implementation GourmetObject
-(id) initFromDict:(NSDictionary*) dict{
    self = [super init];
    self.restaurantName = [dict stringForKey:@"RestaurantName"];
    self.restaurantDescription = stringByStrippingHTML([[dict stringForKey:@"RestaurantDescription"] stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"]);
    self.priceRange = [dict stringForKey:@"PriceRange"];
    self.rating = [dict stringForKey:@"Rating"];
    if(self.rating.length>0){
        self.rating = [self.rating stringByAppendingString:@" Michelin Stars"];
    }
    self.ratingNumber = [dict stringForKey:@"Rating"];
    self.internalLabel = [dict stringForKey:@"InternalLabel"];
    
    self.ItemURL = [dict stringForKey:@"ItemUrl"];
    self.officalSiteUrl = [dict stringForKey:@"OfficalSiteUrl"];
    self.address = [dict stringForKey:@"Address"];
    self.zipCode = [dict stringForKey:@"Zipcode"];
    self.longitude = [dict stringForKey:@"Long"];
    self.lattitude = [dict stringForKey:@"Lat"];
    self.hoursOfOperation = stringByStrippingHTML([dict stringForKey:@"HoursOfOperation"]);
    self.restaurantID = [dict stringForKey:@"IDStr"];
    self.cityName = [dict stringForKey:@"CityName"];
    self.countryName = [dict stringForKey:@"CountryName"];
    self.countryCode = [dict stringForKey:@"CountryCode"];
    self.telephone = [dict stringForKey:@"Telephone"];
    self.backgroundImg = [dict stringForKey:@"BackgroundImageUrl"];
    self.termConditions = stringByStrippingHTML([[dict stringForKey:@"TermsCondition"] stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"]);
    self.gemContent = [dict stringForKey:@"InternalLabel"];
    self.dressCode = [dict stringForKey:@"DressCode"];
    self.email = [dict stringForKey:@"Email"];
    self.subTitle = [dict stringForKey:@"SubTitle"];
    self.isInterested = ![[dict stringForKey:@"AddWishListStatusToCSS"] isEqualToString:@""];
    self.cuisines = [dict arrayForKey:@"Cuisines"];
    
    NSArray* menus = [dict arrayForKey:@"Menu"];
    NSMutableArray *menusResult = [NSMutableArray array];
    for (int i = 0; i < menus.count; i++) {
        [menusResult addObject:[[GourmetMenuObject alloc] initFromDict:[menus objectAtIndex:i]]];
    }
    self.menu = menusResult;
    
    NSArray* br = [dict arrayForKey:@"Branches"];
    NSMutableArray *result = [NSMutableArray array];
    for (int i = 0; i < br.count; i++) {
        [result addObject:[[BranchObject alloc] initFromDict:[br objectAtIndex:i]]];
    }
    self.branches = result;
    
    NSArray* g = [dict arrayForKey:@"Galleries"];
    NSMutableArray *resultGallery = [NSMutableArray array];
    for (int i = 0; i < g.count; i++) {
        [resultGallery addObject:[[GalleryObject alloc] initFromDict:[g objectAtIndex:i]]];
    }
    if(resultGallery.count == 0){
        GalleryObject *item = [[GalleryObject alloc] init];
        item.backgroundImgURL = self.backgroundImg;
        item.captionTitle = @"";
        [resultGallery addObject:item];
    }
    self.galleries = resultGallery;

    self.occasions = [dict arrayForKey:@"Occations"];
    self.logoAlt = [dict stringForKey:@"LogoAlt"];
    self.logoSource = [DOMAIN_URL stringByAppendingString:[dict stringForKey:@"LogoSource"]];
    self.siteURL = [dict stringForKey:@"OfficalSiteUrl"];
    self.bookingURL = [dict stringForKey:@"BookingURL"];
    
    return self;
}

-(NSString*) getCuisineString{
    if(_cuisines!=nil && _cuisines.count>0){
        NSMutableString *result = [NSMutableString string];
        for (int i = 0 ; i < _cuisines.count - 1; i++) {
            [result appendString:[_cuisines objectAtIndex:i]];
            [result appendString:@", "];
        }
        
        [result appendString:[_cuisines objectAtIndex:_cuisines.count-1]];
        return result;
        
    }
    return @"";
}

-(NSString*) getDistance:(CLLocation*) currentLocation{
    if(_lattitude != nil && _lattitude.length>0 && _longitude!=nil && _longitude.length>0){
       CLLocationDistance distance = [currentLocation distanceFromLocation:[[CLLocation alloc] initWithLatitude:[_lattitude doubleValue]  longitude:[_longitude doubleValue]]];
       float fdistance =[[NSNumber numberWithDouble: distance] floatValue];
       return getDistance(fdistance);
    }
    return @"";
}

@end
