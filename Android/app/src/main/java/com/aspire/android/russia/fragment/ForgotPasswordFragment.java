package com.aspire.android.russia.fragment;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aspire.android.russia.R;
import com.aspire.android.russia.apiservices.apis.appimplement.BaseResponse;
import com.aspire.android.russia.apiservices.apis.appimplement.b2c.B2CBaseResponse;
import com.aspire.android.russia.apiservices.apis.appimplement.b2c.B2CICallback;
import com.aspire.android.russia.apiservices.b2c.B2CWSForgotPassword;
import com.aspire.android.russia.apiservices.b2c.RequestModel.B2CForgotPasswordRequest;
import com.aspire.android.russia.application.coreactivitys.BaseFragment;
import com.aspire.android.russia.utils.CommonUtils;
import com.aspire.android.russia.utils.FontUtils;
import com.aspire.android.russia.utils.SharedPreferencesUtils;
import com.aspire.android.russia.widgets.CustomErrorView;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTouch;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by anh.trinh on 9/13/2016.
 */
public class ForgotPasswordFragment
        extends BaseFragment
        implements Callback, B2CICallback {

    @BindView(R.id.ic_back)
    ImageView mIcBack;
    @BindView(R.id.tv_tool_bar_title)
    TextView mTvToolBarTitle;
    @BindView(R.id.img_mc_logo)
    ImageView mImgMcLogo;
    @BindView(R.id.tv_app_name)
    TextView mTvAppName;
    @BindView(R.id.tv_note_forgot_password)
    TextView mTvNoteForgotPassword;
    @BindView(R.id.edt_email)
    EditText mEdtEmail;
    @BindView(R.id.email_error)
    CustomErrorView mEmailError;
    @BindView(R.id.layout_email)
    LinearLayout mLayoutEmail;
    @BindView(R.id.btn_next)
    Button mBtnNext;
    @BindView(R.id.container)
    LinearLayout mContainer;

    @Override
    protected int layoutId() {
        return R.layout.fragment_forgot_password;
    }

    @Override
    protected void initView() {
        mTvToolBarTitle.setText(R.string.text_title_forgot_pass);
        mEmailError.setBackground(null);
        CommonUtils.setFontForViewRecursive(mContainer,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);
        CommonUtils.setFontForViewRecursive(mBtnNext,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
        CommonUtils.setFontForViewRecursive(mTvAppName,
                                            FontUtils.FONT_FILE_NAME_GOTHAM_ROUND_MEDIUM);
        CommonUtils.setFontForViewRecursive(mTvToolBarTitle,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
    }

    @Override
    protected void bindData() {

    }

    @Override
    public boolean onBack() {
        return false;
    }


  /*  @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater,
                                           container,
                                           savedInstanceState);
        ButterKnife.bind(this,
                         rootView);
        return rootView;
    }*/

    @OnClick({R.id.ic_back,
              R.id.btn_next})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ic_back:

                onBackPress();
                break;
            case R.id.btn_next:

                if (CommonUtils.emailValidator(mEdtEmail.getText()
                                                        .toString()
                                                        .trim())) {
                    showDialogProgress();

                    /*WSUserForgotPassword wsUserForgotPassword = new WSUserForgotPassword();
                    wsUserForgotPassword.login(mEdtEmail.getText()
                                                        .toString()
                                                        .trim());
                    wsUserForgotPassword.run(this);*/
                    B2CWSForgotPassword b2CWSForgotPassword = new B2CWSForgotPassword(this);
                    b2CWSForgotPassword.setRequest(new B2CForgotPasswordRequest().build(mEdtEmail.getText().toString().trim()));
                    b2CWSForgotPassword.run(null);

                } else {
                    mEmailError.fillData(getString(R.string.text_sign_up_error_email));
                    mEmailError.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

    @OnTouch({R.id.edt_email})
    boolean onInputTouchEmail(final View v,
                              final MotionEvent event) {
        mEmailError.setVisibility(View.GONE);

        return false;
    }

    @Override
    public void onResponse(final Call call,
                           final Response response) {
        if(getActivity()==null) {
            return;
        }
        if (response.body() instanceof BaseResponse) {
            hideDialogProgress();
            BaseResponse baseResponse = ((BaseResponse) response.body());
            int code = baseResponse.getStatus();
            if (code == 200) {

                pushFragment(new ForgotPasswordSuccessFragment(),
                             true,
                             true);

            } else {
                Bundle bundle = new Bundle();
                bundle.putString(ErrorFragment.PRE_ERROR_MESSAGE,
                                 getString(R.string.text_forgot_password_error_message));
                ErrorFragment fragment = new ErrorFragment();
                fragment.setArguments(bundle);
                pushFragment(fragment,
                             true,
                             true);

            }
        }
    }

    @Override
    public void onFailure(final Call call,
                          final Throwable t) {
        if(getActivity()==null) {
            return;
        }
        hideDialogProgress();
        Bundle bundle = new Bundle();
        bundle.putString(ErrorFragment.PRE_ERROR_MESSAGE,
                         getString(R.string.text_server_error_message));
        ErrorFragment fragment = new ErrorFragment();
        fragment.setArguments(bundle);
        pushFragment(fragment,
                     true,
                     true);
    }

    @Override
    public void onB2CResponse(B2CBaseResponse response) {
        hideDialogProgress();
        if(getActivity()==null) {
            return;
        }
        if(response != null && response.isSuccess()){
            // Push to share preference about the email address of account as forgot password
            SharedPreferencesUtils.setPreferences(mEdtEmail.getText().toString(), true);

            onBackPress();
            pushFragment(new ForgotPasswordSuccessFragment(),
                    true,
                    true);
        }else{
            Bundle bundle = new Bundle();
            bundle.putString(ErrorFragment.PRE_ERROR_MESSAGE,
                    getString(R.string.text_forgot_password_error_message));
            ErrorFragment fragment = new ErrorFragment();
            fragment.setArguments(bundle);
            pushFragment(fragment,
                    true,
                    true);
        }
    }

    @Override
    public void onB2CResponseOnList(List<B2CBaseResponse> responseList) {
        if(getActivity()==null) {
            return;
        }
    }

    @Override
    public void onB2CFailure(String errorMessage, String errorCode) {
        hideDialogProgress();
        if(getActivity()==null) {
            return;
        }
        Bundle bundle = new Bundle();
        bundle.putString(ErrorFragment.PRE_ERROR_MESSAGE,
                getString(R.string.text_forgot_password_error_message));
        ErrorFragment fragment = new ErrorFragment();
        fragment.setArguments(bundle);
        pushFragment(fragment,
                true,
                true);
    }
}
