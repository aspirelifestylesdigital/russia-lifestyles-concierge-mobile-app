package com.aspire.android.russia.apiservices.b2c;

import com.aspire.android.russia.BuildConfig;
import com.aspire.android.russia.apiservices.apis.appimplement.ApiInterface;
import com.aspire.android.russia.apiservices.apis.appimplement.b2c.B2CApiProviderClient;
import com.aspire.android.russia.apiservices.apis.appimplement.b2c.B2CICallback;
import com.aspire.android.russia.apiservices.b2c.ResponseModel.B2CRequestTokenResponse;
import com.aspire.android.russia.application.AppConstant;
import com.aspire.android.russia.utils.SharedPreferencesUtils;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Callback;

public class B2CWSRequestToken
        extends B2CApiProviderClient<B2CRequestTokenResponse>{

    public B2CWSRequestToken(B2CICallback callback){
        this.b2CICallback = callback;
    }
    @Override
    public void run(Callback<B2CRequestTokenResponse> cb) {
        this.callback = cb;
        super.checkAuthenticateAndRun();
    }
    @Override
    protected boolean isUseAuthenticateInApi() {
        return false;
    }

    @Override
    protected void runApi() {
        serviceInterface.requestToken(BuildConfig.B2C_CALL_BACK_URL,
                                    BuildConfig.B2C_MEMBER_DEVICE_ID,
                        SharedPreferencesUtils.getPreferences(AppConstant.PRE_B2C_ONLINE_MEMBER_ID, "")).enqueue(this);
    }

    @Override
    protected Map<String, String> prepareHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put(ApiInterface.WS_QUERY_CONSUMER_KEY, BuildConfig.B2C_CONSUMER_KEY);
        headers.put(ApiInterface.WS_QUERY_CONSUMER_SECRET, BuildConfig.B2C_CONSUMER_SECRECT);
        return headers;
    }

    @Override
    protected void postResponse(B2CRequestTokenResponse response) {
        // Save online member id here
        SharedPreferencesUtils.setPreferences(AppConstant.PRE_B2C_REQUEST_TOKEN, response.getRequestToken());
    }
}
