//
//  CountryObject.h
//  ALC
//
//  Created by Anh Tran on 10/6/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CountryObject : NSObject
@property (strong, nonatomic) NSString* countryId;
@property (strong, nonatomic) NSString* countryCode;
@property (strong, nonatomic) NSString* countryName;
@property (strong, nonatomic) NSString* region;
-(id) initFromDict:(NSDictionary*) dict;
@end
