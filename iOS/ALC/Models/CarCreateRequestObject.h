//
//  CarCreateRequestObject.h
//  ALC
//
//  Created by Anh Tran on 10/10/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PhotoResizedInfo.h"
#import "CarRentalDetailsObject.h"
#import "CarTransferDetailsObject.h"
@interface CarCreateRequestObject : NSObject
@property (strong, nonatomic) NSString* bookingId;
@property (strong, nonatomic) NSString* userId;
@property (strong, nonatomic) NSString* bookingItemId;
@property (strong, nonatomic) NSString* email;
@property (strong, nonatomic) NSString* mobileNumber;
@property (strong, nonatomic) NSString* pickUpDate;
@property (strong, nonatomic) NSString* pickUpTime;
@property (strong, nonatomic) NSString* dropOffDate;
@property (strong, nonatomic) NSString* dropOffTime;
@property (strong, nonatomic) NSString* driverName;
@property (strong, nonatomic) NSString* pickUpLocation;
@property (strong, nonatomic) NSString* dropOffLocation;
@property (strong, nonatomic) NSMutableArray* preferredVerhicle;
@property (strong, nonatomic) NSMutableArray* preferredCarRentals;
@property (strong, nonatomic) NSString* maximumPrice;
@property (strong, nonatomic) NSString* transportType;
@property ( nonatomic) BOOL isInternationalLicense;
@property ( nonatomic) NSInteger nbOfPassengers;
@property ( nonatomic) NSInteger driverAge;
@property (strong, nonatomic) PhotoResizedInfo* photo;
@property ( nonatomic) BOOL isContactPhone;
@property ( nonatomic) BOOL isContactEmail;
@property ( nonatomic) BOOL isContactBoth;
@property (strong, nonatomic) NSString* specialMessage;
@property (strong, nonatomic) NSString* EditType;
@property (strong, nonatomic) NSString* reservationName;

-(id) initRentalFromDetails:(CarRentalDetailsObject*)detailsObj;
-(id) initTransferFromDetails:(CarTransferDetailsObject*)detailsObj;
@end