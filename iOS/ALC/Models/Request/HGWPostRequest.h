//
//  HGWPostRequest.h
//  HungryGoWhere
//
//  Created by Linh Le on 1/10/13.
//  Copyright (c) 2013 Linh Le. All rights reserved.
//

#import "HGWBaseRequest.h"
#import "HGWPostBaseRequest.h"

@interface HGWPostRequest : HGWPostBaseRequest

@end
