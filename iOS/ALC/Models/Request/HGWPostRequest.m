//
//  HGWPostRequest.m
//  HungryGoWhere
//
//  Created by Linh Le on 1/10/13.
//  Copyright (c) 2013 Linh Le. All rights reserved.
//

#import "HGWPostRequest.h"
#import "SBJson.h"
#import "NSString+MD5.h"

@implementation HGWPostRequest

- (NSString*) requestString
{    
    if (self.dictKeyValues == nil) {
        return nil;
    }
    
    NSString *requestString=@"";
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self.dictKeyValues
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    
    if (! jsonData) {
        
    } else {
        requestString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    return requestString;
}

-(NSString*) getSignature
{
    if (self.dictKeyValues == nil) {
        return nil;
    }
    
    NSString *parameter = [self.dictKeyValues JSONRepresentation];
    return [self getAPISignature:parameter secret:self.secret];
}

@end
