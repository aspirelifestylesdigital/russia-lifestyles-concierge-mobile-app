//
//  ChatBotViewController.m
//  ALC
//
//  Created by Nhat Huy Truong  on 9/12/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "ChatBotViewController.h"
#import "ChatBotObject.h"
#import "InAppBrowserViewController.h"
#import "UIView+DCAnimationKit.h"


#define TIME_DELAY 60
@interface ChatBotViewController ()

@end

@implementation ChatBotViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    isFreeTime = YES;
    isWarning = NO;
    
    isNeedProvideLocation = NO;
    
    if (![stringFromObject(self.firstText) isEqualToString:@""])
    {
        [self.txtChat setText:self.firstText];
    }
    
    [self addTitleToHeader:NSLocalizedString(@"ASK ALEXIS", nil) withColor:HEADER_COLOR];
    [self customButtonBack];
    
    currentHeight = 0.0;
    self.arrayChatBot = [[NSMutableArray alloc] init];
    
    [self.txtChat setValue:[UIColor darkGrayColor]
                    forKeyPath:@"_placeholderLabel.textColor"];
    
    isFirst = YES;
    
    // Do any additional setup after loading the view, typically from a nib.
    
    currentLocation = [[CLLocationManager alloc] init];
    currentLocation.delegate = self;
    currentLocation.desiredAccuracy = kCLLocationAccuracyThreeKilometers;
    if([currentLocation respondsToSelector:@selector(requestWhenInUseAuthorization)])
    {
        canGetLocation = NO;
        [currentLocation requestWhenInUseAuthorization];
    }
    else
    {
        canGetLocation = YES;
        [currentLocation startUpdatingLocation];
    }
    
}

- (void) showWarningChat
{
    if (!isWarning)
    {
        isWarning = YES;
        ChatBotObject*itemChat = [[ChatBotObject alloc] init];
        itemChat.valueContent = @"I didn't quite get that. Would you like to speak to our concierge consultant?";
        itemChat.isBot = YES;
        
        ChatButtonObject* buttonCall = [[ChatButtonObject alloc] init];
        buttonCall.action = @"link_to_xxx";
        buttonCall.type = @"callus";
        buttonCall.textButton = @"Call Us";
        [itemChat.listButtons addObject:buttonCall];
        
        ChatButtonObject* buttonLiveChat = [[ChatButtonObject alloc] init];
        buttonLiveChat.action = @"link_to_yyyy";
        buttonLiveChat.type = @"livechat";
        buttonLiveChat.textButton = @"Live Chat";
        [itemChat.listButtons addObject:buttonLiveChat];
        
        [self loadViewChatToMainViewChatWithObjectChat:itemChat];
    }
}


- (void) checkUserStillActive
{
    currentSecond += 1;
    if (currentSecond > TIME_DELAY)
    {
        if (isFreeTime && !isWarning)
        {
            [self showWarningChat];
            currentSecond = 0;
        }
        else
        {
            currentSecond = 0;
        }
    }
    else
    {
        if (!isFreeTime)
        {
            currentSecond = 0;
        }
    }
}


-(BOOL)isShowConciergeButton{
    return NO;
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    isWarning = NO;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    if (isFirst == YES)
    {
        [self callChatBotForText:@"hello"];
        isFirst = NO;
    }
    currentSecond = 0;
    timeCount = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(checkUserStillActive) userInfo:nil repeats:YES];
    [timeCount fire];
    
    
    timeShake = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(shake) userInfo:nil repeats:YES];
    [timeShake fire];
//    [self hardcodeDataChatBot];
}



- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self.txtChat resignFirstResponder];
    if (timeCount)
    {
        [timeCount invalidate];
        timeCount = nil;
    }
    if (timeShake)
    {
        [timeShake invalidate];
        timeShake = nil;
    }
}

- (void) lockPreviousViewChatBot
{
    UIView* viewBot = [self.vwMainView.subviews lastObject];
    if ([viewBot isKindOfClass:[CellView_Bot class]])
    {
        [(CellView_Bot*)viewBot lockView];
    }
}

- (void) keyboardDidShow:(NSNotification *)notification
{
    NSDictionary* info = [notification userInfo];
    CGRect kbRect = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    kbRect = [self.view convertRect:kbRect fromView:nil];
    
//    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbRect.size.height, 0.0);
//    self.scrollView.contentInset = contentInsets;
//    self.scrollView.scrollIndicatorInsets = contentInsets;
//    
//    CGRect aRect = self.view.frame;
//    aRect.size.height -= kbRect.size.height;
//    if (!CGRectContainsPoint(aRect, self.txtChat.frame.origin) ) {
//        [self.scrollView scrollRectToVisible:self.txtChat.frame animated:YES];
//    }
    
    [UIView animateWithDuration:1.0 animations:^{
        self.layoutSpaceWithBottom.constant = kbRect.size.height;
    } completion:^(BOOL finished) {
        [self.scrollView scrollRectToVisible:CGRectMake(0, self.scrollView.contentSize.height - 1, 1, 1) animated:NO];
    }];
   
}

- (void) keyboardWillBeHidden:(NSNotification *)notification
{
//    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
//    self.scrollView.contentInset = contentInsets;
//    self.scrollView.scrollIndicatorInsets = contentInsets;
    [UIView animateWithDuration:1.0 animations:^{
        self.layoutSpaceWithBottom.constant = 0;
    } completion:^(BOOL finished) {
        [self.scrollView scrollRectToVisible:CGRectMake(0, self.scrollView.contentSize.height - 1, 1, 1) animated:NO];
    }];
}


- (id) createNewCustomObjectViewFromNibName:(NSString*)nibName owner:(id)owner withClass:(Class)classView
{
    NSArray* nibContents = [[NSBundle mainBundle]
                            loadNibNamed:nibName owner:owner options:NULL];
    
    NSEnumerator *nibEnumerator = [nibContents objectEnumerator];
    NSObject* nibItem = nil;
    
    while ( (nibItem = [nibEnumerator nextObject]) != nil) {
        
        if ( [nibItem isKindOfClass: classView])
        {
            return nibItem;
        }
    }
    return nil;
}


- (void) loadScrollViewChatToMainViewChatWithObjectChat:(ChatBotObject*)objectChat
{
    
    CellScrollViewBot* vwBot = (CellScrollViewBot*)[self createNewCustomObjectViewFromNibName:@"CellScrollViewBot" owner:self withClass:[CellScrollViewBot class]];
    vwBot.currentItemChat = objectChat;
    vwBot.arrayListShow = [[NSMutableArray alloc] init];
    vwBot.arrayListShow = objectChat.listRestaurant;
    vwBot.isShowImages = NO;
    
    [vwBot setFrame:CGRectMake(0, 0, self.vwMainView.frame.size.width, vwBot.frame.size.height)];
    [self lockPreviousViewChatBot];
    [self.vwMainView addSubview:vwBot];
    [vwBot loadView];
    
    [vwBot setFrame:CGRectMake(0, currentHeight, self.vwMainView.frame.size.width, vwBot.frame.size.height)];
    currentHeight += vwBot.frame.size.height;
    
    self.layoutHeightMainView.constant = currentHeight;
    
    [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, currentHeight)];
    [self.scrollView scrollRectToVisible:vwBot.frame animated:NO];
}

- (void) loadScrollViewImageChatToMainViewChatWithObjectChat:(ChatBotObject*)objectChat
{
    
    CellScrollViewBot* vwBot = (CellScrollViewBot*)[self createNewCustomObjectViewFromNibName:@"CellScrollViewBot" owner:self withClass:[CellScrollViewBot class]];
    vwBot.currentItemChat = objectChat;
    vwBot.arrayListShow = [[NSMutableArray alloc] init];
    vwBot.arrayListShow = objectChat.listImageURL;
    vwBot.isShowImages = YES;
    
    [vwBot setFrame:CGRectMake(0, 0, self.vwMainView.frame.size.width, vwBot.frame.size.height)];
    [self lockPreviousViewChatBot];
    [self.vwMainView addSubview:vwBot];
    [vwBot loadView];
    
    [vwBot setFrame:CGRectMake(0, currentHeight, self.vwMainView.frame.size.width, vwBot.frame.size.height)];
    currentHeight += vwBot.frame.size.height;
    
    self.layoutHeightMainView.constant = currentHeight;
    
    [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, currentHeight)];
    [self.scrollView scrollRectToVisible:vwBot.frame animated:NO];
}

- (void) loadViewChatToMainViewChatWithObjectChat:(ChatBotObject*)objectChat
{
    
    if (objectChat.isBot)
    {
        CellView_Bot* vwBot = (CellView_Bot*)[self createNewCustomObjectViewFromNibName:@"CellView_Bot" owner:self withClass:[CellView_Bot class]];
        vwBot.currentChat = objectChat;
        vwBot.delegate = self;
        [vwBot setFrame:CGRectMake(0, 0, self.vwMainView.frame.size.width, vwBot.frame.size.height)];
        
        [self lockPreviousViewChatBot];
        [self.vwMainView addSubview:vwBot];
        [vwBot loadView];
        
        [vwBot setFrame:CGRectMake(0, currentHeight, self.vwMainView.frame.size.width, vwBot.frame.size.height)];
        currentHeight += vwBot.frame.size.height;
        
        self.layoutHeightMainView.constant = currentHeight;
        [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, currentHeight)];
        [self.scrollView scrollRectToVisible:vwBot.frame animated:NO];
        
    }
    else
    {
        CellView_User* vwUser = (CellView_User*)[self createNewCustomObjectViewFromNibName:@"CellView_User" owner:self withClass:[CellView_User class]];
        vwUser.currentChat = objectChat;
        
        [vwUser setFrame:CGRectMake(0, 0, self.vwMainView.frame.size.width, vwUser.frame.size.height)];
        
        [self lockPreviousViewChatBot];
        [self.vwMainView addSubview:vwUser];
        [vwUser layoutIfNeeded];
        [vwUser loadView];
        
        [vwUser setFrame:CGRectMake(0, currentHeight, self.vwMainView.frame.size.width, vwUser.frame.size.height)];
        currentHeight += vwUser.frame.size.height;
        
        self.layoutHeightMainView.constant = currentHeight;
        [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, currentHeight)];
        [self.scrollView scrollRectToVisible:vwUser.frame animated:NO];
    }
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITextField Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    currentSecond = 0;
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    if (![stringFromObject(textField.text) isEqualToString:@""])
    {
        ChatBotObject* itemChat = [[ChatBotObject alloc] init];
        itemChat.isBot = NO;
        itemChat.valueContent = self.txtChat.text;
        [self loadViewChatToMainViewChatWithObjectChat:itemChat];
        
        if ([[self.txtChat.text lowercaseString] isEqualToString:@"hi"] || [[self.txtChat.text lowercaseString] isEqualToString:@"hello"])
        {
            if (isFirst)
            {
                [self callChatBotForText:self.txtChat.text];
            }
            else
            {
                isWarning = NO;
                ChatBotObject* itemChat = [[ChatBotObject alloc] init];
                itemChat.isBot = YES;
                itemChat.valueContent = @"Hi";
                [self loadViewChatToMainViewChatWithObjectChat:itemChat];
            }
        }
        else
        {
            [self callChatBotForText:self.txtChat.text];
        }
        self.txtChat.text = nil;
    }
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



#pragma mark - Method Custom

- (void) shake
{
//    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"transform.translation.x"];
//    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
//    animation.duration = 0.6;
//    animation.values = @[ @(-20), @(20), @(-20), @(20), @(-10), @(10), @(-5), @(5), @(0) ];
//    [self.imvSpoon.layer addAnimation:animation forKey:@"shake"];
    [self.imvSpoon swing:NULL];
}

-(NSString *)encodeUrlString:(NSString *)string {
    NSCharacterSet *set = [NSCharacterSet URLHostAllowedCharacterSet];
    NSString *result = [string stringByAddingPercentEncodingWithAllowedCharacters:set];
    return result;
}

- (void) callChatBotForText:(NSString*)textSearch
{
    isFreeTime = NO;
    isWarning = NO;
    NSMutableDictionary* dictionParam = [[NSMutableDictionary alloc] init];
    [dictionParam setObject:[NSNumber numberWithInteger:1] forKey:@"bot_id"];
    if (convo_id && ![convo_id isEqualToString:@""])
    {
        [dictionParam setObject:convo_id forKey:@"convo_id"];
    }
    
    [dictionParam setObject:@"json" forKey:@"format"];
    [dictionParam setObject:[self encodeUrlString:textSearch] forKey:@"say"];
    AppDelegate* appdele=(AppDelegate*) [[UIApplication sharedApplication] delegate];
    if([appdele isLoggedIn])
    {
        UserObject* currentUser = getUser();
        [dictionParam setObject:[self encodeUrlString:stringFromObject(currentUser.userId)]forKey:@"uid"];
        [dictionParam setObject:[self encodeUrlString:stringFromObject(currentUser.fullName)]forKey:@"usName"];
    }
    
    
    if (isNeedProvideLocation)
    {
        [dictionParam setObject:@"1" forKey:@"location_request"];
        isNeedProvideLocation = NO;
    }
    else
    {
        [dictionParam setObject:@"0" forKey:@"location_request"];
    }
    
    [self showToast];
    
    
    
    [currentLocation startUpdatingLocation];
    if (currentLocation.location)
    {
        CLGeocoder * geoCoder = [[CLGeocoder alloc] init];
        [geoCoder reverseGeocodeLocation:currentLocation.location
                       completionHandler:^(NSArray *placemarks, NSError *error)
        {
            if (!error)
            {
                for (CLPlacemark *placemark in placemarks)
                {
                    
                    NSLog(@"%@",[placemark locality]);
                    
                    CLPlacemark *placemark = [placemarks objectAtIndex:0];
                    
                    NSLog(@"placemark.ISOcountryCode %@",placemark.ISOcountryCode);
                    NSLog(@"placemark.country %@",placemark.country);
                    NSLog(@"placemark.postalCode %@",placemark.postalCode);
                    NSLog(@"placemark.administrativeArea %@",placemark.administrativeArea);
                    NSLog(@"placemark.locality %@",placemark.locality);
                    NSLog(@"placemark.subLocality %@",placemark.subLocality);
                    NSLog(@"placemark.subThoroughfare %@",placemark.subThoroughfare);
                    
                    if (![stringFromObject(placemark.country) isEqualToString:@""])
                    {
                        [dictionParam setObject:[self encodeUrlString:stringFromObject(placemark.country)]forKey:@"countries"];
                    }
                    
                    if (![stringFromObject(placemark.administrativeArea) isEqualToString:@""])
                    {
//                        if ([placemark.administrativeArea rangeOfString:@"Da Nang"].location != NSNotFound)
//                        {
//                            [dictionParam setObject:@"Da Nang"] forKey:@"cities"];
//                        }
//                        else
//                        {
                        if ([placemark.administrativeArea rangeOfString:@"Đà Nẵng"].location != NSNotFound)
                        {
                            [dictionParam setObject:[self encodeUrlString:@"Da Nang"] forKey:@"cities"];
                        }
                        else if ([placemark.administrativeArea rangeOfString:@"Ho Chi Minh City"].location != NSNotFound)
                        {
                            [dictionParam setObject:[self encodeUrlString:@"Ho Chi Minh"] forKey:@"cities"];
                        }
                        else
                        {
                            [dictionParam setObject:[self encodeUrlString:stringFromObject(placemark.administrativeArea)] forKey:@"cities"];
                        }
//                        }

                    }
                    
                    [dictionParam setObject:[NSNumber numberWithDouble:currentLocation.location.coordinate.longitude] forKey:@"longt"];
                    [dictionParam setObject:[NSNumber numberWithDouble:currentLocation.location.coordinate.latitude] forKey:@"lat"];
                    
                    [dictionParam setObject:@"1" forKey:@"location"];
                    
                    callAPINoHeaderForGET(API_CALL_CHAT,dictionParam,self,@selector(getBotResponseComplete:),@selector(getBotResponseFail:));
                    return ;
                }
            }
            else
            {
                if([appdele isLoggedIn])
                {
                    UserObject* currentUser = getUser();
                    if (![stringFromObject(currentUser.currentCountry) isEqualToString:@""])
                    {
                        [dictionParam setObject:[self encodeUrlString:stringFromObject(currentUser.currentCountry)]forKey:@"countries"];
                    }
                    
                    if (![stringFromObject(currentUser.currentCity) isEqualToString:@""])
                    {
                        [dictionParam setObject:[self encodeUrlString:stringFromObject(currentUser.currentCity)]forKey:@"cities"];
                    }
                    if (![stringFromObject(currentUser.currentCity) isEqualToString:@""] || ![stringFromObject(currentUser.currentCountry) isEqualToString:@""])
                    {
                        [dictionParam setObject:@"1" forKey:@"location"];
                    }
                    else
                    {
                        [dictionParam setObject:@"0" forKey:@"location"];
                    }
                }
                else
                {
                    [dictionParam setObject:@"0" forKey:@"location"];
                }
                callAPINoHeaderForGET(API_CALL_CHAT,dictionParam,self,@selector(getBotResponseComplete:),@selector(getBotResponseFail:));
                return ;
            }
        }];
    }
    else
    {
         if([appdele isLoggedIn])
         {
             UserObject* currentUser = getUser();
             if (![stringFromObject(currentUser.currentCountry) isEqualToString:@""])
             {
                  [dictionParam setObject:[self encodeUrlString:stringFromObject(currentUser.currentCountry)]forKey:@"countries"];
             }
             
             if (![stringFromObject(currentUser.currentCity) isEqualToString:@""])
             {
                  [dictionParam setObject:[self encodeUrlString:stringFromObject(currentUser.currentCity)]forKey:@"cities"];
             }
             if (![stringFromObject(currentUser.currentCity) isEqualToString:@""] || ![stringFromObject(currentUser.currentCountry) isEqualToString:@""])
             {
                 [dictionParam setObject:@"1" forKey:@"location"];
             }
             else
             {
                 [dictionParam setObject:@"0" forKey:@"location"];
             }
         }
        else
        {
            [dictionParam setObject:@"0" forKey:@"location"];
        }
        callAPINoHeaderForGET(API_CALL_CHAT,dictionParam,self,@selector(getBotResponseComplete:),@selector(getBotResponseFail:));
        return ;
    }
}

- (IBAction)clickSendMessage:(id)sender
{
    if (![stringFromObject(self.txtChat.text) isEqualToString:@""])
    {
        ChatBotObject* itemChat = [[ChatBotObject alloc] init];
        itemChat.isBot = NO;
        itemChat.valueContent = self.txtChat.text;
        [self loadViewChatToMainViewChatWithObjectChat:itemChat];
        
        if ([[self.txtChat.text lowercaseString] isEqualToString:@"hi"] || [[self.txtChat.text lowercaseString] isEqualToString:@"hello"])
        {
            if (isFirst)
            {
                [self callChatBotForText:self.txtChat.text];
            }
            else
            {
                isWarning = NO;
                ChatBotObject* itemChat = [[ChatBotObject alloc] init];
                itemChat.isBot = YES;
                itemChat.valueContent = @"Hi";
                [self loadViewChatToMainViewChatWithObjectChat:itemChat];
            }
        }
       else
       {
            [self callChatBotForText:self.txtChat.text];
       }
     self.txtChat.text = nil;
    }
}

- (IBAction)clickShowOrCloseKeyBoard:(id)sender
{
    if ([self.txtChat isFirstResponder])
    {
        [self.txtChat resignFirstResponder];
    }
    else
    {
        [self.txtChat becomeFirstResponder];
    }
}


//bot_id=1&say=DA%20NANG%20RESTAURANT&convo_id=bq94sbpmcotcmfqmebure48c66&format=json

- (IBAction)clickListRestaurant:(id)sender
{
    isFreeTime = NO;
    isWarning = NO;
    isNeedProvideLocation = NO;
    [self callChatBotForText:@"recommend"];
}

#pragma mark Call API




#pragma mark Response API

-(void) getBotResponseComplete:(NSData*)data
{
    id jsonObject=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
    if ([jsonObject isKindOfClass:[NSDictionary class]])
    {
        NSLog(@"%@",jsonObject);
        NSString* status = stringFromObject([jsonObject objectForKey:@"status"]);
        if ([status isEqualToString:@"200"])
        {
            if (!convo_id)
            {
                convo_id = stringFromObject([jsonObject objectForKey:@"convo_id"]);
                
            }
            NSDictionary* dataRes = [jsonObject objectForKey:@"botsay"];
            if (dataRes && [dataRes isKindOfClass:[NSDictionary class]])
            {
                NSString* typeParser = stringFromObject([dataRes objectForKey:@"type"]);
                if ([typeParser isEqualToString:@"api"] || [typeParser isEqualToString:@"array"])
                {
                    [self createItemChatViewArray:dataRes];
                }
                else if ([typeParser isEqualToString:@"normal"] || [typeParser isEqualToString:@"nonrelated"])
                {
                    if ([typeParser isEqualToString:@"nonrelated"])
                    {
                        isWarning = YES;
                    }
                    [self createItemChatViewNormal:dataRes];
                }
                else if ([typeParser isEqualToString:@"greeting"])
                {
                    [self createItemChatViewRecommended:dataRes];
                }
                else if ([typeParser isEqualToString:@"recommend"])
                {
                    [self createItemChatViewNormal:dataRes];
                }
                else if ([typeParser isEqualToString:@"location_request"])
                {
                    isNeedProvideLocation = YES;
                    [self createItemChatViewNormal:dataRes];
                }
                else 
                {
                    [self showWarningChat];
                }
            }
        }
        else
        {
            [self showWarningChat];
        }
    }
    else
    {
        [self showWarningChat];
    }
    [self stopToast];
    isFreeTime = YES;
}

-(void) getBotResponseFail:(NSError*)error
{
    [self stopToast];
    NSLog(@"%@",error);
    //cal again
    isFreeTime = YES;
   [self showWarningChat];
    return;
}


- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential *))completionHandler{
    if([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust])
    {
        NSLog(@"Trust:%@",challenge.protectionSpace.host);
        if([challenge.protectionSpace.host isEqualToString:@"chatbot.s3corp.com.vn"]){
            NSURLCredential *credential = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
            completionHandler(NSURLSessionAuthChallengeUseCredential,credential);
        }
    }
}

#pragma mark - Create ItemChat

- (void) createItemChatViewNormal:(NSDictionary*)jsonObject
{
    ChatBotObject* itemChat = [[ChatBotObject alloc] init];
    itemChat.isBot = YES;
    itemChat.valueContent = stringFromObject([jsonObject objectForKey:@"text"]);
    if ([itemChat.valueContent isEqualToString:@""])
    {
        [self showWarningChat];
    }
    else
    {
        NSArray* arrayObject = [jsonObject objectForKey:@"buttons"];
        if (arrayObject && [arrayObject isKindOfClass:[NSArray class]])
        {
            for (NSDictionary* itemButton in arrayObject)
            {
                [itemChat.listButtons addObject:[ChatButtonObject createObjectWithDic:itemButton]];
            }
        }
        [self loadViewChatToMainViewChatWithObjectChat:itemChat];
    }
}

- (void) createItemChatViewArray:(NSDictionary*)jsonObject
{
    NSDictionary* dicObjectServer = [jsonObject objectForKey:@"data"];
    if (dicObjectServer && [dicObjectServer isKindOfClass:[NSDictionary class]])
    {
        NSArray* arrayData = [dicObjectServer objectForKey:@"ListData"];
        if (arrayData && [arrayData isKindOfClass:[NSArray class]])
        {
            ChatBotObject* itemChat = [[ChatBotObject alloc] init];
            itemChat.isBot = YES;
            for (NSDictionary* itemRestaurant in arrayData)
            {
                [itemChat.listRestaurant addObject:[[GourmetObject alloc] initFromDict:itemRestaurant]];
            }
            if ([itemChat.listRestaurant count] > 0)
            {
                [self loadScrollViewChatToMainViewChatWithObjectChat:itemChat];
            }
        }
        else
        {
            [self showWarningChat];
        }
    }
    else if (dicObjectServer && [dicObjectServer isKindOfClass:[NSArray class]])
    {
        if ([dicObjectServer count] > 0)
        {
            ChatBotObject* itemChat = [[ChatBotObject alloc] init];
            itemChat.isBot = YES;
            for (NSDictionary* itemRestaurant in dicObjectServer)
            {
                [itemChat.listRestaurant addObject:[[GourmetObject alloc] initFromDict:itemRestaurant]];
            }
            if ([itemChat.listRestaurant count] > 0)
            {
                [self loadScrollViewChatToMainViewChatWithObjectChat:itemChat];
            }
        }
        else
        {
            ChatBotObject* itemChat = [[ChatBotObject alloc] init];
            itemChat.isBot = YES;
            itemChat.valueContent = @"No data restaurants match. Please try again.";
            [self loadViewChatToMainViewChatWithObjectChat:itemChat];
        }
    }
    else
    {
        [self showWarningChat];
    }
}


- (void) createItemChatViewRecommended:(NSDictionary*)jsonObject
{
    ChatBotObject* itemChat = [[ChatBotObject alloc] init];
    itemChat.isBot = YES;
    itemChat.valueContent = stringFromObject([jsonObject objectForKey:@"text"]);
    if ([itemChat.valueContent isEqualToString:@""])
    {
        [self showWarningChat];
    }
    else
    {
        [self loadViewChatToMainViewChatWithObjectChat:itemChat];
        ChatBotObject* itemImageChat = [[ChatBotObject alloc] init];
        itemImageChat.isBot = YES;
        
        NSArray* arrayObject = [jsonObject objectForKey:@"data"];
        if (arrayObject && [arrayObject isKindOfClass:[NSArray class]])
        {
            for (NSString* itemImage in arrayObject)
            {
                [itemImageChat.listImageURL addObject:itemImage];
            }
            [self loadScrollViewImageChatToMainViewChatWithObjectChat:itemImageChat];
        }
        
    }
}

#pragma mark - Location Delegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    canGetLocation = NO;
}
-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    switch (status) {
        case kCLAuthorizationStatusNotDetermined:
        case kCLAuthorizationStatusRestricted:
        case kCLAuthorizationStatusDenied:
        {
            // do some error handling
            canGetLocation = NO;
        }
            break;
        default:{
            [currentLocation startUpdatingLocation];
            canGetLocation = YES;
        }
            break;
    }
}
- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations
{
//    CLLocation *location = [locations lastObject];
//    CLGeocoder * geoCoder = [[CLGeocoder alloc] init];
//    [geoCoder reverseGeocodeLocation:location
//                   completionHandler:^(NSArray *placemarks, NSError *error) {
//                       for (CLPlacemark *placemark in placemarks) {
//                           
//                           NSLog(@"%@",[placemark locality]);
//                           
//                           CLPlacemark *placemark = [placemarks objectAtIndex:0];
//                           
//                           NSLog(@"placemark.ISOcountryCode %@",placemark.ISOcountryCode);
//                           NSLog(@"placemark.country %@",placemark.country);
//                           NSLog(@"placemark.postalCode %@",placemark.postalCode);
//                           NSLog(@"placemark.administrativeArea %@",placemark.administrativeArea);
//                           NSLog(@"placemark.locality %@",placemark.locality);
//                           NSLog(@"placemark.subLocality %@",placemark.subLocality);
//                           NSLog(@"placemark.subThoroughfare %@",placemark.subThoroughfare);
//                           
//                       }
//                   }];
    [currentLocation stopUpdatingLocation];
}

#pragma mark - Delegate CellView Bot
- (void) activeClickToButton:(ChatButtonObject *)currentButton
{
    if ([currentButton.type isEqualToString:@"api"] || [currentButton.type isEqualToString:@"api_confirm"])
    {
        ChatBotObject* itemChat = [[ChatBotObject alloc] init];
        itemChat.isBot = NO;
        itemChat.valueContent = currentButton.textButton;
        [self loadViewChatToMainViewChatWithObjectChat:itemChat];
        [self callChatBotForText:[currentButton.action stringByReplacingOccurrencesOfString:@"_" withString:@""]];
    }
    else if ([currentButton.type isEqualToString:@"callus"])
    {
        NSString *phoneNumber = @"+6563363075";//currentButton.action;
        NSURL *phoneUrl = [NSURL URLWithString:[@"telprompt://" stringByAppendingString:phoneNumber]];
        NSURL *phoneFallbackUrl = [NSURL URLWithString:[@"tel://" stringByAppendingString:phoneNumber]];
        
        if ([UIApplication.sharedApplication canOpenURL:phoneUrl]) {
            [UIApplication.sharedApplication openURL:phoneUrl];
        } else if ([UIApplication.sharedApplication canOpenURL:phoneFallbackUrl]) {
            [UIApplication.sharedApplication openURL:phoneFallbackUrl];
        } else {
            // Show an error message: Your device can not do phone calls.
            UIAlertController * alert=   [UIAlertController
                                          alertControllerWithTitle:@""
                                          message:@"Your device can not do phone calls."
                                          preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:NSLocalizedString(@"Close",nil)
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     //Do some thing here
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
    else if ([currentButton.type isEqualToString:@"livechat"])
    {
        // Go to live Chat
        InAppBrowserViewController* controller = [[InAppBrowserViewController alloc] initWithNibName:@"InAppBrowserViewController" bundle:nil];
        controller.urlBooking = @"";
        controller.isLiveChat = YES;
        [self.navigationController pushViewController:controller animated:YES];
    }
}
@end
