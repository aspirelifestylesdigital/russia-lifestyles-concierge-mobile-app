//
//  WebController.h
//  ALC
//
//  Created by Anh Tran on 9/9/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "MainViewController.h"

@interface WebController : MainViewController
@property (strong, nonatomic) NSString* urlWebsite;
@property (strong, nonatomic) NSString* pageTitle;
@end
