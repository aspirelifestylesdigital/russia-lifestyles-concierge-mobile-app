//
//  UIViewController+UpdatingTextForViews.m
//  ALC
//
//  Created by Chung Mai on 8/29/17.
//  Copyright © 2017 Sunrise Software Solutions. All rights reserved.
//

#import "UIViewController+UpdatingTextForViews.h"
#import <objc/runtime.h>

static void * UpdatingTextDelegateProperty = &UpdatingTextDelegateProperty;

@implementation UIViewController (UpdatingTextForViews)


+ (void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        Class class = [self class];
        
        SEL originalSelector = @selector(viewWillAppear:);
        SEL swizzledSelector = @selector(viewWillAppearWithUpdatingText:);
        
        Method originalMethod = class_getInstanceMethod(class, originalSelector);
        Method swizzledMethod = class_getInstanceMethod(class, swizzledSelector);
        
        // When swizzling a class method, use the following:
        // Class class = object_getClass((id)self);
        // ...
        // Method originalMethod = class_getClassMethod(class, originalSelector);
        // Method swizzledMethod = class_getClassMethod(class, swizzledSelector);
        
        BOOL didAddMethod =
        class_addMethod(class,
                        originalSelector,
                        method_getImplementation(swizzledMethod),
                        method_getTypeEncoding(swizzledMethod));
        
        if (didAddMethod) {
            class_replaceMethod(class,
                                swizzledSelector,
                                method_getImplementation(originalMethod),
                                method_getTypeEncoding(originalMethod));
        } else {
            method_exchangeImplementations(originalMethod, swizzledMethod);
        }
    });
}
- (id<UpdatingTextForViewsDelegate>)updatingTextDelegate {
    return objc_getAssociatedObject(self, UpdatingTextDelegateProperty);
}

- (void)setUpdatingTextDelegate:(id<UpdatingTextForViewsDelegate>)delegate {
    objc_setAssociatedObject(self, UpdatingTextDelegateProperty, delegate, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void) viewWillAppearWithUpdatingText:(BOOL)animated {
    [self viewWillAppearWithUpdatingText:animated];
    if(self.updatingTextDelegate && [self.updatingTextDelegate respondsToSelector:@selector(setTextForViews)])
    {
        [self.updatingTextDelegate setTextForViews];
        
    }
    NSLog(@"logged view did appear for %@", [self class]);
}

@end
