//
//  WSCreateGourmetPre.h
//  ALC
//
//  Created by Hai NguyenV on 9/16/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSB2CBase.h"

@interface WSCreateGourmetPre : WSB2CBase

@property(nonatomic, strong)NSMutableArray* arrCuisine;
@property(nonatomic, strong)NSMutableArray* arrLoyalty;
@property(nonatomic, strong)NSString* otherCuisine;
@property(nonatomic, strong)NSString* preferenceID;
@property(nonatomic, strong)NSString* foodAllergies;

-(void)createGourmet;

@end
