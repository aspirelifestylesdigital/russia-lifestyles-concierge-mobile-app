//
//  WSGetCityGuideList.h
//  ALC
//
//  Created by Anh Tran on 9/14/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSBase.h"

@interface WSGetCityGuideList : WSBase
@property (strong, nonatomic) NSString *region;
@property (strong, nonatomic) NSString *country;
@property (strong, nonatomic) NSMutableArray *cities;
@property (strong, nonatomic) NSMutableArray* itemDatas;
-(void)getCityGuideList;
-(void) nextPage;
-(id)initWithPageSize:(NSInteger)size;
@end
