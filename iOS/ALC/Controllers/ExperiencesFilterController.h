//
//  ExperiencesFilterController.h
//  ALC
//
//  Created by Anh Tran on 8/24/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "MainViewController.h"
#import "Common.h"
#import "SubFilterController.h"

#define Cuisine @"Cuisines"
#define ZipCode @"Zipcode"
#define PriceRange @"PriceRange"
#define Star @"Rating"
#define Benefit @"benefit"
#define Popularity @"popularity"
#define Activities @"activity"
#define Event @"event"
#define Occasion @"Occasions"

@protocol ExperiencesFilterControllerDelegate;

@interface ExperiencesFilterController : MainViewController <SubFilterControllerDelegate>
@property ( nonatomic) enum EXPERIENCE_FILTER_TYPE filterType;
@property (unsafe_unretained) id<ExperiencesFilterControllerDelegate> delegate;
@property (strong, nonatomic) NSMutableDictionary *selectedOptions;
@end

@protocol ExperiencesFilterControllerDelegate <NSObject>

-(void) selectedFilterOptions:(NSMutableDictionary*) options;

@end