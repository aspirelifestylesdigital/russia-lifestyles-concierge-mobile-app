//
//  BranchObject.h
//  ALC
//
//  Created by Anh Tran on 8/23/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BranchObject : NSObject
@property (strong, nonatomic) NSString* branchId;
@property (strong, nonatomic) NSString* name;
@property (strong, nonatomic) NSString* address1;
@property (strong, nonatomic) NSString* address2;
@property (strong, nonatomic) NSString* telephone;
@property (strong, nonatomic) NSString* zipCode;
@property (strong, nonatomic) NSString* longitude;
@property (strong, nonatomic) NSString* lattitude;
-(id) initFromDict:(NSDictionary*) dict;
@end
