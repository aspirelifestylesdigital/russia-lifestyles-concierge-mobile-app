//
//  GourmetObject.h
//  ALC
//
//  Created by Anh Tran on 8/23/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GourmetObject : NSObject
@property (strong, nonatomic) NSString* restaurantID;
@property (strong, nonatomic) NSString* restaurantName;
@property (strong, nonatomic) NSString* restaurantDescription;
@property (strong, nonatomic) NSString* priceRange;
@property (strong, nonatomic) NSString* rating;
@property (strong, nonatomic) NSString* ratingNumber;
@property (strong, nonatomic) NSString* internalLabel;
@property (strong, nonatomic) NSString* address;
@property (strong, nonatomic) NSString* zipCode;
@property (strong, nonatomic) NSString* longitude;
@property (strong, nonatomic) NSString* lattitude;
@property (strong, nonatomic) NSArray* cuisines;
@property (strong, nonatomic) NSString* cityName;
@property (strong, nonatomic) NSString* countryName;
@property (strong, nonatomic) NSString* countryCode;
@property (strong, nonatomic) NSString* telephone;
@property (strong, nonatomic) NSArray* menu;
@property (strong, nonatomic) NSArray* branches;
@property (strong, nonatomic) NSArray* galleries;
@property (strong, nonatomic) NSArray* occasions;
@property (strong, nonatomic) NSString* logoAlt;
@property (strong, nonatomic) NSString* logoSource;
@property (strong, nonatomic) NSString* privilegeExpirationDate;
@property (strong, nonatomic) NSString* siteURL;
@property (strong, nonatomic) NSString* bookingDirectLabel;
@property (strong, nonatomic) NSString* bookingURL;
@property (strong, nonatomic) NSString* bannerURL;
@property (strong, nonatomic) NSString* hoursOfOperation;
@property (strong, nonatomic) NSString* backgroundImg;
@property (strong, nonatomic) NSString* termConditions;
@property (strong, nonatomic) NSString* gemContent;
@property (strong, nonatomic) NSString* dressCode;
@property (strong, nonatomic) NSString* email;
@property (strong, nonatomic) NSString* subTitle;
@property ( nonatomic) Boolean isInterested;
@property (strong,nonatomic) NSString* officalSiteUrl;
@property (strong,nonatomic) NSString* ItemURL;

-(id) initFromDict:(NSDictionary*) dict;
-(NSString*) getCuisineString;
-(NSString*) getDistance:(CLLocation*) currentLocation;
@end
