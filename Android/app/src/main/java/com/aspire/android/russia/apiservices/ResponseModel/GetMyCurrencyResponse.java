package com.aspire.android.russia.apiservices.ResponseModel;

import com.aspire.android.russia.apiservices.apis.appimplement.BaseResponse;
import com.aspire.android.russia.model.CommonObject;
import com.aspire.android.russia.model.CurrencyObject;


public class GetMyCurrencyResponse
        extends BaseResponse {
    private CommonObject Data;

    public CommonObject getData() {
        return Data;
    }

    public void setData(CommonObject data) {
        this.Data = data;
    }

    public CurrencyObject getCurrencyObject(){
        CurrencyObject currencyObject = new CurrencyObject();
        if(Data!=null && Data.getValue()!=null && Data.getKey()!=null) {
            currencyObject.setCurrencyKey(Data.getKey());
            currencyObject.setCurrencyText(Data.getValue());
        }
        return currencyObject;
    }
}
