package com.aspire.android.russia.fragment;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.aspire.android.russia.R;
import com.aspire.android.russia.application.coreactivitys.BaseFragment;
import com.aspire.android.russia.utils.CommonUtils;
import com.aspire.android.russia.utils.FontUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by anh.trinh on 9/13/2016.
 */
public class ErrorFragment
        extends BaseFragment {

    public static final String PRE_ERROR_MESSAGE = "error_message";
    @BindView(R.id.ic_back)
    ImageView mIcBack;
    @BindView(R.id.tv_tool_bar_title)
    TextView mTvToolBarTitle;
    @BindView(R.id.tv_error_message)
    TextView mTvErrorMessage;
    @BindView(R.id.btn_ok)
    Button mBtnOk;

    @Override
    protected int layoutId() {
        return R.layout.fragment_error;
    }

    @Override
    protected void initView() {
        mTvToolBarTitle.setText(getString(R.string.text_title_error));
        CommonUtils.setFontForViewRecursive(mTvErrorMessage,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);
        CommonUtils.setFontForViewRecursive(mTvToolBarTitle,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
        CommonUtils.setFontForViewRecursive(mBtnOk,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
        Bundle bundle = getArguments();
        if(bundle!=null) {
            String errorMessage = getArguments().getString(PRE_ERROR_MESSAGE,
                                                           "");

            mTvErrorMessage.setText(errorMessage);
        }

    }

    @Override
    protected void bindData() {


    }

    @Override
    public void onResume() {

        super.onResume();
    }

    @Override
    public boolean onBack() {
        return false;
    }


    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater,
                                           container,
                                           savedInstanceState);
        ButterKnife.bind(this,
                         rootView);
        return rootView;
    }

    @OnClick({R.id.ic_back,
              R.id.btn_ok})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ic_back:
                onBackPress();
                break;
            case R.id.btn_ok:
                onBackPress();
                break;
        }
    }
}
