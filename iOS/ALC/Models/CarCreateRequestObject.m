//
//  CarCreateRequestObject.m
//  ALC
//
//  Created by Anh Tran on 10/10/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "CarCreateRequestObject.h"

@implementation CarCreateRequestObject
-(id) initRentalFromDetails:(CarRentalDetailsObject*)detailsObj{
    self = [super init];
    self.bookingId = self.bookingItemId = detailsObj.bookingId;
    self.reservationName = detailsObj.guestName;
    self.email = detailsObj.email;
    self.mobileNumber = detailsObj.mobileNumber;
    self.isContactPhone = detailsObj.isContactPhone;
    self.isContactBoth = detailsObj.isContactBoth;
    self.isContactEmail = detailsObj.isContactEmail;
    self.specialMessage = detailsObj.specialRequirement;
    self.pickUpDate = formatDate(detailsObj.pickUpDate, DATE_SEND_REQUEST_FORMAT);
    self.pickUpTime = formatDate(detailsObj.pickUpTime, TIME_SEND_REQUEST_FORMAT);
    self.dropOffDate = formatDate(detailsObj.dropOffDateTime, DATE_SEND_REQUEST_FORMAT);
    self.dropOffTime = formatDate(detailsObj.dropOffDateTime, TIME_SEND_REQUEST_FORMAT);
    self.preferredVerhicle = [NSMutableArray arrayWithArray:detailsObj.prefferedVerhicles];
    self.preferredCarRentals = [NSMutableArray arrayWithArray:detailsObj.prefferedRentals];
    self.maximumPrice = detailsObj.maximumPrice;
    self.isInternationalLicense = detailsObj.isInternationalLicense;   
    self.driverAge = detailsObj.driverAge;
    self.driverName = detailsObj.driverName;
    self.pickUpLocation = detailsObj.pickUpLocation;
    self.dropOffLocation = detailsObj.dropOffLocation;
    return self;
}

-(id) initTransferFromDetails:(CarTransferDetailsObject*)detailsObj{
    self = [super init];
    self.bookingId = self.bookingItemId = detailsObj.bookingId;
    self.reservationName = detailsObj.guestName;
    self.email = detailsObj.email;
    self.mobileNumber = detailsObj.mobileNumber;
    self.isContactPhone = detailsObj.isContactPhone;
    self.isContactBoth = detailsObj.isContactBoth;
    self.isContactEmail = detailsObj.isContactEmail;
    self.specialMessage = detailsObj.specialRequirement;
    self.pickUpDate = formatDate(detailsObj.pickUpDate, DATE_SEND_REQUEST_FORMAT);
    self.pickUpTime = formatDate(detailsObj.pickUpTime, TIME_SEND_REQUEST_FORMAT);
    self.pickUpLocation = detailsObj.pickUpLocation;
    self.dropOffLocation = detailsObj.dropOffLocation;
    self.transportType = detailsObj.transportType;
    self.nbOfPassengers = detailsObj.nbPassengers;
    return self;
}
@end
