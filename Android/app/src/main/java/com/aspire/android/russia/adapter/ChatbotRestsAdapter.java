package com.aspire.android.russia.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aspire.android.russia.R;
import com.aspire.android.russia.model.Restaurant;
import com.aspire.android.russia.model.UserItem;
import com.aspire.android.russia.utils.Fontfaces;
import com.aspire.android.russia.utils.StringUtil;
//import com.bumptech.glide.Glide;
import com.joooonho.SelectableRoundedImageView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by nga.nguyent on 10/25/2016.
 */

public class ChatbotRestsAdapter extends PagerAdapter
{
    private List<Restaurant> data = new ArrayList<>();
    private Context context;
    private int itemIndex = 0;
    private ChatbotAdapter.OnRestaurantInfoClickListener restaurantInfoClickListener;
    private boolean clickable = false;
    //private boolean firstLoad = true;
    private int currentPageItemSelectedIndex = 0;

    public ChatbotRestsAdapter(Context context) {
        this.context = context;
    }

    public void setData(int pos, boolean clickableItem, List<Restaurant> data) {
        itemIndex = pos;
        this.clickable = clickableItem;
        this.data.clear();
        this.data.addAll(data);
    }

    public void setCurrentPageItemSelectedIndex(int currentPageItemSelectedIndex) {
        this.currentPageItemSelectedIndex = currentPageItemSelectedIndex;
    }

    public void setRestaurantInfoClickListener(ChatbotAdapter.OnRestaurantInfoClickListener restaurantInfoClickListener) {
        this.restaurantInfoClickListener = restaurantInfoClickListener;
    }

    @Override
    public int getCount() {
        return data.size();
        //return 5;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view==object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        PageItem view = new PageItem(context);
        view.setData(position, data.get(position));
        view.setId(position);
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        //super.destroyItem(container, position, object);
        container.removeView((View) object);
    }

    public class PageItem extends FrameLayout{
        @BindView(R.id.imgRestaurant)
        SelectableRoundedImageView imgRestaurant;
        @BindView(R.id.imgRate)
        ImageView imgRate;
        @BindView(R.id.imgFavorite)
        ImageView imgFavorite;
        @BindView(R.id.tvCost)
        TextView tvCost;
        @BindView(R.id.tvShare)
        TextView tvShare;
        @BindView(R.id.tvRestaurantName)
        TextView tvRestaurantName;
        @BindView(R.id.tvAddress)
        TextView tvAddress;
        @BindView(R.id.tvDistrict)
        TextView tvDistrict;
        @BindView(R.id.tvCountry)
        TextView tvCountry;
        @BindView(R.id.tvReadMore)
        TextView tvReadMore;
        @BindView(R.id.tvBookNow)
        TextView tvBookNow;
        @BindView(R.id.llColor)
        LinearLayout llColor;

        int position;

        public PageItem(Context context) {
            super(context);
            initView();
        }

        public PageItem(Context context, AttributeSet attrs) {
            super(context, attrs);
            initView();
        }

        public PageItem(Context context, AttributeSet attrs, int defStyleAttr) {
            super(context, attrs, defStyleAttr);
            initView();
        }

        public void initView() {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.layout_chatbot_image_item, null, false);
            this.addView(view);

            ButterKnife.bind(this, this);

            imgRate.setVisibility(View.GONE);
            tvCost.setText("");
            tvRestaurantName.setText("");
            tvAddress.setText("");
            tvDistrict.setText("");
            tvCountry.setText("");

            tvCost.setTypeface(Fontfaces.getFont(context, Fontfaces.FONTLIST.Avenirnext_demibold));
            tvShare.setTypeface(Fontfaces.getFont(context, Fontfaces.FONTLIST.Avenirnext_demibold));
            tvRestaurantName.setTypeface(Fontfaces.getFont(context, Fontfaces.FONTLIST.Avenirnext_demibold));
            tvAddress.setTypeface(Fontfaces.getFont(context, Fontfaces.FONTLIST.Avenirnext_demibold));
            tvDistrict.setTypeface(Fontfaces.getFont(context, Fontfaces.FONTLIST.Avenirnext_demibold));
            tvCountry.setTypeface(Fontfaces.getFont(context, Fontfaces.FONTLIST.Avenirnext_demibold));
            tvReadMore.setTypeface(Fontfaces.getFont(context, Fontfaces.FONTLIST.Avenirnext_demibold));
            tvBookNow.setTypeface(Fontfaces.getFont(context, Fontfaces.FONTLIST.Avenirnext_demibold));

            /*if(firstLoad){
                firstLoad = false;
                setColorCurrentItem(0);
            }*/
        }

        public void setColorCurrentItem(int currentItem){
            if(position == currentItem){
                llColor.setVisibility(GONE);
            }
            else {
                llColor.setVisibility(VISIBLE);
            }
        }

        public void setData(int pos, Restaurant rest){
            imgFavorite.setClickable(clickable);
            tvShare.setClickable(clickable);
            tvBookNow.setClickable(clickable);
            tvReadMore.setClickable(clickable);

            this.position = pos;
            if(pos == currentPageItemSelectedIndex){
                llColor.setVisibility(GONE);
            }
            else{
                llColor.setVisibility(VISIBLE);
            }

            if(rest==null)
                return;

            tvCost.setText(rest.getPriceRange());
            tvRestaurantName.setText(rest.getRestaurantName());
            String[] address = rest.getAddress().split(",");
            if(address.length>0)
                tvAddress.setText(address[0].trim());
            if(address.length>1)
                tvDistrict.setText(address[1].trim());
            String city = rest.getCityName();
            if(!StringUtil.isEmpty(rest.getCountryName())) {
                if(city.length()>0)
                    city += ", ";
                city += rest.getCountryName();
            }
            if(StringUtil.isEmpty(city)){
                if(address.length>2){
                    String addr = rest.getAddress();
                    int idx = addr.indexOf(address[2]);
                    city = addr.substring(idx).trim();
                }
            }
            tvCountry.setText(city);

            if(!UserItem.isLogined()){
                imgFavorite.setVisibility(GONE);
                imgFavorite.setClickable(false);
            }
            else {
                imgFavorite.setClickable(true);
                imgFavorite.setVisibility(View.VISIBLE);
                if (rest.isVoted()) {
                    imgFavorite.setSelected(true);
                } else {
                    imgFavorite.setSelected(false);
                }
            }

//            Glide
//                    .with(context)
//                    .load(rest.getBannerURL())
//                    .asBitmap()
//                    //.override(400,200)
//                    .centerCrop()
//                    //.placeholder(R.drawable.loading_spinner)
//                    //.crossFade()
//                    .into(imgRestaurant);
        }

        @OnClick({R.id.tvRestaurantName, R.id.imgFavorite, R.id.tvShare, R.id.tvReadMore, R.id.tvBookNow})
        void onInfoItemClick(View v){
            switch (v.getId()){
                case R.id.imgFavorite:
                    if(clickable && restaurantInfoClickListener!=null){
                        restaurantInfoClickListener.onFavoriteClick(itemIndex, position);
                    }
                    break;
                case R.id.tvShare:
                    if(clickable && restaurantInfoClickListener!=null){
                        restaurantInfoClickListener.onShareClick(itemIndex, position);
                    }
                    break;
                case R.id.tvRestaurantName:
                case R.id.tvReadMore:
                    if(clickable && restaurantInfoClickListener!=null){
                        restaurantInfoClickListener.onReadMoreClick(itemIndex, position);
                    }
                    break;
                case R.id.tvBookNow:
                    if(clickable && restaurantInfoClickListener!=null){
                        restaurantInfoClickListener.onBookNowClick(itemIndex, position);
                    }
                    break;
            }
        }

    }

}
