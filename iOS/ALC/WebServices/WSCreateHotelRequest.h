//
//  WSCreateHotelRequest.h
//  ALC
//
//  Created by Anh Tran on 10/6/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSB2CBase.h"
#import "HotelRequestObject.h"
@interface WSCreateHotelRequest : WSB2CBase
-(void)bookingHotel:(HotelRequestObject*) item;
@end
