//
//  EventRecommendRequestObj.m
//  ALC
//
//  Created by Hai NguyenV on 12/2/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "EventRecommendRequestObj.h"

@implementation EventRecommendRequestObj
-(id) initFromDetails:(EntertainmentRequestDetailsObject*)detailsObj{
    self = [super init];
    self.BookingId = detailsObj.bookingId;
    self.reservationName = detailsObj.guestName;
    self.EmailAddress = detailsObj.email;
    self.MobileNumber = detailsObj.mobileNumber;
    self.isContactPhone = detailsObj.isContactPhone;
    self.isContactBoth = detailsObj.isContactBoth;
    self.isContactEmail = detailsObj.isContactEmail;
    self.SpecialRequirements = detailsObj.specialRequirement;
    self.State = detailsObj.stateValue;
    self.City = detailsObj.city;
    self.Country = detailsObj.country;
    
    self.reservationDate = formatDate(detailsObj.preferDate, DATETIME_SEND_REQUEST_FORMAT);
    
    if(detailsObj.noOfTickets.length > 0){
        self.adultPax = [detailsObj.noOfTickets integerValue];
    }
    
    if(detailsObj.eventCategory){
        self.EventCategory = convertArrayToString(detailsObj.eventCategory);
    }
    
    return self;
}
@end
