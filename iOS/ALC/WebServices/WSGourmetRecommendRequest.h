//
//  WSGourmetRecommendRequest.h
//  ALC
//
//  Created by Hai NguyenV on 10/18/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSB2CBase.h"
#import "GourmetRecommendRequestObj.h"

@interface WSGourmetRecommendRequest : WSB2CBase
{
    GourmetRecommendRequestObj* requestObject;
}
-(void)bookingRecommend:(GourmetRecommendRequestObj*)ortherRequest;

@end
