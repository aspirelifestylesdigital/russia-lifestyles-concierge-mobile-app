package com.aspire.android.russia.apiservices.b2c;

import com.aspire.android.russia.BuildConfig;
import com.aspire.android.russia.apiservices.apis.appimplement.b2c.B2CApiProviderClient;
import com.aspire.android.russia.apiservices.apis.appimplement.b2c.B2CICallback;
import com.aspire.android.russia.apiservices.b2c.ResponseModel.B2CUploadFileResponse;
import com.aspire.android.russia.application.AppConstant;
import com.aspire.android.russia.utils.SharedPreferencesUtils;
import com.aspire.android.russia.utils.StringUtil;

import java.io.File;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Callback;

public class B2CWSUploadMediaFile
        extends B2CApiProviderClient<B2CUploadFileResponse>{
    private String localPath;
    public B2CWSUploadMediaFile(B2CICallback callback){
        this.b2CICallback = callback;
    }

    public void setLocalPath(String localPath){
        this.localPath = localPath;
    }
    @Override
    public void run(Callback<B2CUploadFileResponse> cb) {
        this.callback = cb;
        super.checkAuthenticateAndRun();
    }
    @Override
    protected boolean isUseAuthenticateInApi() {
        return true;
    }

    @Override
    protected void runApi() {
        MultipartBody.Part partUploadPhoto = null;
        if (localPath != null) {
            File file = new File(localPath);
            if (!StringUtil.isEmpty(localPath) && file.exists()) {
                RequestBody requestFile =
                        RequestBody.create(MediaType.parse("multipart/form-data"),
                                file);
                partUploadPhoto =
                        MultipartBody.Part.createFormData("picture",
                                file.getName(),
                                requestFile);
            } else {

            }
        }
        serviceInterface.uploadMediaFile(BuildConfig.B2C_CONSUMER_KEY, SharedPreferencesUtils.getPreferences(AppConstant.PRE_B2C_ONLINE_MEMBER_ID, ""),
                partUploadPhoto).enqueue(this);
    }

    @Override
    protected Map<String, String> prepareHeaders() {
        return null;
    }

    @Override
    protected void postResponse(B2CUploadFileResponse response) {
    }
}
