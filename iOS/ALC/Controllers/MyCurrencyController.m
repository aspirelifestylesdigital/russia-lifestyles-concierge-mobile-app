//
//  MyCurrencyController.m
//  ALC
//
//  Created by Anh Tran on 9/26/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "MyCurrencyController.h"
#import "BaseResponseObject.h"
#import "CurrencyObject.h"
#import "CCNumberCell.h"

#define USER_CURRENCY @"user_currency"
#define USER_CURRENCY_KEY @"user_currency_key"
@interface MyCurrencyController (){
    //WSGetUserCurrency* wsGetUserCurrency;
    //WSCreateUserCurrency* wsCreateUserCurrency;
    NSArray* allCurrencies;
    NSArray* popularCurrencies;
    IBOutlet UITableView *table;   
    CurrencyObject* selectedCurrency;
}

@end

@implementation MyCurrencyController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addTitleToHeader:NSLocalizedString(@"CURRENCY", nil) withColor:HEADER_COLOR];
    [self customButtonBack];
    
    allCurrencies = getNormalCurrencyList();
    popularCurrencies = getPopularCurrencyList();
    selectedCurrency = [[CurrencyObject alloc] init:@"РУБЛЬ"   withValue:@"Russian ruble"];
    [self getSelectedCurrency];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) getSelectedCurrency{
    /*[self showToast];
    wsGetUserCurrency = [[WSGetUserCurrency alloc] init];
    wsGetUserCurrency.delegate = self;
    [wsGetUserCurrency getUserCurrency];*/
    
    selectedCurrency = [[CurrencyObject alloc] init];
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults] ;
    selectedCurrency.currencyText = [pref objectForKey:USER_CURRENCY];
    selectedCurrency.currencyKey = [pref objectForKey:USER_CURRENCY_KEY];
    if(selectedCurrency.currencyKey.length==0){
        selectedCurrency = [[CurrencyObject alloc] init:@"РУБЛЬ"   withValue:@"Russian ruble"];
    }
    [table reloadData];
}

-(void) createUserCurrency:(CurrencyObject*) currency{
    /*[self showToast];
    wsCreateUserCurrency = [[WSCreateUserCurrency alloc] init];
    wsCreateUserCurrency.delegate = self;
    [wsCreateUserCurrency createCurrency:currency];*/
    
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults] ;
    [pref setObject:currency.currencyText forKey:USER_CURRENCY];
    [pref setObject:currency.currencyKey forKey:USER_CURRENCY_KEY];
    [pref synchronize];
}

/*
-(void)loadDataDoneFrom:(WSBase*)ws{
    if(ws.task == WS_GET_CURRENCY){
        if(wsGetUserCurrency.selectedCurrency){
            selectedCurrency = wsGetUserCurrency.selectedCurrency;
        }
        [table reloadData];
    } else if(ws.task == WS_CREATE_CURRENCY){
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    
    [self stopToast];
}

-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode{
    [self stopToast];
    if(result!=nil && result.message!=nil){
        
    }
}*/

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return section == 0 ? popularCurrencies.count : allCurrencies.count;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 58.0*SCREEN_SCALE;;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 58.0*SCREEN_SCALE;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"CCCell";
    
    CCNumberCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if(cell == nil){
        NSArray *nib;
        nib = [[NSBundle mainBundle] loadNibNamed:@"CCNumberCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
        UIFont* font = cell.labelName.font;
        [cell.labelName setFont:[UIFont fontWithName:font.fontName size:font.pointSize*SCREEN_SCALE]];

    }
    
    [cell setBackgroundColor:[UIColor clearColor]];
    
    CurrencyObject* item;
    if(indexPath.section == 0){
        item = [popularCurrencies objectAtIndex:indexPath.row];
    } else {
        item = [allCurrencies objectAtIndex:indexPath.row];
    }
    
    cell.labelName.text = item.fullKeyvalue;
    if (selectedCurrency){
        cell.btCheck.hidden = ![selectedCurrency.currencyKey isEqualToString:item.currencyKey];
    } else {
        cell.btCheck.hidden = YES;
    }
    return cell;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    static NSString *simpleTableIdentifier = @"CCCell";
    
    CCNumberCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if(cell == nil){
        NSArray *nib;
        nib = [[NSBundle mainBundle] loadNibNamed:@"CCNumberCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    [cell setBackgroundColor:[UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:0.7]];
    if(section == 0){
        cell.labelName.text = NSLocalizedString( @"Popular", nil);
    } else {
        cell.labelName.text = NSLocalizedString(@"All currencies", nil);
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(cell && ((CCNumberCell*)cell).btCheck.hidden){
        if(indexPath.section == 0){
            selectedCurrency = [popularCurrencies objectAtIndex:indexPath.row];
        } else if(indexPath.section == 1) {
            selectedCurrency = [allCurrencies objectAtIndex:indexPath.row];
        }
        [table reloadData];
        [self createUserCurrency:selectedCurrency];
    } else {
       
    }
     [self.navigationController popViewControllerAnimated:YES];
    
   
}


@end
