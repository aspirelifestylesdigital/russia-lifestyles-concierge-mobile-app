package com.aspire.android.russia.processing.cancelrequest;

import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.aspire.android.russia.R;
import com.aspire.android.russia.apiservices.ResponseModel.HotelBookingDetailResponse;
import com.aspire.android.russia.apiservices.apis.appimplement.b2c.B2CBaseResponse;
import com.aspire.android.russia.apiservices.b2c.B2CWSGetRequestDetail;
import com.aspire.android.russia.apiservices.b2c.RequestModel.B2CUpsertConciergeRequestRequest;
import com.aspire.android.russia.apiservices.b2c.ResponseModel.B2CGetRecentRequestResponse;
import com.aspire.android.russia.application.AppConstant;
import com.aspire.android.russia.application.AppContext;
import com.aspire.android.russia.model.concierge.HotelBookingDetailData;
import com.aspire.android.russia.utils.DateTimeUtil;
import com.aspire.android.russia.utils.SharedPreferencesUtils;
import com.aspire.android.russia.utils.StringUtil;

import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by ThuNguyen on 10/26/2016.
 */

public class HotelCancelRequestDetail
        extends BaseCancelRequestDetail {
    private HotelBookingDetailData hotelBookingDetailData;

    public HotelCancelRequestDetail() {
    }

    public HotelCancelRequestDetail(View view) {
        super(view);
    }

    @Override
    public void getRequestDetail() {
        showProgressDialog();
        B2CWSGetRequestDetail b2CWSGetRequestDetail = new B2CWSGetRequestDetail(this);
        b2CWSGetRequestDetail.setValue(myRequestObject.getEpcCaseId(),
                                       myRequestObject.getBookingItemID());
        b2CWSGetRequestDetail.run(null);
        /*WSGetHotelBookingDetail wsGetHotelBookingDetail = new WSGetHotelBookingDetail();
        wsGetHotelBookingDetail.setBookingId(myRequestObject.getBookingId());
        wsGetHotelBookingDetail.run(this);
        showProgressDialog();*/
    }

    @Override
    protected void updateUI() {
        super.updateUI();
        long epochCreatedDate = hotelBookingDetailData.getCreateDateEpoc();
        tvRequestedDate.setText(DateTimeUtil.shareInstance()
                                            .formatTimeStamp(epochCreatedDate,
                                                             "dd MMMM yyyy"));
        tvRequestedTime.setText(DateTimeUtil.shareInstance()
                                            .formatTimeStamp(epochCreatedDate,
                                                             "hh:mm aa"));
        // Request type
        tvRequestType.setText(AppContext.getSharedInstance().getResources().getString(R.string.text_cancel_request_title_hotel_request));

        // Request name
        String requestName = hotelBookingDetailData.getHotelName();
        if (TextUtils.isEmpty(requestName) || requestName.equalsIgnoreCase("null")) {
            requestName = myRequestObject.getItemTitle();
        }
        if (TextUtils.isEmpty(requestName) || requestName.equalsIgnoreCase("null")) {
            tvRequestName.setText(hotelBookingDetailData.getCity());
        } else {
            tvRequestName.setText(requestName + ", " + hotelBookingDetailData.getCity());
        }
        // Request detail header
        tvRequestDetailHeader.setText(AppContext.getSharedInstance().getResources().getString(R.string.text_cancel_request_hotel_request_detail));

        if (hotelBookingDetailData != null) {
            // Case Id
            tvCaseId.setText(hotelBookingDetailData.getBookingItemID());

            // Guests
            addRequestDetailItem(AppContext.getSharedInstance().getResources().getString(R.string.text_cancel_request_guests),
                                 hotelBookingDetailData.getGuestDisplay());

            // Room and Suites
            /*String roomSuitesObj = hotelBookingDetailData.getRoomType();
            if (roomSuitesObj != null && !TextUtils.isEmpty(roomSuitesObj)) {
                addRequestDetailItem("Rooms & suites",
                                     roomSuitesObj);
            }*/

            // Check In Date
            long checkInDate = hotelBookingDetailData.getCheckInDateEpoc();
            addRequestDetailItem(AppContext.getSharedInstance().getResources().getString(R.string.text_cancel_request_check_in_date),
                                 DateTimeUtil.shareInstance()
                                             .formatTimeStamp(checkInDate,
                                                              "dd MMMM yyyy"));

            // Check In Date
            long checkOutDate = hotelBookingDetailData.getCheckOutDateEpoc();
            addRequestDetailItem(AppContext.getSharedInstance().getResources().getString(R.string.text_cancel_request_check_out_date),
                                 DateTimeUtil.shareInstance()
                                             .formatTimeStamp(checkOutDate,
                                                              "dd MMMM yyyy"));
        }
    }

    @Override
    public void onResponse(Call call,
                           Response response) {
        hideProgressDialog();
        if (response != null && response.body() instanceof HotelBookingDetailResponse) {
            hotelBookingDetailData = ((HotelBookingDetailResponse) response.body()).getData();
            updateUI();
        }
    }

    @Override
    public void onFailure(Call call,
                          Throwable t) {
        hideProgressDialog();
    }

    @Override
    public void onB2CResponse(final B2CBaseResponse response) {
        hideProgressDialog();
        if (response instanceof B2CGetRecentRequestResponse) {
            hotelBookingDetailData =
                    new HotelBookingDetailData((B2CGetRecentRequestResponse) response);
            updateUI();
        }

    }

    @Override
    public void onB2CResponseOnList(final List<B2CBaseResponse> responseList) {

    }

    @Override
    public void onB2CFailure(final String errorMessage,
                             final String errorCode) {
        hideProgressDialog();
    }

    @Override
    public B2CUpsertConciergeRequestRequest getB2CUpsertConciergeRequestRequest() {
        B2CUpsertConciergeRequestRequest upsertConciergeRequestRequest =
                new B2CUpsertConciergeRequestRequest();
        upsertConciergeRequestRequest.setFunctionality(AppConstant.CONCIERGE_FUNCTIONALITY_TYPE.HOTEL.getValue());
        upsertConciergeRequestRequest.setRequestType(AppConstant.CONCIERGE_REQUEST_TYPE.BOOK_HOTEL.getValue());
        upsertConciergeRequestRequest.setEmail1((SharedPreferencesUtils.getPreferences(AppConstant.USER_EMAIL_PRE,
                                                                                       "")));
        if (!TextUtils.isEmpty(hotelBookingDetailData.getMobileNumber())) {
            upsertConciergeRequestRequest.setPhoneNumber(hotelBookingDetailData.getMobileNumber());
        }
            upsertConciergeRequestRequest.setPrefResponse(hotelBookingDetailData.getPrefResponse());

        if (!TextUtils.isEmpty(myRequestObject.getFirstName())) {
            upsertConciergeRequestRequest.setFirstName(myRequestObject.getFirstName());
        }
        if (!TextUtils.isEmpty(myRequestObject.getLastName())) {
            upsertConciergeRequestRequest.setLastName(myRequestObject.getLastName());
        }
        if (!TextUtils.isEmpty(hotelBookingDetailData.getCity())) {
            upsertConciergeRequestRequest.setCity(hotelBookingDetailData.getCity());
        }
        if (!TextUtils.isEmpty(hotelBookingDetailData.getCountry())) {
            upsertConciergeRequestRequest.setCountry(hotelBookingDetailData.getCountry());
        }
        if (hotelBookingDetailData.getNumberOfAdults()>0) {
            upsertConciergeRequestRequest.setNumberOfAdults(String.valueOf(hotelBookingDetailData.getNumberOfAdults()));
        }
        if (hotelBookingDetailData.getNumberOfKids()>0) {
            upsertConciergeRequestRequest.setNumberOfKids(String.valueOf(hotelBookingDetailData.getNumberOfKids()));
        }
        // Check in date
        upsertConciergeRequestRequest.setStartDate(DateTimeUtil.shareInstance().formatTimeStamp(hotelBookingDetailData.getCheckInDateEpoc(),
                                                                                                AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS));
        upsertConciergeRequestRequest.setDelivery(DateTimeUtil.shareInstance().formatTimeStamp(hotelBookingDetailData.getCheckInDateEpoc(),
                                                                                               AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS));
        upsertConciergeRequestRequest.setCheckIn(DateTimeUtil.shareInstance().formatTimeStamp(hotelBookingDetailData.getCheckInDateEpoc(),
                                                                                              AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS));



        // Check out date
        upsertConciergeRequestRequest.setEndDate(DateTimeUtil.shareInstance().formatTimeStamp(hotelBookingDetailData.getCheckOutDateEpoc(),
                                                                                              AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS));
        upsertConciergeRequestRequest.setCheckOut(DateTimeUtil.shareInstance().formatTimeStamp(hotelBookingDetailData.getCheckOutDateEpoc(),
                                                                                               AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS));
       // Request details
        upsertConciergeRequestRequest.setRequestDetails(hotelBookingDetailData.getRequestDetaiString());


        if (myRequestObject != null) {
            upsertConciergeRequestRequest.setTransactionID(myRequestObject.getBookingItemID());
        }
        return upsertConciergeRequestRequest;
    }

}
