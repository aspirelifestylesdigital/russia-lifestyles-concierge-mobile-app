package com.aspire.android.russia.fragment;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.aspire.android.russia.R;
import com.aspire.android.russia.adapter.ExperiencesDetailAdapter;
import com.aspire.android.russia.application.coreactivitys.BaseFragment;
import com.aspire.android.russia.interfaces.OnRecyclerViewItemClickListener;
import com.aspire.android.russia.model.ExperiencesDetailObject;
import com.aspire.android.russia.model.Restaurant;
import com.aspire.android.russia.utils.CommonUtils;
import com.aspire.android.russia.utils.Fontfaces;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/*
 * Created by chau.nguyen on 10/20/2016.
 */
public class ExperiencesDetailGourmetInfoFragment extends BaseFragment {

    @BindView(R.id.tvExpPrice)
    TextView tvPrice;
    @BindView(R.id.tvExpStar)
    TextView tvStar;
    @BindView(R.id.tvExpGem)
    TextView tvGem;
    @BindView(R.id.tvDetailEatAboutStore)
    TextView tvAboutTitle;
    @BindView(R.id.tvDetailEatDescription)
    TextView tvAboutDescription;

    @BindView(R.id.rcvDetail)
    RecyclerView rcvRow;
    List<ExperiencesDetailObject> listExpDetailObj;

    private Restaurant restaurant;

    public ExperiencesDetailGourmetInfoFragment() {
        listExpDetailObj = new ArrayList<>();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int layoutId() {
        return R.layout.fragment_experiences_detail_gourmet_info;
    }

    private void setFont(){
        tvPrice.setTypeface(Fontfaces.getFont(getActivity(), Fontfaces.FONTLIST.AVENIR_NEXT_LT_MEDIUM));
        tvStar.setTypeface(Fontfaces.getFont(getActivity(), Fontfaces.FONTLIST.AVENIR_NEXT_LT_MEDIUM));
        tvGem.setTypeface(Fontfaces.getFont(getActivity(), Fontfaces.FONTLIST.AVENIR_NEXT_LT_MEDIUM));

        tvAboutTitle.setTypeface(Fontfaces.getFont(getActivity(), Fontfaces.FONTLIST.Avenirnext_demibold));
        tvAboutDescription.setTypeface(Fontfaces.getFont(getActivity(), Fontfaces.FONTLIST.AVENIR_NEXT_LT_MEDIUM));
    }
    @Override
    protected void initView() {
        setFont();

        if(restaurant != null){
            if(!restaurant.getPriceRange().isEmpty()){
                tvPrice.setText(restaurant.getPriceRange());
            }else {
                tvPrice.setText("");
            }

            if(!restaurant.getRating().isEmpty()){
                tvStar.setText(String.format(getString(R.string.experiences_detail_gourmet_star),restaurant.getRating()));
            }else {
                tvStar.setText("");
            }

            if(!restaurant.getInternalLabel().isEmpty()){
                tvGem.setText(restaurant.getInternalLabel());
            }else {
                tvGem.setText("");
            }

            CommonUtils.convertHtmlShowHtml(restaurant.getRestaurantDescription(),tvAboutDescription);

            String cuisine = restaurant.getShowCuisines();
            if(!cuisine.isEmpty()){
                listExpDetailObj.add(new ExperiencesDetailObject(cuisine, ExperiencesDetailAdapter.ROW_SHOW.CUISINE));
            }

            if(!restaurant.getHoursOfOperation().isEmpty()){
                listExpDetailObj.add(new ExperiencesDetailObject(restaurant.getHoursOfOperation(), ExperiencesDetailAdapter.ROW_SHOW.HOURS));
            }

            if(!restaurant.getDressCode().isEmpty()){
                listExpDetailObj.add(new ExperiencesDetailObject(restaurant.getDressCode(), ExperiencesDetailAdapter.ROW_SHOW.DRESS_CODE));
            }

            if(!restaurant.getTermsCondition().isEmpty()){
                listExpDetailObj.add(new ExperiencesDetailObject(restaurant.getTermsCondition(), ExperiencesDetailAdapter.ROW_SHOW.TERMS_CONDITION));
            }

            ExperiencesDetailAdapter adapter = new ExperiencesDetailAdapter(listExpDetailObj, new OnRecyclerViewItemClickListener() {
                @Override
                public void onItemClick(int position, View view) {

                }
            });

            LinearLayoutManager layoutManager = new LinearLayoutManager(view.getContext());
            layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            rcvRow.setLayoutManager(layoutManager);
            rcvRow.setAdapter(adapter);
        }
    }

    @Override
    protected void bindData() {

    }

    @Override
    protected boolean onBack() {
        return false;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }
}