//
//  CustomButtonView.m
//  demoChatBot
//
//  Created by Nhat Huy on 9/10/16.
//  Copyright © 2016 Nhat Huy. All rights reserved.
//

#import "CustomButtonView.h"

@implementation CustomButtonView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void) loadView
{
    [self layoutIfNeeded];
//    if ([self.currentButtonObject.type isEqualToString:@"api"] || [self.currentButtonObject.type isEqualToString:@"callus"] || [self.currentButtonObject.type isEqualToString:@"livechat"])
//    {
        [self.layer setCornerRadius:4.0];
        [self.layer setBorderColor:[[UIColor blackColor] CGColor]];
        [self.layer setBorderWidth:1.0];
//    }
//    else
//    {
//        [self.lblText setTextColor:[UIColor blueColor]];
//    }
    [self.lblText setText:self.currentButtonObject.textButton];
    
    NSInteger fistWidth = self.lblText.frame.size.width;
    
    [self.lblText sizeToFit];
    
    NSInteger spaceBetweenWidth = self.lblText.frame.size.width - fistWidth;
    
    self.layoutHeightText.constant = self.lblText.frame.size.height + 10;
    
    [self setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width + spaceBetweenWidth + 15,self.layoutHeightText.constant)];
}

- (IBAction)clickActiveButton:(id)sender
{
//    if ([self.currentButtonObject.type isEqualToString:@"api"] || [self.currentButtonObject.type isEqualToString:@"callus"] || [self.currentButtonObject.type isEqualToString:@"livechat"] )
//    {
        [self.btnShoe setBackgroundColor:[UIColor blackColor]];
        [self.lblText setTextColor:[UIColor whiteColor]];
        
//    }
    if (self.delegate)
    {
        if ([self.delegate respondsToSelector:@selector(clickAtButtonObject:)])
        {
            [self.delegate clickAtButtonObject:self.currentButtonObject];
        }
    }
}

- (void) returnNormal
{
    [self.btnShoe setBackgroundColor:[UIColor clearColor]];
    [self.lblText setTextColor:[UIColor blackColor]];
}


@end
