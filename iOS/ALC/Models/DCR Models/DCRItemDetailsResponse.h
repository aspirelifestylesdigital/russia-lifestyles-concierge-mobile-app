//
//  DCRItemDetailsResponse.h
//  ALC
//
//  Created by Anh Tran on 3/14/17.
//  Copyright © 2017 Sunrise Software Solutions. All rights reserved.
//

#import "BaseDCRObject.h"

@interface DCRItemDetailsResponse : BaseDCRObject
@property (nonatomic, strong) NSArray* items;
@end
