package com.aspire.android.russia.activitys;

import com.aspire.android.russia.R;
import com.aspire.android.russia.application.AppConstant;
import com.aspire.android.russia.application.coreactivitys.BaseActivityNonNavigationview;
import com.aspire.android.russia.fragment.LoginFragment;
import com.aspire.android.russia.model.UserItem;
import com.aspire.android.russia.utils.SharedPreferencesUtils;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.widget.FrameLayout;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by anh.trinh on 9/14/2016.
 */
public class LoginAcitivy
                extends BaseActivityNonNavigationview {


    @BindView(R.id.container)
    FrameLayout mContainer;
    @BindView(R.id.img_background)
    ImageView mImgBackground;
    @Override
    protected int activityLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    protected void initView() {
        /*loadFragment(new LoginFragment(),
                     null,
                     true,
                     false,
                     true);*/
        pushFragment(new LoginFragment(), true, false, false);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
        if (!SharedPreferencesUtils.getPreferences(AppConstant.USER_FIRST_TIME_PRE,
                                                   false)) {

            Intent intent = new Intent(getBaseContext(),
                                       SplashAcitivy.class);
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
        }else{
            if(UserItem.isLogined()){
                Intent intent = new Intent(this, HomeActivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
            }
        }
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager()
                .getDefaultDisplay()
                .getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        mImgBackground.getLayoutParams().height = height;
        mImgBackground.getLayoutParams().width = width;
        mImgBackground.setScaleType(ImageView.ScaleType.FIT_XY);
    }

    /*public void loadFragment(Fragment fragment, Bundle data, Boolean replace, Boolean
                                                                                      addToStack,
                             Boolean clearStack) {

        if (findViewById(R.id.parentContainer) != null) {
            if (clearStack) {
                clearAllFragments();
            }

            if (fragment != null) {
                if (replace) {
                    replaceFragment(fragment, data, addToStack);
                } else {
                    pushFragment(fragment, data, addToStack);
                }
            }
        }
    }

    protected void pushFragment(Fragment fragment, Bundle data, Boolean addToBackStack) {
        if (data != null) {
            fragment.setArguments(data);
        }
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft;

        if (addToBackStack) {
            ft = fm.beginTransaction();
            ft.add(R.id.parentContainer, fragment, fragment.getClass().getName());
            ft.addToBackStack(fragment.getClass().getName());
            ft.commitAllowingStateLoss();
        } else {
            ft = fm.beginTransaction();
            ft.add(R.id.parentContainer, fragment, fragment.getClass().getName());
            ft.commitAllowingStateLoss();
        }

    }

    protected void replaceFragment(Fragment fragment, Bundle data, Boolean addToBackStack) {
        if (data != null) {
            fragment.setArguments(data);
        }
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.parentContainer, fragment, fragment.getClass().getName());
        if (addToBackStack) {
            ft = fm.beginTransaction();
            ft.replace(R.id.parentContainer, fragment, fragment.getClass().getName());
            ft.addToBackStack(fragment.getClass().getName());
            ft.commitAllowingStateLoss();
        } else {
            ft = fm.beginTransaction();
            ft.replace(R.id.parentContainer, fragment, fragment.getClass().getName());
            ft.commitAllowingStateLoss();
        }

    }*/
}
