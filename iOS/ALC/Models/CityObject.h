//
//  CityObject.h
//  ALC
//
//  Created by Anh Tran on 10/6/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CountryObject.h"
@interface CityObject : NSObject
@property (strong, nonatomic) CountryObject* country;
@property (strong, nonatomic) NSString* cityName;
@property (strong, nonatomic) NSString *states;
-(id) initFromDict:(NSDictionary*) dict;
@end
