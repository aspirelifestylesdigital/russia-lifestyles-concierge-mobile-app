package com.aspire.android.russia.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aspire.android.russia.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author anh.trinh
 * @since v0.1
 */
public class CustomErrorView
        extends RelativeLayout {

    private static final int LAYOUT_RESOURCE_ID = R.layout.custom_error_view; // 1s
    @BindView(R.id.ic_error)
    ImageButton mIcError;
    @BindView(R.id.tv_error)
    TextView mTvError;
    @BindView(R.id.layout_error_message)
    RelativeLayout mLayoutErrorMessage;

    /**
     * @param context
     *         The context which view is running on.
     * @return New ItemListCollections object.
     * @since v0.3
     */
    public static CustomErrorView newInstance(final Context context) {
        if (context == null) {
            return null;
        }

        return new CustomErrorView(context);
    }

    /**
     * The constructor with current context.
     *
     * @param context
     *         The context which view is running on.
     * @since v0.3
     */
    public CustomErrorView(final Context context) {
        super(context);
        this.initialize();
    }

    /**
     * The constructor with current context.
     *
     * @param context
     *         The context which view is running on.
     * @param attrs
     *         The initialize attributes set.
     * @since v0.3
     */
    public CustomErrorView(final Context context,
                           final AttributeSet attrs) {
        super(context,
              attrs);
        this.initialize();
    }

    /**
     * The constructor with current context.
     *
     * @param context
     *         The context which view is running on.
     * @param attrs
     *         The initialize attributes set.
     * @param defStyleAttr
     *         The default style.
     * @since v0.3
     */
    public CustomErrorView(final Context context,
                           final AttributeSet attrs,
                           final int defStyleAttr) {
        super(context,
              attrs,
              defStyleAttr);

        this.initialize();
    }

    /**
     * @see View#setEnabled(boolean)
     * @since v0.5
     */
    @Override
    public void setEnabled(final boolean isEnabled) {
        this.setAlpha(isEnabled ?
                      1f :
                      0.5f);
        super.setEnabled(isEnabled);
    }

    /**
     * Reset injected resources created by ButterKnife.
     *
     * @since v0.8
     */
    public void resetInjectedResource() {
        ButterKnife.bind(this);
    }

    /**
     * Initialize UI sub views.
     *
     * @since v0.3
     */
    private void initialize() {

        final LayoutInflater layoutInflater = LayoutInflater.from(this.getContext());
        layoutInflater.inflate(LAYOUT_RESOURCE_ID,
                               this,
                               true);

        ButterKnife.bind(this,
                         this);
        setBackgroundResource(R.drawable.bg_sign_in_circle_stroke_transparent_red);
    }

    /**
     * Fill data for component.
     *
     * @param errorMessage
     *         Type of button.
     * @since v0.3
     */
    public void fillData(final String errorMessage) {
        mTvError.setText(errorMessage);
    }

    public void setMessage(final String errorMessage) {
        mTvError.setText(errorMessage);
    }

    @OnClick(R.id.ic_error)
    public void onClick() {
        if(mLayoutErrorMessage.getVisibility()!=VISIBLE)
            mLayoutErrorMessage.setVisibility(VISIBLE);
        else{
            mLayoutErrorMessage.setVisibility(GONE);
        }
    }

    public void show(){
        this.setVisibility(VISIBLE);
    }

    public void hide(){
        this.setVisibility(GONE);
    }

}
