//
//  GouetmetRecommendDetailsObj.h
//  ALC
//
//  Created by Hai NguyenV on 10/19/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "BaseBookingDetailsObject.h"

@interface GouetmetRecommendDetailsObj : BaseBookingDetailsObject

@property (strong, nonatomic) NSArray* occasion;
@property (strong, nonatomic) NSArray* cuisine;
@property (strong, nonatomic) NSString* MaximumPrice;
@property (strong, nonatomic) NSString* MinimumPrice;
@property (strong, nonatomic) NSDate* reservationDate;
@property (strong, nonatomic) NSDate* reservationTime;
@property (strong, nonatomic) NSString* reservationName;
@property (strong, nonatomic) NSString* foodAllergies;

-(id) initFromDict:(NSDictionary*) dict;

-(NSMutableArray*) cuisineKeys;
-(NSMutableArray*) cuisineValues;
-(NSMutableArray*) occasionKeys;
-(NSMutableArray*) occasionValues;
@end
