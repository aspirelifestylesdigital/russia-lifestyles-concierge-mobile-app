//
//  DCRRelatedItemObject.h
//  ALC
//
//  Created by Anh Tran on 3/29/17.
//  Copyright © 2017 Sunrise Software Solutions. All rights reserved.
//
#import "DCRCommonObject.h"
#import <Foundation/Foundation.h>

@interface DCRRelatedItemObject : NSObject
@property (nonatomic, strong) NSString* itemId;
@property (nonatomic, strong) NSString* Name;
@property (nonatomic, strong) DCRCommonObject* type;
@property (nonatomic, strong) NSString* itemDescription;
- (id) initFromDict:(NSDictionary*) dict;
- (BOOL) isValid;
@end
