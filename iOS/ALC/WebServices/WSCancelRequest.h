//
//  WSCancelRequest.h
//  ALC
//
//  Created by Anh Tran on 9/26/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSB2CBase.h"

@interface WSCancelRequest : WSB2CBase
-(void) cancelRequestBooking:(NSString*) bookingId;
@end
