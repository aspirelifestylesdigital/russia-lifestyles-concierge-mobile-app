//
//  CellScrollViewBot.m
//  demoChatBot
//
//  Created by Nhat Huy on 9/11/16.
//  Copyright © 2016 Nhat Huy. All rights reserved.
//

#import "CellScrollViewBot.h"

@implementation CellScrollViewBot

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void) loadView
{
    [self.vwAvatar.layer setCornerRadius:self.vwAvatar.frame.size.height/2.0];
    [self.vwShow reloadData];
    currentIndex = 0;

    if ([self.arrayListShow count] > 1)
    {
       [self.btnRight setHidden:NO];
    }
   
}

- (IBAction)clickLeft:(id)sender
{
    if (currentIndex > 0)
    {
        currentIndex -= 1;
        [self.vwShow scrollToItemAtIndex:currentIndex animated:YES];
        [self.btnRight setHidden:NO];
        if (currentIndex == 0)
        {
            [self.btnLeft setHidden:YES];
        }
    }
}

- (IBAction)clickRight:(id)sender
{
    if (currentIndex < [self.arrayListShow count] - 1)
    {
        currentIndex += 1;
        [self.vwShow scrollToItemAtIndex:currentIndex animated:YES];
        [self.btnLeft setHidden:NO];
        if (currentIndex == [self.arrayListShow count] - 1)
        {
            [self.btnRight setHidden:YES];
        }
    }
}

- (id) createNewCustomObjectViewFromNibName:(NSString*)nibName owner:(id)owner withClass:(Class)classView
{
    
    NSArray* nibContents = [[NSBundle mainBundle]
                            loadNibNamed:nibName owner:owner options:NULL];
    
    NSEnumerator *nibEnumerator = [nibContents objectEnumerator];
    NSObject* nibItem = nil;
    
    while ( (nibItem = [nibEnumerator nextObject]) != nil) {
        
        if ( [nibItem isKindOfClass: classView])
        {
            return nibItem;
        }
    }
    return nil;
}

#pragma mark - iCasoule

- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel;
{
    return [self.arrayListShow count];
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(nullable UIView *)view
{
    if (!self.isShowImages)
    {
        if (view == nil)
        {
            view = (CellView_Restaurant*)[self createNewCustomObjectViewFromNibName:@"CellView_Restaurant" owner:self withClass:[CellView_Restaurant class]];
            ((CellView_Restaurant*)view).currentGourmet = [self.arrayListShow objectAtIndex:index];
            [view setFrame:CGRectMake(0, 0, view.frame.size.width, view.frame.size.height)];
            [(CellView_Restaurant*)view loadView];
        }
        else
        {
            //get a reference to the label in the recycled view
            ((CellView_Restaurant*)view).currentGourmet = [self.arrayListShow objectAtIndex:index];
            [(CellView_Restaurant*)view loadView];
        }
    }
    else
    {
        if (view == nil)
        {
            view = (CellView_Image*)[self createNewCustomObjectViewFromNibName:@"CellView_Image" owner:self withClass:[CellView_Image class]];
            ((CellView_Image*)view).linkImage = [self.arrayListShow objectAtIndex:index];
            [view setFrame:CGRectMake(0, 0, view.frame.size.width, view.frame.size.height)];
            [(CellView_Image*)view loadView];
        }
        else
        {
            //get a reference to the label in the recycled view
            ((CellView_Image*)view).linkImage = [self.arrayListShow objectAtIndex:index];
            [(CellView_Image*)view loadView];
        }
    }
    
    //set item label
    //remember to always set any properties of your carousel item
    //views outside of the `if (view == nil) {...}` check otherwise
    //you'll get weird issues with carousel item content appearing
    //in the wrong place in the carousel
    
    return view;
}

- (void)carouselDidEndScrollingAnimation:(iCarousel *)carousel
{
     if ([self.arrayListShow count] > 1)
     {
         currentIndex = carousel.currentItemIndex;
         if (currentIndex == 0)
         {
             [self.btnLeft setHidden:YES];
             [self.btnRight setHidden:NO];
         }
         else if (currentIndex == [self.arrayListShow count] - 1)
         {
             [self.btnLeft setHidden:NO];
             [self.btnRight setHidden:YES];
         }
         else
         {
             [self.btnLeft setHidden:NO];
             [self.btnRight setHidden:NO];
         }
     }
}

- (void) lockView
{
    for (UIView* itemView in self.vwShow.subviews)
    {
        if ([itemView isKindOfClass:[CellView_Image class]] || [itemView isKindOfClass:[CellView_Restaurant class]])
        {
            [itemView setUserInteractionEnabled:NO];
        }
    }
}
@end
