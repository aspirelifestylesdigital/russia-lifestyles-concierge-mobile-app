//
//  PaddingUILabel.m
//  ALC
//
//  Created by Anh Tran on 12/22/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "PaddingUILabel.h"

@implementation PaddingUILabel

- (void)drawTextInRect:(CGRect)rect
{
    UIEdgeInsets insets = {0, 10, 0, 10};
    [super drawTextInRect:UIEdgeInsetsInsetRect(rect, insets)];
}
@end
