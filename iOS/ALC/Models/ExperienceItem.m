//
//  ExperienceItem.m
//  ALC
//
//  Created by Anh Tran on 9/23/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "ExperienceItem.h"

@implementation ExperienceItem

-(id) initWithType:(NSString*) type andImage:(NSString*) image{
    self = [super init];
    if(self){
        self.title = type;
        self.imageURL = image;
    }
    return self;
}

@end