//
//  OrthersRequestObj.m
//  ALC
//
//  Created by Hai NguyenV on 10/13/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "OrthersRequestObj.h"

@implementation OrthersRequestObj
-(id) initFromDetails:(OrtherRequestDetailsObj*)detailsObj{
    self = [super init];
    self.BookingId = detailsObj.bookingId;
    self.isContactPhone = detailsObj.isContactPhone;
    self.isContactBoth = detailsObj.isContactBoth;
    self.isContactEmail = detailsObj.isContactEmail;
    
    self.MobileNumber = detailsObj.mobileNumber;
    self.EmailAddress = detailsObj.email;
    
    self.ortherString = detailsObj.specialRequirement;
    self.reservationName = detailsObj.guestName;
    
     // For privilege Spa
    self.spaName = detailsObj.spaName;    
    self.Country = detailsObj.country;
    self.City = detailsObj.city;
    self.State = detailsObj.stateValue;

    [self setPrivilegeData:detailsObj.requestDetails];
    return self;
}



@end
