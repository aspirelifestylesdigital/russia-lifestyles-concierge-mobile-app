//
//  BaseResponseObject.m
//  ALC
//
//  Created by Anh Tran on 8/19/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "BaseResponseObject.h"
#import "NSDictionary+SBJSONHelper.h"
@implementation BaseResponseObject
-(id) initFromDict:(NSDictionary*) dict{
    self = [[BaseResponseObject alloc] init];
    self.status = [[dict objectForKey:@"Status"] integerValue];
    //self.message = [dict objectForKey:@"Message"];
    self.message = ERROR_API_MSG;
    return self;
}

-(id) initFromB2CDict:(NSDictionary*) dict{
    self = [[BaseResponseObject alloc] init];
    self.b2cStatus = [dict boolForKey:@"success"];
    NSArray* mesArr = [dict arrayForKey:@"message"];
    if(mesArr && mesArr.count > 0){
        NSDictionary * error = [mesArr objectAtIndex:0];
        self.message = [error stringForKey:@"message"];
        self.b2cErrorCode = [error stringForKey:@"code"];
    }
    
    NSDictionary* bookStatus = [dict dictionaryForKey:@"status"];
    if(bookStatus){
        NSString* statusValue = [[bookStatus stringForKey:@"success"] lowercaseString];
        self.b2cStatus = ([statusValue isEqualToString:@"true"] ||
                         [statusValue isEqualToString:@"yes"] ||
                          [statusValue isEqualToString:@"success"]) ? true : false;
        //self.message = [bookStatus stringForKey:@"message"];
        self.message = ERROR_API_MSG;
    }
    self.message = ERROR_API_MSG;
    return self;
}

-(id) initFromDCR:(BaseDCRObject*) dcrResponse{
    self = [[BaseResponseObject alloc] init];
    self.message = dcrResponse.message;
    self.b2cStatus = dcrResponse.success;
    if(dcrResponse.errors.count > 0){
        self.b2cErrorCode = [dcrResponse.errors objectAtIndex:0];
    }
    
    return self;
}

- (BOOL) isSuccess{
    if(self.status == 200 || self.b2cStatus){
        return YES;
    }
    return NO;
}


@end
