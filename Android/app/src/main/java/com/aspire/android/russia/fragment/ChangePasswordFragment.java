package com.aspire.android.russia.fragment;

import com.aspire.android.russia.R;
import com.aspire.android.russia.activitys.HomeActivity;
import com.aspire.android.russia.apiservices.ResponseModel.ChangePasswordResponse;
import com.aspire.android.russia.apiservices.apis.appimplement.b2c.B2CBaseResponse;
import com.aspire.android.russia.apiservices.apis.appimplement.b2c.B2CICallback;
import com.aspire.android.russia.apiservices.b2c.B2CWSChangePassword;
import com.aspire.android.russia.apiservices.b2c.RequestModel.B2CChangePasswordRequest;
import com.aspire.android.russia.application.coreactivitys.BaseFragment;
import com.aspire.android.russia.model.UserItem;
import com.aspire.android.russia.utils.CommonUtils;
import com.aspire.android.russia.utils.Fontfaces;
import com.aspire.android.russia.utils.StringUtil;
import com.aspire.android.russia.widgets.CustomErrorView;

import android.content.DialogInterface;
import android.content.Intent;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTouch;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by anh.trinh on 9/13/2016.
 */
public class ChangePasswordFragment
        extends BaseFragment implements Callback, B2CICallback {
    public static final String PRE_FORCE_CHANGE_PASS = "force_change_pass";
    public static final String IS_FORCE_CHANGE_PASS = "force_change_pass";
    @BindView(R.id.header)
    View mToolbar;
    @BindView(R.id.ic_back)
    ImageView mIcBack;
    @BindView(R.id.tv_tool_bar_title)
    TextView mTvToolBarTitle;
    @BindView(R.id.tvOldPass)
    TextView tvOldPass;
    @BindView(R.id.tvMandatory)
    TextView tvMandatory;
    @BindView(R.id.edtOldPass)
    EditText edtOldPass;
    @BindView(R.id.tvNewPass)
    TextView tvNewPass;
    @BindView(R.id.edtNewPass)
    EditText edtNewPass;
    @BindView(R.id.tvTipText)
    TextView tvTipText;
    @BindView(R.id.tvRetypeNewPass)
    TextView tvRetypeNewPass;
    @BindView(R.id.edtRetypeNewPass)
    EditText edtRetypeNewPass;
    @BindView(R.id.cbShowPassword)
    CheckBox cbShowPassword;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;

    @BindView(R.id.ervOldPass)
    CustomErrorView ervOldPass;
    @BindView(R.id.ervNewPass)
    CustomErrorView ervNewPass;
    @BindView(R.id.ervRetypeNewPass)
    CustomErrorView ervRetypeNewPass;
    private boolean isForceChangePass= false;

    @Override
    protected int layoutId() {
        return R.layout.fragment_change_password;
    }

    @Override
    protected void initView() {

        setTitle(getString(R.string.text_title_change_password));
        cbShowPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (!isChecked) {
                    // show password
                    edtOldPass.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    edtNewPass.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    edtRetypeNewPass.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    // hide password
                    edtOldPass.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    edtNewPass.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    edtRetypeNewPass.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
                edtOldPass.setSelection(edtOldPass.getText().length());
                edtNewPass.setSelection(edtNewPass.getText().length());
                edtRetypeNewPass.setSelection(edtRetypeNewPass.getText().length());
            }
        });
        tvOldPass.setTypeface(Fontfaces.getFont(getActivity(), Fontfaces.FONTLIST.AVENIR_NEXT_LT_MEDIUM));
        tvMandatory.setTypeface(Fontfaces.getFont(getActivity(), Fontfaces.FONTLIST.AVENIR_NEXT_LT_MEDIUM));
        tvNewPass.setTypeface(Fontfaces.getFont(getActivity(), Fontfaces.FONTLIST.AVENIR_NEXT_LT_MEDIUM));
        tvTipText.setTypeface(Fontfaces.getFont(getActivity(), Fontfaces.FONTLIST.AVENIR_NEXT_LT_MEDIUM));
        tvRetypeNewPass.setTypeface(Fontfaces.getFont(getActivity(), Fontfaces.FONTLIST.AVENIR_NEXT_LT_MEDIUM));
        btnSubmit.setTypeface(Fontfaces.getFont(getActivity(), Fontfaces.FONTLIST.Avenirnext_demibold));
        edtNewPass.setTypeface(Fontfaces.getFont(getActivity(), Fontfaces.FONTLIST.AVENIR_NEXT_LT_MEDIUM));
        edtOldPass.setTypeface(Fontfaces.getFont(getActivity(), Fontfaces.FONTLIST.AVENIR_NEXT_LT_MEDIUM));
        edtRetypeNewPass.setTypeface(Fontfaces.getFont(getActivity(), Fontfaces.FONTLIST.AVENIR_NEXT_LT_MEDIUM));
        cbShowPassword.setTypeface(Fontfaces.getFont(getActivity(), Fontfaces.FONTLIST.AVENIR_NEXT_LT_MEDIUM));
        //
        ervOldPass.hide();
        ervNewPass.hide();
        ervRetypeNewPass.hide();
        if (getArguments() != null) {
            isForceChangePass = getArguments().getString(PRE_FORCE_CHANGE_PASS,"").equals(IS_FORCE_CHANGE_PASS);
        }
        if(isForceChangePass){
            mTvToolBarTitle.setText(getString(R.string.text_title_change_password));
        }else{
            mToolbar.setVisibility(View.GONE);
        }
        mIcBack.setVisibility(View.GONE);


    }

    @Override
    protected void bindData() {

    }

    @Override
    public boolean onBack() {
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!isForceChangePass) {
            ((HomeActivity) getActivity()).setVisibilityForLogoAppToolbar(View.GONE);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if(getActivity() != null && !getActivity().isDestroyed()) {
            if(!isForceChangePass) {
                ((HomeActivity) getActivity()).setVisibilityForLogoAppToolbar(View.VISIBLE);
            }
        }
    }

    @OnClick(R.id.imgBackground)
    void onBackgroundClick(){
        CommonUtils.hideSoftKeyboard(getActivity(), view);
    }

    @OnClick(R.id.btnSubmit)
    void onSubmit(){
        //!CommonUtils.secretValidator(mEdtPassword.getText().toString().trim()
        submit();
    }

    @OnTouch({R.id.edtOldPass, R.id.edtNewPass, R.id.edtRetypeNewPass})
    boolean onTouchEditText(View v,
                         MotionEvent event){
        switch (v.getId()){
            case R.id.edtOldPass:
                ervOldPass.hide();
                break;
            case R.id.edtNewPass:
                ervNewPass.hide();
                break;
            case R.id.edtRetypeNewPass:
                ervRetypeNewPass.hide();
                break;
        }
        return false;
    }

    @Override
    public void onResponse(final Call call,
                           final Response response) {
        if(getActivity()==null) {
            return;
        }
        if (response.body() instanceof ChangePasswordResponse) {
            ChangePasswordResponse rsp = ((ChangePasswordResponse) response.body());
            if (rsp.isSuccess()) {
                CommonUtils.showActionAlertOnlyYES(getContext(),null,
                                                   getString(R.string.text_message_change_password_success),
                                                   getString(R.string.text_ok),
                                                   new DialogInterface.OnClickListener() {
                                                       @Override
                                                       public void onClick(final DialogInterface dialog,
                                                                           final int which) {
                                                           if(isForceChangePass){
                                                               Intent
                                                                       intent = new Intent(getActivity(),
                                                                                           HomeActivity.class);
                                                               intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                               startActivity(intent);

                                                               getActivity().finish();
                                                           }else {
                                                               onBackPress();
                                                           }
                                                       }
                                                   });


            } else {
                String msg = rsp.getMessage();
                if(StringUtil.isEmpty(msg)){
                    msg = getString(R.string.api_failed_message);
                }
                showDialogMessage("", msg);
            }
        }

        //
        hideDialogProgress();

    }

    @Override
    public void onFailure(final Call call,
                          final Throwable t) {
        hideDialogProgress();
        if(getActivity()==null) {
            return;
        }
        String msg = t.getMessage();
        if(StringUtil.isEmpty(msg)){
            msg = getString(R.string.api_failed_message);
        }
        showDialogMessage("", msg);
    }

    private void submit(){
        boolean invalid = false;

        String oldPass = edtOldPass.getText().toString().trim();
        String newPass = edtNewPass.getText().toString().trim();
        String retypeNewPass = edtRetypeNewPass.getText().toString().trim();

        if(CommonUtils.secretValidator(oldPass)){
            //edtOldPass.setError(null);
            ervOldPass.hide();
        }
        else{
            //edtOldPass.setError(getString(R.string.invalid_password));
            ervOldPass.fillData(getString(R.string.text_sign_in_invalid_password));
            ervOldPass.show();
            invalid = true;
        }

        if(CommonUtils.secretValidator(newPass)){
            ervNewPass.hide();
        }
        else{
            ervNewPass.fillData(getString(R.string.text_sign_in_invalid_password));
            ervNewPass.show();
            invalid = true;
        }

        if(CommonUtils.secretValidator(retypeNewPass)){
            ervRetypeNewPass.hide();
        }
        else{
            ervRetypeNewPass.fillData(getString(R.string.text_sign_in_invalid_password));
            ervRetypeNewPass.show();
            invalid = true;
        }

        if(!invalid){
            if(!newPass.equals(retypeNewPass)){
                invalid = true;
                ervRetypeNewPass.fillData(getString(R.string.do_not_password));
                ervRetypeNewPass.show();
            }
        }

        if(invalid)
            return;

        //call api update
        if(!UserItem.isLogined()){
            //pop to root.
            return;
        }

        showDialogProgress();

        /*ChangePasswordRequest request = new ChangePasswordRequest();
        request.setUserId(UserItem.getLoginedId());
        request.setCurrentPassword(oldPass);
        request.setNewPassword(newPass);
        request.setConfirmPassword(retypeNewPass);

        WSChangePassword ws = new WSChangePassword();
        ws.setRequest(request);
        ws.run(this);*/
        B2CWSChangePassword b2CWSChangePassword = new B2CWSChangePassword(this);
        b2CWSChangePassword.setRequest(new B2CChangePasswordRequest().build(edtNewPass.getText().toString(), edtOldPass.getText().toString()));
        b2CWSChangePassword.run(null);
    }

    @Override
    public void onB2CResponse(B2CBaseResponse response) {
        hideDialogProgress();
        if(getActivity()==null) {
            return;
        }
        if(response != null && response.isSuccess()){
            CommonUtils.showActionAlertOnlyYES(getContext(),null,
                                               getString(R.string.text_message_change_password_success),
                                               getString(R.string.text_ok),
                                               new DialogInterface.OnClickListener() {
                                                   @Override
                                                   public void onClick(final DialogInterface dialog,
                                                                       final int which) {

                                                       if(isForceChangePass){
                                                           Intent
                                                                   intent = new Intent(getActivity(),
                                                                                       HomeActivity.class);
                                                           intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                           startActivity(intent);

                                                           getActivity().finish();
                                                       }else{
                                                           onBackPress();
                                                       }


                                                   }
                                               });

          // onBackPress();
        }else{
            showDialogMessage(getString(R.string.text_title_dialog_error), getString(R.string.api_failed_message));
        }
    }

    @Override
    public void onB2CResponseOnList(List<B2CBaseResponse> responseList) {
        if(getActivity()==null) {
            return;
        }
    }

    @Override
    public void onB2CFailure(String errorMessage, String errorCode) {
        hideDialogProgress();
        if(getActivity()==null) {
            return;
        }
        if(StringUtil.isEmpty(errorMessage)){
            showDialogMessage(getString(R.string.text_title_dialog_error), getString(R.string.api_failed_message));
        }else{
            if(errorCode.equals("ENR68-2")||errorCode.equals("ENR73-2")||errorCode.equals("ENR008-3")||errorCode.equals("ENR007-3")||errorCode.equals("ENR70-1")){
                showDialogMessage(getString(R.string.text_title_dialog_error), getString(R.string.api_failed_message));
            }else {
                if (errorCode.equals("ENR028-2")) {
                    showDialogMessage(getString(R.string.text_title_dialog_error),
                                      getString(R.string.api_failed_message_password_should_be_in_correct_format));
                } else {
                    showDialogMessage(getString(R.string.text_title_dialog_error),
                                      errorMessage);
                }
            }
        }
    }
}
