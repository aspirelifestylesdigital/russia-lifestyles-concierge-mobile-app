package com.aspire.android.russia.application;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.text.TextUtils;

import com.aspire.android.russia.R;
import com.aspire.android.russia.utils.Logger;
import com.aspire.android.russia.utils.SharedPreferencesUtils;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.analytics.ecommerce.Product;
import com.google.android.gms.analytics.ecommerce.ProductAction;

import java.util.Locale;

/**
 * Created by nganguyent on 10/08/2016.
 */
public class AppContext extends Application /*MultiDexApplication*/ {
    private static AppContext sSharedInstance = null;
    private Tracker mTracker;
   // private static String language = "ru";

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        /*MultiDex.install(this);*/

        SharedPreferencesUtils.init(getBaseContext());
        Logger.visibleFileStorage();
    }
    public void changeLocale(String language) {
        final Resources res = getResources();
        final Configuration conf = res.getConfiguration();
        if (language == null || language.length() == 0) {
            conf.locale = Locale.getDefault();
        } else {
            conf.locale = new Locale(language);
        }

        res.updateConfiguration(conf, null);
    }
    public static AppContext getSharedInstance() {
        return AppContext.sSharedInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        sSharedInstance = this;
        GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
        analytics.setLocalDispatchPeriod(5);
        // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
        mTracker = analytics.newTracker(R.xml.global_tracker);
        // Start a new session with the hit.
        mTracker.send(new HitBuilders.EventBuilder()
                .setNewSession()
                .build());
        changeLocale(SharedPreferencesUtils.getPreferences(AppConstant.LANGUAGE_SETTINGS, AppConstant.RUSSIAN_LANGUAGE));
    }
    public void track(String eventCategory, String eventAction, String eventLabel){
      if(TextUtils.isEmpty(eventLabel)) {
            mTracker.send(new HitBuilders.EventBuilder()
                    .setCategory(eventCategory)
                    .setAction(eventAction)
                    .setValue(1)
                    .build());
        }else{
            mTracker.send(new HitBuilders.EventBuilder()
                    .setCategory(eventCategory)
                    .setAction(eventAction)
                    .setLabel(eventLabel)
                    .setValue(1)
                    .build());
    }
    }
    public void track(String productCategory, String productName){
        Product product = new Product().setQuantity(1).setCategory(productCategory).setName(productName).setBrand(AppConstant.TRACK_EVENT_PRODUCT_BRAND);
        HitBuilders.EventBuilder builder = new HitBuilders.EventBuilder();
        builder.setProductAction(new ProductAction(ProductAction.ACTION_CHECKOUT).setCheckoutStep(1));
        builder.addProduct(product);
        builder.addImpression(product, "ConciergeRequest");
        mTracker.send(builder.build());

    }
}
