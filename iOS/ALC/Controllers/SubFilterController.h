//
//  SubFilterController.h
//  ALC
//
//  Created by Anh Tran on 8/24/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "MainViewController.h"

enum SERVICE_TASK{
    CUISINES = 1,
    OCCASION = 2,
    RETAL_VERTICLE = 3,
    COMPANY = 4,
    TEE = 5,
    ROOM = 6,
    BED = 7,
    COUNTRY = 9,
    CITY = 10,
    TRANSPORT = 11,
    EVENT_CATEGORY = 13,
    STATE = 14,
};
enum COUNTRY_TASK{
    USA = 1,
    CANADA = 2,
};

@protocol SubFilterControllerDelegate;

@interface SubFilterController : MainViewController <UISearchBarDelegate>

@property (unsafe_unretained) id<SubFilterControllerDelegate> delegate;
@property (strong, nonatomic) NSMutableArray *selectedValues;
@property (strong, nonatomic) NSMutableArray *selectedKey;
@property (nonatomic) NSInteger dataType;
@property (nonatomic) NSInteger countryType;
@property (nonatomic, assign)NSInteger maxCount;
@property (nonatomic, assign)BOOL isCloseAfterClick;
@property (strong, nonatomic) NSString* filterCountryForCitiesList;
@end
@protocol SubFilterControllerDelegate <NSObject>

@optional
- (void) pickOption:(NSMutableArray*) values forType:(NSInteger) type;
- (void) pickOptionKey:(NSMutableArray*) key withValues:(NSMutableArray*) values forType:(NSInteger) type;

@end