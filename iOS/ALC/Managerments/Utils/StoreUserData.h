//
//  StoreUserData.h
//  ALC
//
//  Created by Anh Tran on 8/19/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserObject.h"
@interface StoreUserData : NSObject
extern void storeUserDetails(UserObject* user);
extern void updateUserDetails(NSDictionary* user);
extern UserObject* getUser();
extern void saveUserEmail(NSString* email);
extern void saveUserPassword(NSString* pass);
extern BOOL isUserLoggedIn();
extern void clearUser();
extern NSMutableDictionary* getUserDetailsObj();
extern void storeUserOnlineMemberId(NSString* onlineMemberId);
extern void saveUserForgot(NSString* email);
extern void removeUserForgot();
extern void setHasForgotPassword(BOOL isForgot);
extern BOOL isUserForgot();
@end
