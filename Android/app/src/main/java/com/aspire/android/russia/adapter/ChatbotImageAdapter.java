package com.aspire.android.russia.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aspire.android.russia.R;
import com.aspire.android.russia.model.Restaurant;
import com.aspire.android.russia.model.UserItem;
import com.aspire.android.russia.utils.Fontfaces;
import com.aspire.android.russia.utils.StringUtil;
//import com.bumptech.glide.Glide;
import com.joooonho.SelectableRoundedImageView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.view.View.GONE;

/**
 * Created by nga.nguyent on 2/26/2016.
 */
public class ChatbotImageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Restaurant> data = new ArrayList<>();
    private Context context;
    private int itemIndex = 0;
    private ChatbotAdapter.OnRestaurantInfoClickListener restaurantInfoClickListener;
    private boolean clickable = false;

    public ChatbotImageAdapter(Context context) {
        this.context = context;
    }

    public void setData(int pos, boolean clickableItem, List<Restaurant> data) {
        itemIndex = pos;
        this.clickable = clickableItem;
        this.data.clear();
        this.data.addAll(data);
    }

    public void setRestaurantInfoClickListener(ChatbotAdapter.OnRestaurantInfoClickListener restaurantInfoClickListener) {
        this.restaurantInfoClickListener = restaurantInfoClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = View.inflate(context, R.layout.layout_chatbot_image_item, null);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        //
        ((ViewHolder)holder).setData(position, data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.imgRestaurant)
        SelectableRoundedImageView imgRestaurant;
        @BindView(R.id.imgRate)
        ImageView imgRate;
        @BindView(R.id.imgFavorite)
        ImageView imgFavorite;
        @BindView(R.id.tvCost)
        TextView tvCost;
        @BindView(R.id.tvShare)
        TextView tvShare;
        @BindView(R.id.tvRestaurantName)
        TextView tvRestaurantName;
        @BindView(R.id.tvAddress)
        TextView tvAddress;
        @BindView(R.id.tvDistrict)
        TextView tvDistrict;
        @BindView(R.id.tvCountry)
        TextView tvCountry;
        @BindView(R.id.tvReadMore)
        TextView tvReadMore;
        @BindView(R.id.tvBookNow)
        TextView tvBookNow;

        int position;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            imgRate.setVisibility(View.GONE);
            tvCost.setText("");
            tvRestaurantName.setText("");
            tvAddress.setText("");
            tvDistrict.setText("");
            tvCountry.setText("");

            tvCost.setTypeface(Fontfaces.getFont(context, Fontfaces.FONTLIST.Avenirnext_demibold));
            tvShare.setTypeface(Fontfaces.getFont(context, Fontfaces.FONTLIST.Avenirnext_demibold));
            tvRestaurantName.setTypeface(Fontfaces.getFont(context, Fontfaces.FONTLIST.Avenirnext_demibold));
            tvAddress.setTypeface(Fontfaces.getFont(context, Fontfaces.FONTLIST.Avenirnext_demibold));
            tvDistrict.setTypeface(Fontfaces.getFont(context, Fontfaces.FONTLIST.Avenirnext_demibold));
            tvCountry.setTypeface(Fontfaces.getFont(context, Fontfaces.FONTLIST.Avenirnext_demibold));
            tvReadMore.setTypeface(Fontfaces.getFont(context, Fontfaces.FONTLIST.Avenirnext_demibold));
            tvBookNow.setTypeface(Fontfaces.getFont(context, Fontfaces.FONTLIST.Avenirnext_demibold));

        }

        public void setData(int pos, Restaurant rest){
            imgFavorite.setClickable(clickable);
            tvShare.setClickable(clickable);
            tvBookNow.setClickable(clickable);
            tvReadMore.setClickable(clickable);

            int left = 0;
            if(pos==0){
                left = 50;
            }

            RecyclerView.LayoutParams lp = (RecyclerView.LayoutParams) itemView.getLayoutParams();
            if(lp!=null){
                lp.setMargins(left, 0, 0, 0);
            }
            itemView.requestLayout();

            this.position = pos;
            tvCost.setText(rest.getPriceRange());
            tvRestaurantName.setText(rest.getRestaurantName());
            String[] address = rest.getAddress().split(",");
            if(address.length>0)
                tvAddress.setText(address[0].trim());
            if(address.length>1)
                tvDistrict.setText(address[1].trim());
            if(address.length>2){
                /*String addr = rest.getItemMoreDescription();
                int idx = addr.indexOf(address[2]);
                String city = addr.substring(idx).trim();*/
                String city = rest.getCityName();
                if(!StringUtil.isEmpty(rest.getCountryName())) {
                    if(city.length()>0)
                        city += ", ";
                    city += rest.getCountryName();
                }
                tvCountry.setText(city);
            }

            if(!UserItem.isLogined()){
                imgFavorite.setVisibility(GONE);
                imgFavorite.setClickable(false);
            }
            else {
                imgFavorite.setClickable(true);
                imgFavorite.setVisibility(View.VISIBLE);
                if (rest.isVoted()) {
                    imgFavorite.setSelected(true);
                } else {
                    imgFavorite.setSelected(false);
                }
            }

//            Glide
//                    .with(context)
//                    .load(rest.getBannerURL())
//                    .asBitmap()
//                    //.override(400,200)
//                    .centerCrop()
//                    //.placeholder(R.drawable.loading_spinner)
//                    //.crossFade()
//                    .into(imgRestaurant);
        }

        @OnClick({R.id.imgFavorite, R.id.tvShare, R.id.tvReadMore, R.id.tvBookNow})
        void onInfoItemClick(View v){
            switch (v.getId()){
                case R.id.imgFavorite:
                    if(clickable && restaurantInfoClickListener!=null){
                        restaurantInfoClickListener.onFavoriteClick(itemIndex, position);
                    }
                    break;
                case R.id.tvShare:
                    if(clickable && restaurantInfoClickListener!=null){
                        restaurantInfoClickListener.onShareClick(itemIndex, position);
                    }
                    break;
                case R.id.tvReadMore:
                    if(clickable && restaurantInfoClickListener!=null){
                        restaurantInfoClickListener.onReadMoreClick(itemIndex, position);
                    }
                    break;
                case R.id.tvBookNow:
                    if(clickable && restaurantInfoClickListener!=null){
                        restaurantInfoClickListener.onBookNowClick(itemIndex, position);
                    }
                    break;
            }
        }

    }

}
