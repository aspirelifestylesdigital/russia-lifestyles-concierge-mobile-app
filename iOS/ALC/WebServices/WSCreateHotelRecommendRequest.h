//
//  WSCreateHotelRecommendRequest.h
//  ALC
//
//  Created by Anh Tran on 10/12/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSB2CBase.h"
#import "HotelRequestObject.h"
@interface WSCreateHotelRecommendRequest : WSB2CBase
-(void)bookingHotelRecommend:(HotelRequestObject*) item;
@end
