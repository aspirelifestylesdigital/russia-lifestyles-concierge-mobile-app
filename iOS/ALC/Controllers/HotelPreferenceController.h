//
//  HotelPreferenceController.h
//  ALC
//
//  Created by HaiNguyen on 9/11/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EDStarRating.h"
#import "SubFilterController.h"
#import "CustomTextField.h"
#import "WSCreateHotelPre.h"
#import "WSGetMyPreferences.h"

@interface HotelPreferenceController : MainViewController<SubFilterControllerDelegate,UITextFieldDelegate,DataLoadDelegate,UITextViewDelegate>
{
    NSMutableArray *selectedRoom;
    NSMutableArray *selectedRoomKey;
    NSMutableArray *selectedBed;
    NSMutableArray *selectedBedKey;
    BOOL isVehicle,isSmoking;
    WSCreateHotelPre* wsCreateHotel;
    WSGetMyPreferences* wsGetMyPreference;
}
@property (weak, nonatomic) IBOutlet UIScrollView *pScrollView;
@property (weak, nonatomic) IBOutlet EDStarRating *pStarRating;
@property (weak, nonatomic) IBOutlet UIButton *btnRoom;
@property (weak, nonatomic) IBOutlet UIButton *btnBed;
@property (weak, nonatomic) IBOutlet CustomTextField *tfLoyalty;
@property (weak, nonatomic) IBOutlet CustomTextField *tfMembership;
@property (weak, nonatomic) IBOutlet UIButton *btCancel;
@property (weak, nonatomic) IBOutlet UISwitch *btnSmoking;
@property (weak, nonatomic) IBOutlet UITextView *tfOrtherPreference;
@property (weak, nonatomic) IBOutlet UILabel *lbHolder;
@property (nonatomic, strong)NSString* preferenceID;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pPositionY;
@property (weak, nonatomic) IBOutlet UILabel *hotelTitleLbl;
@property (weak, nonatomic) IBOutlet UILabel *starRatingLbl;
@property (weak, nonatomic) IBOutlet UILabel *roomPreferLbl;
@property (weak, nonatomic) IBOutlet UILabel *bedPreferLbl;
@property (weak, nonatomic) IBOutlet UILabel *smokingRoomLbl;
@property (weak, nonatomic) IBOutlet UILabel *anyOtherPreferLbl;
@property (weak, nonatomic) IBOutlet UILabel *loyaltyProgramLbl;
@property (weak, nonatomic) IBOutlet UILabel *membershipNoLbl;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property (weak, nonatomic) IBOutlet UILabel *hintHelpLbl;

- (IBAction)actionRoom:(id)sender;
- (IBAction)actionBed:(id)sender;
- (IBAction)actionCancel:(id)sender;
- (IBAction)actionSave:(id)sender;
- (IBAction)sbSmoking:(id)sender;

@end
