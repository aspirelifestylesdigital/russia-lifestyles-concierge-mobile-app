//
//  DCRItemGallery.m
//  ALC
//
//  Created by Anh Tran on 3/14/17.
//  Copyright © 2017 Sunrise Software Solutions. All rights reserved.
//

#import "DCRItemGallery.h"
#import "DCRImageObject.h"
#import "UIImageView+AFNetworking.h"
#import "UIImageViewAligned.h"

@interface DCRItemGallery (){
    int selectedPhoto;
    NSArray* imagesArray;
}

@end
@implementation DCRItemGallery


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    self.view = [[[NSBundle mainBundle] loadNibNamed:@"DCRItemGallery" owner:self options:nil] firstObject];
    [self.view setFrame:frame];
    [self addSubview:self.view];
    
    if (self) {
        // Initialization code
        [self setFrame:frame];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        NSString *className = NSStringFromClass([self class]);
        self.view = [[[NSBundle mainBundle] loadNibNamed:className owner:self options:nil] firstObject];
        [self addSubview:self.view];
        return self;
    }
    
    return self;
}


- (IBAction)rightArrowClicked:(id)sender {
    if(imagesArray!=nil && selectedPhoto + 1 < imagesArray.count){
        [self scrollToSelectedPhoto:selectedPhoto+1];
    }
}

- (IBAction)leftArrowClicked:(id)sender {
    if(imagesArray!=nil && selectedPhoto -1 >=0){
        [self scrollToSelectedPhoto:selectedPhoto-1];
    }
}

-(void) scrollToSelectedPhoto:(int) index{
    selectedPhoto = index;
    [_scrollPhotos setContentOffset:CGPointMake(self.view.frame.size.width*selectedPhoto, 0) animated:YES];
    [self displayArrowOnSelectedPosition];
}

-(void) showData:(NSArray*) images{
    imagesArray = images;
    
    if(images.count > 1){
        _rightArrow.hidden = NO;
    }
    
    _scrollPhotos.delegate = self;
    _scrollPhotos.contentSize = CGSizeMake(self.view.frame.size.width*imagesArray.count, _scrollPhotos.frame.size.height);
    
    CGRect frame;
    UIImageViewAligned *imv;
    DCRImageObject *photoObject;
    for (int i = 0; i < imagesArray.count; i++) {
        photoObject = [imagesArray objectAtIndex:i];
        frame = CGRectMake(self.view.frame.size.width*i, 0, self.view.frame.size.width, _scrollPhotos.frame.size.height);
        imv = [[UIImageViewAligned alloc] initWithFrame:frame];
        [imv setContentMode:UIViewContentModeScaleAspectFill];
        imv.clipsToBounds = YES;
        [_scrollPhotos addSubview:imv];
        
        if([photoObject.url hasPrefix:@"http"]){
            [imv setImageWithURL:[NSURL URLWithString:photoObject.url] placeholderImage:nil];
        } else {
            //get image from resouce
            [imv setImage:[UIImage imageNamed:photoObject.url]];
        }
    }
    
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    int pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width);
    if(imagesArray!=nil && pageNumber < imagesArray.count){
        selectedPhoto = pageNumber;
    }
    [self displayArrowOnSelectedPosition];
}

-(void) displayArrowOnSelectedPosition{
    if(selectedPhoto == 0){
        _leftArrow.hidden = YES;
    }
    
    if(selectedPhoto == imagesArray.count - 1){
        _rightArrow.hidden = YES;
    }
    
    if(selectedPhoto > 0){
        _leftArrow.hidden = NO;
    }
    
    if(selectedPhoto < imagesArray.count - 1 && imagesArray.count > 1){
        _rightArrow.hidden = NO;
    }

}

@end
