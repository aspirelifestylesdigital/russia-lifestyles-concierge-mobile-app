//
//  GalleryObject.m
//  ALC
//
//  Created by Anh Tran on 8/23/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "GalleryObject.h"

@implementation GalleryObject
-(id) initFromDict:(NSDictionary*) dict{
    self = [super init];
    self.captionDescription = [dict stringForKey:@"CaptionDescription"];
    self.captionTitle = [dict stringForKey:@"CaptionTitle"];
    self.backgroundImgURL = [DOMAIN_URL stringByAppendingString:[dict stringForKey:@"BackgroundImageUrl"]];
    self.isVideo = [dict boolForKey:@"IsVideo"];
    self.videoLink = [dict stringForKey:@"VideoLink"];
    return self;
}
@end
