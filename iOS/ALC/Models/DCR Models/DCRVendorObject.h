//
//  DCRVendorObject.h
//  ALC
//
//  Created by Anh Tran on 3/10/17.
//  Copyright © 2017 Sunrise Software Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DCRVendorObject : NSObject
@property (nonatomic, strong) NSString* vendorId;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* promotionCode;
-(id) initFromDict:(NSDictionary*) dict;
@end
