//
//  ChatBotObject.h
//  demoChatBot
//
//  Created by Nhat Huy on 9/10/16.
//  Copyright © 2016 Nhat Huy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GourmetObject.h"
#import "ChatButtonObject.h"

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

@interface ChatBotObject : NSObject

@property (nonatomic,strong) NSString* valueContent;
@property (nonatomic,strong) NSMutableArray* listRestaurant;
@property (nonatomic,strong) NSMutableArray* listButtons;
@property (nonatomic,strong) NSMutableArray* listImageURL;
@property (nonatomic,assign) BOOL isBot;
@end
