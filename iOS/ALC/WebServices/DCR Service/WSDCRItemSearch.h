//
//  WSDCRItemSearch.h
//  ALC
//
//  Created by Anh Tran on 3/13/17.
//  Copyright © 2017 Sunrise Software Solutions. All rights reserved.
//

#import "WSDCRBase.h"

@interface WSDCRItemSearch : WSDCRBase
@property (nonatomic, strong) NSArray* items;

-(void) searchItemType:(enum DCR_ITEM_TYPE) itemType andSubType:(NSString*)subType;
-(void) nextPage;
-(BOOL) hasNextItem;
@end
