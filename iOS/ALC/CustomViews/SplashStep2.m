//
//  SplashStep2.m
//  ALC
//
//  Created by Hai NguyenV on 8/17/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "SplashStep2.h"

@implementation SplashStep2
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    self = [[[NSBundle mainBundle] loadNibNamed:@"SplashStep2" owner:self options:nil]
            objectAtIndex:0];
    self.frame = frame;
    resetScaleViewBaseOnScreen(self);
    [self setTextForViews];
    return self;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
