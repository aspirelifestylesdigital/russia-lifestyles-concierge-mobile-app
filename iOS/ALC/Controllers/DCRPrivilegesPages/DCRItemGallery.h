//
//  DCRItemGallery.h
//  ALC
//
//  Created by Anh Tran on 3/14/17.
//  Copyright © 2017 Sunrise Software Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DCRItemGallery : UIView<UIScrollViewDelegate>
@property (strong, nonatomic) IBOutlet UIScrollView *scrollPhotos;
@property (strong, nonatomic) IBOutlet UIView *view;
@property (strong, nonatomic) IBOutlet UIButton *leftArrow;
@property (strong, nonatomic) IBOutlet UIButton *rightArrow;
-(void) showData:(NSArray*) images;
@end
