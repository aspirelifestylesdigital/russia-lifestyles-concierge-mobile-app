package com.aspire.android.russia.apiservices;

import com.aspire.android.russia.apiservices.ResponseModel.RoomTypePreferencesResponse;
import com.aspire.android.russia.apiservices.apis.appimplement.ApiProviderClient;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Callback;


public class WSRoomTypePreferences
        extends ApiProviderClient<RoomTypePreferencesResponse> {

    public WSRoomTypePreferences(){

    }
    @Override
    public void run(Callback<RoomTypePreferencesResponse> cb) {
        this.callback = cb;
        super.checkAuthenticateAndRun();
    }

    @Override
    protected boolean isUseAuthenticateInApi() {
        return true;
    }

    @Override
    public void runApi() {
        serviceInterface.getRoomTypePreferences().enqueue(callback);
    }

    @Override
    protected Map<String, String> prepareHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("User-Agent", "Retrofit-Sample-App");
        return headers;
    }

}
