package com.aspire.android.russia.model;


import com.google.gson.annotations.Expose;

public class UserObject {
    @Expose
    private UserItem UserObject;


    public UserItem getUserItem() {
        return UserObject;
    }

    public void setUserItem(UserItem UserObject) {
        UserObject = UserObject;
    }


}