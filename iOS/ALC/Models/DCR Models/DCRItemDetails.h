//
//  DCRItemDetails.h
//  ALC
//
//  Created by Anh Tran on 3/14/17.
//  Copyright © 2017 Sunrise Software Solutions. All rights reserved.
//

#import "DCRItemObject.h"

@interface DCRItemDetails : DCRItemObject
@property (nonatomic, strong) NSString* itemDescription;
@property (nonatomic, strong) NSString* direction;
@property (nonatomic, strong) NSString* websiteUrl;
@property (nonatomic, strong) NSString* phoneNumber;
@property (nonatomic, strong) NSString* operatingHoursDescription;
@property (nonatomic, strong) NSArray* openingHours;
@property (nonatomic, strong) NSString* internalLabel;
@property (nonatomic, strong) NSArray* typeOfPlace;
@property (nonatomic, strong) NSArray* occasions;
@property (nonatomic, strong) NSString* termsAndConditions;
@property (nonatomic, strong) NSString* logoUrl;
@property (nonatomic, strong) NSArray* chains;

//For Spa
@property (nonatomic, strong) NSString* spaService;
@property (nonatomic, strong) NSString* spaTreatment;
@property (nonatomic, strong) NSString* spaFullfillmentInstruction;
@property (nonatomic, strong) NSString* spaHighlight;

- (NSString*) fullLocationAddress;
- (NSString*) mapLocationAddress;
- (NSString*) fullValueFromDCRCommonArray:(NSArray*)array;
- (NSString*) fullOpeningHoursDetails;
@end
