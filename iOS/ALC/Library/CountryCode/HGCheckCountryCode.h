//
//  HGCheckCountryCode.h
//  HungryGoWhere
//
//  Created by Hai NguyenV on 5/9/14.
//  Copyright (c) 2014 Linh Le. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HGCheckCountryCode : NSObject

@property (nonatomic, strong) NSDictionary *diallingCodesDictionary;

-(BOOL)checkCountryCode:(NSString*)code;
@end
