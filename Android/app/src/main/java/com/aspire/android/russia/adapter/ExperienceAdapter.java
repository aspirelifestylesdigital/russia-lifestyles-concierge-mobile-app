package com.aspire.android.russia.adapter;

import android.graphics.Typeface;
import android.location.Location;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aspire.android.russia.BuildConfig;
import com.aspire.android.russia.R;
import com.aspire.android.russia.fragment.ExperienceSearchFragment;
import com.aspire.android.russia.interfaces.OnRecyclerViewItemClickListener;
import com.aspire.android.russia.model.ExperienceObject;
import com.aspire.android.russia.model.Restaurant;
import com.aspire.android.russia.utils.CommonUtils;
import com.aspire.android.russia.utils.FontUtils;
import com.aspire.android.russia.views.LoadingViewHolder;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

//import com.bumptech.glide.Glide;

/*
 * Created by chau.nguyen on 10/5/2016.
 */
public class ExperienceAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int ITEM_VIEW_TYPE_BASIC = 0;
    private final int ITEM_VIEW_TYPE_LOADING = 1;

    private List<Parcelable> data;
    private final OnRecyclerViewItemClickListener listener;
    private final ExperienceSearchFragment.ApiRequest apiRequest;
    private Location locationUser;

    public ExperienceAdapter(List<Parcelable> listDTO, OnRecyclerViewItemClickListener listener, ExperienceSearchFragment.ApiRequest apiRequest) {
        this.data = listDTO;
        this.listener = listener;
        this.apiRequest = apiRequest;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == ITEM_VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_footer_loadmore, parent, false);
            return new LoadingViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_experience, parent, false);
            return new ExperienceViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == ITEM_VIEW_TYPE_BASIC) {
            Parcelable item = data.get(position);
            ((ExperienceViewHolder) holder).bind(position, item, listener);
        } else {
            //view loading
        }

    }

    @Override
    public int getItemCount() {
        return (data == null) ? 0 : data.size();
    }

    public void setUpdateData(List<? extends Parcelable> responseData) {
        this.data.addAll(responseData);
        notifyDataSetChanged();
    }

    public void setClearAll() {
        this.data.clear();
        notifyDataSetChanged();
    }

    public List<Parcelable> getData() {
        return data;
    }

    //========== Config load more ===============

    /**
     * check is correct have view loading (show Process)
     *
     * @return true: show | false: hide
     */
    private boolean isShowLoading() {
        return (data.size() > 1 && data.contains(null));
    }

    /**
     * Visible view loading process pause load more
     */
    public void addViewLoading() {
        if (!isShowLoading()) {
            data.add(null);
            notifyItemInserted(getItemCount() - 1);
        }
    }

    /**
     * Hide view loading
     */
    public void removeViewLoading() {
        if (isShowLoading()) {
            int itemLoad = data.indexOf(null);
            if(itemLoad != -1){
                data.remove(itemLoad);
                notifyItemRemoved(itemLoad);
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        return data.get(position) != null ? ITEM_VIEW_TYPE_BASIC : ITEM_VIEW_TYPE_LOADING;
    }

    public int getRealSize() {
        if (data == null || data.size() <= 1)
            return 0;
        int n = data.size();
        if (isShowLoading())
            n -= 1;//loading item
        if (n < 0)
            n = 0;
        return n;
    }
    //========== End Config load more ===============

    //========== Location User ===============


    public void setLocationUser(Location locationUser) {
        this.locationUser = locationUser;
        notifyDataSetChanged();
    }

    class ExperienceViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imgExperience)
        ImageView imgContent;
        @BindView(R.id.imgExperienceSpecial)
        ImageView imgSpecial;
        @BindView(R.id.tvExperienceTile)
        TextView tvTitle;
        @BindView(R.id.tvExperienceSub)
        TextView tvSub;
        @BindView(R.id.tvExperienceDistance)
        TextView tvDistance;
        int pos;

        ExperienceViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            CommonUtils.setFontForTextView(tvTitle,
                                                FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
            CommonUtils.setFontForTextView(tvSub,
                                                FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);
            CommonUtils.setFontForTextView(tvDistance,
                                                FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);
            imgSpecial.setVisibility(View.GONE);
        }

        protected void bind(int position, final Parcelable dto, final OnRecyclerViewItemClickListener event) {
            this.pos = position;
            if (dto instanceof Restaurant) {
                Restaurant restaurant = ((Restaurant) dto);
                setViewGourmet(restaurant);
            } else if (dto instanceof ExperienceObject) {
                ExperienceObject experience = ((ExperienceObject) dto);
                setViewExperienceObj(experience);
            }

            //parent view set event click
            itemView.setTag(dto);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    event.onItemClick(pos, itemView);
                }
            });
        }

        private void setViewGourmet(Restaurant rest) {
            tvSub.setVisibility(View.VISIBLE);
            tvDistance.setVisibility(View.VISIBLE);

            tvTitle.setText(rest.getRestaurantName());
            tvSub.setText(rest.getShowCuisines());

            float distance = 0;
            if(locationUser != null){
                Location locationRest = new Location("");
                locationRest.setLongitude(Double.valueOf(rest.getLong()));
                locationRest.setLatitude(Double.valueOf(rest.getLat()));
                //cover meter -> km
                distance = locationUser.distanceTo(locationRest)/1000;
            }
            tvDistance.setText(String.format(itemView.getContext().getString(R.string.experiences_item_distance), distance));

//            Glide.with(itemView.getContext())
//                    .load(rest.getBackgroundImageUrl())
//                    .crossFade()
//                    .into(imgContent);
        }

        private void setViewExperienceObj(ExperienceObject experience) {
            tvSub.setVisibility(View.GONE);
            tvDistance.setVisibility(View.GONE);

            tvTitle.setText(experience.getTitle());
            String urlBanner = BuildConfig.WS_ROOT_URL + experience.getBannerUrl();
            if (apiRequest == ExperienceSearchFragment.ApiRequest.ENTERTAINMENT) {
//                Glide.with(itemView.getContext())
//                        .load(urlBanner)
//                        .crossFade()
//                        .transform(new CropImageTransformUtil(itemView.getContext()))
//                        .into(imgContent);
            } else {
//                Glide.with(itemView.getContext())
//                        .load(urlBanner)
//                        .crossFade()
//                        .into(imgContent);
            }
        }
    }

}
