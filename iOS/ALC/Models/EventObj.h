//
//  EventObj.h
//  ALC
//
//  Created by Hai NguyenV on 10/25/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EventObj : NSObject

@property (strong, nonatomic) NSString* bookingID;
@property (strong, nonatomic) NSString* identify;

@end
