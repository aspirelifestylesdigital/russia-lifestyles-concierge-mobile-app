//
//  GolfDetailsObj.h
//  ALC
//
//  Created by Hai NguyenV on 10/12/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "BaseBookingDetailsObject.h"

@interface GolfDetailsObj : BaseBookingDetailsObject

@property (strong, nonatomic) NSString* GolfCourse;
@property (strong, nonatomic) NSDate* GolfDate;
@property ( nonatomic, strong) NSString* noOfBaler;
@property ( nonatomic, strong) NSString* hole;
@property ( nonatomic, strong) NSString* handicaf;

-(id) initFromDict:(NSDictionary*) dict;

@end
