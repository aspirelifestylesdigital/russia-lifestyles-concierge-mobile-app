package com.aspire.android.russia.dcr.api;

import com.aspire.android.russia.dcr.api.ResponseModel.DCRBaseResponse;

import java.util.List;

/**
 * Created by ThuNguyen on 11/3/2016.
 */

public interface DCRICallback {
    void onDCRResponse(DCRBaseResponse response);
    void onDCRResponseOnList(List<DCRBaseResponse> responseList);
    void onDCRFailure(String errorMessage,
                      String errorCode);
}
