package com.aspire.android.russia.utils;

import android.content.Context;
import android.graphics.Typeface;

import java.io.File;
import java.util.Hashtable;

public class Fontfaces {

    /** Font in App*/
    public enum FONTLIST {
        AVENIR_NEXT_LT_MEDIUM("AvenirNextLTPro-Medium.otf"),
        AvenirLTStd_Light("AvenirNextLTPro-Regular.otf"),
        AvenirLTStd_Heavy("AvenirNextLTPro-Bold.otf"),
        AvenirLTStd_Black("AvenirNextLTPro-Medium.otf"),
        Avenirnext_demibold("AvenirNextLTPro-Demi.otf")
        ;

        FONTLIST(String path) {
            this.mPath = path;
        }

        private String mPath;

        public String getPath() {
            return mPath;
        }

    }

    public static Typeface getFont(Context context, FONTLIST font) {

        if (font == null) {
            return null;
        }

        Fontfaces.FontItem item = new Fontfaces.FontItem();
        item.mFontPath = "fonts/" + font.mPath;
        item.mFontName = font.mPath;
        item.mSrc = Fontfaces.FONTSRC.INTERNAL_TYPE;

        return Fontfaces.get(context, item);

    }

    /**
     * Font manage structure
     *
     * */
    private static final Hashtable<String, Typeface> cache = new Hashtable<String, Typeface>();

    private static Typeface get(Context c, FontItem font) {

        synchronized (cache) {

            try {

                if (!cache.containsKey(font.mFontName)) {

                    Typeface typeFace = null;

                    if (font.mSrc == FONTSRC.INTERNAL_TYPE) {
                        typeFace = Typeface.createFromAsset(c.getAssets(), font.mFontPath);
                    } else if (font.mSrc == FONTSRC.EXTERNAL_TYPE) {
                        //
                        File f = new File(font.mFontPath);

                        if (f.exists()) {
                            typeFace = Typeface.createFromFile(f);
                        }
                    } else {

                        if (font.mFontName.equalsIgnoreCase("DEFAULT")) {
                            typeFace = Typeface.DEFAULT;
                        } else if (font.mFontName.equalsIgnoreCase("MONOSPACE")) {
                            typeFace = Typeface.MONOSPACE;
                        } else if (font.mFontName.equalsIgnoreCase("SANS_SERIF")) {
                            typeFace = Typeface.SANS_SERIF;
                        } else {
                            //SERIF
                            typeFace = Typeface.SERIF;
                        }

                    }

                    if (typeFace != null) {
                        //
                        cache.put(font.mFontName, typeFace);
                    }

                }

            } catch (Exception e) {
                System.out.println("Get Typeface '" + font.mFontPath + "' error " + e.getMessage());
                return null;
            }

            //
            return cache.get(font.mFontName);

        }

    }

    public static class FontItem {
        public String mFontName;
        public String mFontPath;
        public FONTSRC mSrc;
    }

    public enum FONTSRC {
        INTERNAL_TYPE,
        EXTERNAL_TYPE,
        SYSTEM_TYPE
    }

}
