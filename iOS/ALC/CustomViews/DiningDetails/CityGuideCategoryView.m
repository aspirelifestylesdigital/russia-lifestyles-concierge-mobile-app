//
//  CityGuideCategoryView.m
//  ALC
//
//  Created by Anh Tran on 9/19/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "CityGuideCategoryView.h"
@interface CityGuideCategoryView(){
    NSInteger categoryIndex;
    UITapGestureRecognizer* tapRecognizer;
}
@end

@implementation CityGuideCategoryView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    self.view = [[[NSBundle mainBundle] loadNibNamed:@"CityGuideCategoryView" owner:self options:nil] firstObject];
    [self.view setFrame:frame];
    [self addSubview:self.view];
    
    if (self) {
        // Initialization code
        [self setFrame:frame];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        NSString *className = NSStringFromClass([self class]);
        self.view = [[[NSBundle mainBundle] loadNibNamed:className owner:self options:nil] firstObject];
        [self addSubview:self.view];
        return self;
    }
    
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void) showView:(NSInteger) index withTitle:(NSString*) title andImage:(NSString*) imageUrl{
    categoryIndex = index;
    [_lbCategoryName setFont:[UIFont fontWithName:@"AvenirNext-Medium" size:FONT_SIZE_18*SCREEN_SCALE]];
    [_lbCategoryName setText:title];
    
    _imvCategory.image = [UIImage imageNamed:imageUrl];
    
    tapRecognizer = [[UITapGestureRecognizer alloc]
                     initWithTarget:self action:@selector(respondToTapGesture:)];
    tapRecognizer.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:tapRecognizer];
}

-(void)respondToTapGesture:(UITapGestureRecognizer *)tapGuesture
{
    if(_delegate != nil){
        [_delegate clickOnCategory:categoryIndex];
    }
}
@end
