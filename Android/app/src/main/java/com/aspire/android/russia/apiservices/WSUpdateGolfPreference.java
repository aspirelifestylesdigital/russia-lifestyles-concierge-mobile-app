package com.aspire.android.russia.apiservices;

import com.aspire.android.russia.apiservices.RequestModel.UpdategGolfRequest;
import com.aspire.android.russia.apiservices.ResponseModel.UpdateGolfResponse;
import com.aspire.android.russia.apiservices.apis.appimplement.ApiProviderClient;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Callback;


public class WSUpdateGolfPreference
        extends ApiProviderClient<UpdateGolfResponse> {

    UpdategGolfRequest request;

    public WSUpdateGolfPreference(){

    }

    public UpdategGolfRequest getRequest() {
        return request;
    }

    public void setRequest(UpdategGolfRequest request) {
        this.request = request;
    }

    @Override
    public void run(Callback<UpdateGolfResponse> cb) {
        this.callback = cb;
        super.checkAuthenticateAndRun();
    }

    @Override
    protected boolean isUseAuthenticateInApi() {
        return true;
    }

    @Override
    public void runApi() {
        if(request==null)
            return;

        serviceInterface.updateGolfPreferences(request).enqueue(callback);
    }

    @Override
    protected Map<String, String> prepareHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("User-Agent", "Retrofit-Sample-App");
        return headers;
    }

}
