//
//  GourmetRecommendRequestObj.m
//  ALC
//
//  Created by Hai NguyenV on 10/17/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "GourmetRecommendRequestObj.h"

@implementation GourmetRecommendRequestObj
-(id) initFromDetails:(GouetmetRecommendDetailsObj*)detailsObj{
    self = [super init];
    self.BookingId = detailsObj.bookingId;
    self.reservationName = detailsObj.guestName;
    self.EmailAddress = detailsObj.email;
    self.MobileNumber = detailsObj.mobileNumber;
    self.isContactPhone = detailsObj.isContactPhone;
    self.isContactBoth = detailsObj.isContactBoth;
    self.isContactEmail = detailsObj.isContactEmail;
    self.SpecialRequirements = detailsObj.specialRequirement;
    
    self.City = detailsObj.city;
    self.Country = detailsObj.country;
    
    self.reservationDate = formatDate(detailsObj.reservationDate, DATE_SEND_REQUEST_FORMAT);
    self.reservationTime = formatDate(detailsObj.reservationTime, TIME_SEND_REQUEST_FORMAT);
 
    self.adultPax = detailsObj.adulsPax;
    self.kidsPax = detailsObj.kidsPax;
    
    self.Cuisines = [NSMutableArray arrayWithArray:detailsObj.cuisine];
    self.Occasion = [NSMutableArray arrayWithArray:detailsObj.occasion];
    [self setPrivilegeData:detailsObj.requestDetails];
    return self;
}
@end
