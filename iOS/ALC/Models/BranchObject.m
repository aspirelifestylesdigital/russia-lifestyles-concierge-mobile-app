//
//  BranchObject.m
//  ALC
//
//  Created by Anh Tran on 8/23/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "BranchObject.h"

@implementation BranchObject
-(id) initFromDict:(NSDictionary*) dict{
    self = [super init];
    self.branchId = [dict stringForKey:@"ID"];
    self.name = [dict stringForKey:@"Name"];
    self.address1 = [dict stringForKey:@"Address1"];
    self.address2 = [dict stringForKey:@"Address2"];
    self.telephone = [dict stringForKey:@"Telephone"];
    self.zipCode = [dict stringForKey:@"Zipcode"];
    self.lattitude = [dict stringForKey:@"Latitude"];
    self.longitude = [dict stringForKey:@"Longitude"];
    return self;
}
@end
