//
//  DCRItemDetails.m
//  ALC
//
//  Created by Anh Tran on 3/14/17.
//  Copyright © 2017 Sunrise Software Solutions. All rights reserved.
//

#import "DCRItemDetails.h"
#import "DCROpeningHourObject.h"
@implementation DCRItemDetails
-(id) initFromDict:(NSDictionary*) dict{
    self = [super initFromDict:dict];
    self.itemDescription  = [dict stringForKey:@"description"];
    self.direction  = [dict stringForKey:@"direction"];
    self.websiteUrl = [dict stringForKey:@"website_url"];
    self.phoneNumber = [dict stringForKey:@"phone_number"];
    self.operatingHoursDescription = [dict stringForKey:@"operation_hours_description"];
    self.openingHours = [dict arrayForKey:@"opening_hours" withObjectClass:[DCROpeningHourObject class]];
    self.internalLabel = [dict stringForKey:@"internal_label"];
    self.typeOfPlace = [dict arrayForKey:@"types_of_place" withObjectClass:[DCRCommonObject class]];
    self.occasions = [dict arrayForKey:@"occasions" withObjectClass:[DCRCommonObject class]];
    self.termsAndConditions = [dict stringForKey:@"terms_and_conditions"];
    self.logoUrl = [dict stringForKey:@"logo_url"];
    self.chains = [dict arrayForKey:@"chain" withObjectClass:[DCRCommonObject class]];
    return self;
}

- (NSString*) fullLocationAddress{
    NSString* result = @"";
    result = [result stringByAppendingString:self.addressLine1];
    result = [self appendNextStringWithComma:self.addressLine2 toString:result];
    result = [self appendNextStringWithComma:self.addressMetropolitan toString:result];
    result = [self appendNextStringWithComma:self.addressNeighbourhood toString:result];
    
    if(self.addressCity.Name){
        result = [self appendNextStringWithComma:self.addressCity.Name toString:result];
    }
    
    if(self.addressState.Name){
        result = [self appendNextStringWithComma:self.addressState.Name toString:result];
    }
    
    if(self.addressCountry.Name){
        result = [self appendNextStringWithComma:self.addressCountry.Name toString:result];
    }
    
    result = [self appendNextStringWithComma:self.addressPostalCode toString:result];
    return result;
}

- (NSString*) mapLocationAddress{
    NSString* result = @"";
    result = [result stringByAppendingString:self.addressLine1];
    if(result.length > 0){
        result = [self appendNextStringWithComma:self.addressLine2 toString:result];
    }
    result = [self appendNextStringWithComma:self.addressMetropolitan toString:result];
    result = [self appendNextStringWithComma:self.addressNeighbourhood toString:result];
    
    if(self.addressCity.Name){
        result = [self appendNextStringWithComma:self.addressCity.Name toString:result];
    }
    
    if(self.addressState.Name){
        result = [self appendNextStringWithComma:self.addressState.Name toString:result];
    }
    
    if(self.addressCountry.Name){
        result = [self appendNextStringWithComma:self.addressCountry.Name toString:result];
    }
    
    result = [self appendNextStringWithComma:self.addressPostalCode toString:result];
    return result;
}

-(NSString*) fullValueFromDCRCommonArray:(NSArray*)array{
    NSString* result = @"";
    for (DCRCommonObject* c in array) {
        result = [self appendNextStringWithComma:c.Name toString:result];
    }
    return result;
}

-(NSString*) fullOpeningHoursDetails{
    NSString* result = @"";
    for (DCROpeningHourObject* o in _openingHours) {
        result = [result stringByAppendingString:[NSString stringWithFormat:@"%@%@", (result.length > 0 ? @"<br>" : @""),[o fullValue]]];
    }
    return result;

}

- (NSString*) appendNextStringWithComma:(NSString*) next toString:(NSString*)source{
    if(next && next.length > 0){
        return [source stringByAppendingString:[NSString stringWithFormat:@"%@%@", (source.length > 0 ? @", " : @""),next]];
    }
    return source;
}
@end
