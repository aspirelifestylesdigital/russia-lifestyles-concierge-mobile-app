//
//  MyPreferenceController.h
//  ALC
//
//  Created by Hai NguyenV on 9/8/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyPreferenceController : MainViewController

- (IBAction)actionHotel:(id)sender;
- (IBAction)actinDinning:(id)sender;
- (IBAction)actionCarRetal:(id)sender;
- (IBAction)actionOther:(id)sender;
@end
