package com.aspire.android.russia.activitys;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.aspire.android.russia.R;
import com.aspire.android.russia.adapter.SplashPageAdapter;
import com.aspire.android.russia.application.AppConstant;
import com.aspire.android.russia.application.coreactivitys.BaseActivityNonNavigationview;
import com.aspire.android.russia.model.UserItem;
import com.aspire.android.russia.utils.CommonUtils;
import com.aspire.android.russia.utils.FontUtils;
import com.aspire.android.russia.utils.SharedPreferencesUtils;
import com.aspire.android.russia.widgets.CirclePageIndicator;
import com.aspire.android.russia.widgets.ViewPagerSpalshView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by and.trinh on 9/14/2016.
 */
public class SplashAcitivy
        extends BaseActivityNonNavigationview {
    @BindView(R.id.view_pager)
    ViewPagerSpalshView viewPager;
    @BindView(R.id.view_pager_indicator)
    CirclePageIndicator viewPagerIndicator;
    @BindView(R.id.tv_get_started)
    Button mTvGetStarted;
    @BindView(R.id.img_logo)
    ImageView mImgLogo;
    @BindView(R.id.tv_title)
    TextView mTvTitle;
    @BindView(R.id.tv_description)
    TextView mTvDescription;
    private SplashPageAdapter pageAdapter;

    @Override
    protected int activityLayoutId() {
        return R.layout.activity_splash;
    }

    @Override
    protected void initView() {
        ButterKnife.bind(this);
        pageAdapter = new SplashPageAdapter(this,
                                            getSupportFragmentManager());
        viewPager.setAdapter(pageAdapter);
        viewPager.setCurrentItem(0);
        viewPagerIndicator.setViewPager(viewPager);
        viewPagerIndicator.setActualCount(SplashPageAdapter.actualPage);
        CommonUtils.setFontForViewRecursive(mTvGetStarted,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LT_MEDIUM);
        CommonUtils.setFontForViewRecursive(mTvTitle,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LTPRO_ULTLT);
        CommonUtils.setFontForViewRecursive(mTvDescription,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_LTPRO_REGULAR);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(final int position,
                                       final float positionOffset,
                                       final int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                position = position % SplashPageAdapter.actualPage;
                if(position ==0){
                    setInfo(getResources().getString(R.string.text_title_splash_one),getResources().getString(R.string.text_description_splash_one));

                }
               /* if(position==1){
                    setInfo(getResources().getString(R.string.text_title_splash_two),getResources().getString(R.string.text_description_splash_two));
                }*/
                if(position==1){
                    setInfo(getResources().getString(R.string.text_title_splash_three),getResources().getString(R.string.text_description_splash_three));
                }
                if(position == 2){
                    setInfo(getResources().getString(R.string.text_title_splash_four),getResources().getString(R.string.text_description_splash_four));
                }
            }

            @Override
            public void onPageScrollStateChanged(final int state) {

            }
        });


    }

    @OnClick(R.id.tv_get_started)
    public void onClick() {
        if(UserItem.isLogined()){
            Intent intent = new Intent(this, HomeActivity.class);
            startActivity(intent);
        }else {
            Intent intent = new Intent(this,
                                       LoginAcitivy.class);
            startActivity(intent);
        }
        finish();

    }
    public void setInfo(final String title, final String description){
        mTvTitle.setText(title);
        mTvDescription.setText(description);
    }

    @Override
    protected void onResume() {
        SharedPreferencesUtils.setPreferences(AppConstant.USER_FIRST_TIME_PRE, true);
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}
