package com.aspire.android.russia.adapter;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aspire.android.russia.R;
import com.aspire.android.russia.application.AppConstant;
import com.aspire.android.russia.model.BotSay;
import com.aspire.android.russia.model.ChatButton;
import com.aspire.android.russia.model.UserItem;
import com.aspire.android.russia.utils.AndroidDevice;
import com.aspire.android.russia.utils.Fontfaces;
import com.aspire.android.russia.utils.SharedPreferencesUtils;
import com.aspire.android.russia.utils.StringUtil;
//import com.bumptech.glide.Glide;
import com.joooonho.SelectableRoundedImageView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by nga.nguyent on 2/26/2016.
 */
public class ChatbotAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    enum CHAT_DATA_TYPE{
        USER(0),
        CHATBOT_TEXT(1),
        CHATBOT_IMGS(2);

        CHAT_DATA_TYPE(int val){
            value = val;
        }

        public int getValue() {
            return value;
        }

        int value;
    }

    public interface OnChatbotButtonClickListener {
        void onButtonClick(View v);
    }

    public interface OnRestaurantInfoClickListener{
        void onFavoriteClick(int chatItemIndex, int restaurantIndex);
        void onShareClick(int chatItemIndex, int restaurantIndex);
        void onReadMoreClick(int chatItemIndex, int restaurantIndex);
        void onBookNowClick(int chatItemIndex, int restaurantIndex);
    }

    private List<BotSay> data = new ArrayList<>();
    private Context context;
    private OnChatbotButtonClickListener chatbotClick;
    private OnRestaurantInfoClickListener restaurantInfoClickListener;
    private boolean sayHiInPreviousChat = false;

    public ChatbotAdapter(Context context) {
        this.context = context;
    }

    public void addData(BotSay bot){
        if(bot.isHelloMessage()){
            if(sayHiInPreviousChat){
                bot.setText("Hi");
            }
            else {
                bot.removeEmptyParameter();
                sayHiInPreviousChat = true;
            }
        }
        data.add(bot);
    }

    public BotSay getLastItem(){
        if(getItemCount()>0)
            return data.get(data.size()-1);
        return null;
    }

    public BotSay getItemAtIndex(int pos){
        if(getItemCount()>0)
            return data.get(pos);
        return null;
    }

    public boolean lastItemIsConsultantMessage(){
        if(data.size()==0)
            return false;

        BotSay bot = getLastItem();
        if(bot!=null && bot.isFaceWithConsultantMessage())
            return true;
        return false;
    }

    public void setChatbotClick(OnChatbotButtonClickListener chatbotClick) {
        this.chatbotClick = chatbotClick;
    }

    public void setRestaurantInfoClickListener(OnRestaurantInfoClickListener restaurantInfoClickListener) {
        this.restaurantInfoClickListener = restaurantInfoClickListener;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        BotSay bs = data.get(position);
        if(bs.isUserData())
            return CHAT_DATA_TYPE.USER.value;
        else if(bs.isImagesData()){
            return CHAT_DATA_TYPE.CHATBOT_IMGS.value;
        }
        return CHAT_DATA_TYPE.CHATBOT_TEXT.value;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == CHAT_DATA_TYPE.USER.value){
            View view = View.inflate(parent.getContext(), R.layout.layout_chatbot_user, null);
            UserViewHolder holder = new UserViewHolder(view);
            return holder;
        }
        else if(viewType == CHAT_DATA_TYPE.CHATBOT_IMGS.value){
            View view = View.inflate(parent.getContext(), R.layout.layout_chatbot_images, null);
            ChatbotImageViewHolder holder = new ChatbotImageViewHolder(view);
            return holder;
        }
        else if(viewType == CHAT_DATA_TYPE.CHATBOT_TEXT.value){
            View view = View.inflate(parent.getContext(), R.layout.layout_chatbot_system, null);
            ChatbotTextViewHolder holder = new ChatbotTextViewHolder(view);
            return holder;
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof UserViewHolder){
            ((UserViewHolder)holder).setData(position, data.get(position));
        }
        else if(holder instanceof ChatbotImageViewHolder){
            ((ChatbotImageViewHolder)holder).setData(position, data.get(position));
        }
        else if(holder instanceof ChatbotTextViewHolder){
            ((ChatbotTextViewHolder)holder).setData(position, data.get(position));
        }
    }

    public class UserViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.tvUserName)
        TextView tvUserName;
        @BindView(R.id.tvMessage)
        TextView tvMessage;
        @BindView(R.id.imgAvatar)
        SelectableRoundedImageView imgAvatar;

        int position;

        public UserViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            tvUserName.setTypeface(Fontfaces.getFont(context, Fontfaces.FONTLIST.AVENIR_NEXT_LT_MEDIUM));
            tvMessage.setTypeface(Fontfaces.getFont(context, Fontfaces.FONTLIST.AVENIR_NEXT_LT_MEDIUM));

            tvUserName.setText(UserItem.getLoginedFullName());
            tvMessage.setText("");

            String avatarUrl = SharedPreferencesUtils.getPreferences(AppConstant.USER_AVATAR, "");
            if(!StringUtil.isEmpty(avatarUrl)){
//                Glide.with(itemView.getContext())
//                        .load(BuildConfig.WS_ROOT_URL + avatarUrl)
//                        .asBitmap()
//                        //.crossFade()
//                        .transform(new CropImageTransformUtil(itemView.getContext()))
//                        .into(imgAvatar);
            }
        }

        public void setData(int pos, BotSay bot){
            position = pos;
            tvMessage.setText(bot.getText());
        }
    }

    public class ChatbotTextViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.tvBotName)
        TextView tvBotName;
        @BindView(R.id.tvMessage)
        TextView tvMessage;
        @BindView(R.id.llControllerButtons)
        LinearLayout llControllerButtons;
        @BindView(R.id.llRecommendButtons)
        LinearLayout llRecommendButtons;
        @BindView(R.id.btnYes)
        TextView btnYes;
        @BindView(R.id.btnNo)
        TextView btnNo;
        @BindView(R.id.btnCallUs)
        TextView btnCallUs;
        @BindView(R.id.btnLiveChat)
        TextView btnLiveChat;
        @BindView(R.id.btnAvailableTables)
        TextView btnAvailableTables;
        @BindView(R.id.btnAuthenticVietnameseFood)
        TextView btnAuthenticVietnameseFood;
        @BindView(R.id.btnConciergeRecommend)
        TextView btnConciergeRecommend;
        @BindView(R.id.btnMichelinStars)
        TextView btnMichelinStars;
        @BindView(R.id.btnNearbyRestaurant)
        TextView btnNearbyRestaurant;
        @BindView(R.id.btnNewRestaurant)
        TextView btnNewRestaurant;

        int position;

        public ChatbotTextViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            tvBotName.setTypeface(Fontfaces.getFont(context, Fontfaces.FONTLIST.AVENIR_NEXT_LT_MEDIUM));
            tvMessage.setTypeface(Fontfaces.getFont(context, Fontfaces.FONTLIST.AVENIR_NEXT_LT_MEDIUM));
            btnYes.setTypeface(Fontfaces.getFont(context, Fontfaces.FONTLIST.AVENIR_NEXT_LT_MEDIUM));
            btnNo.setTypeface(Fontfaces.getFont(context, Fontfaces.FONTLIST.AVENIR_NEXT_LT_MEDIUM));
            btnCallUs.setTypeface(Fontfaces.getFont(context, Fontfaces.FONTLIST.AVENIR_NEXT_LT_MEDIUM));
            btnLiveChat.setTypeface(Fontfaces.getFont(context, Fontfaces.FONTLIST.AVENIR_NEXT_LT_MEDIUM));
            btnAvailableTables.setTypeface(Fontfaces.getFont(context, Fontfaces.FONTLIST.AVENIR_NEXT_LT_MEDIUM));
            btnAuthenticVietnameseFood.setTypeface(Fontfaces.getFont(context, Fontfaces.FONTLIST.AVENIR_NEXT_LT_MEDIUM));
            btnConciergeRecommend.setTypeface(Fontfaces.getFont(context, Fontfaces.FONTLIST.AVENIR_NEXT_LT_MEDIUM));
            btnMichelinStars.setTypeface(Fontfaces.getFont(context, Fontfaces.FONTLIST.AVENIR_NEXT_LT_MEDIUM));
            btnNearbyRestaurant.setTypeface(Fontfaces.getFont(context, Fontfaces.FONTLIST.AVENIR_NEXT_LT_MEDIUM));
            btnNewRestaurant.setTypeface(Fontfaces.getFont(context, Fontfaces.FONTLIST.AVENIR_NEXT_LT_MEDIUM));

            llControllerButtons.setVisibility(View.GONE);
            llRecommendButtons.setVisibility(View.GONE);
            tvMessage.setText("");

            btnAvailableTables.setTag(itemView.getContext().getString(R.string.text_chat_bot_availabletables));
            btnAuthenticVietnameseFood.setTag(itemView.getContext().getString(R.string.text_chat_bot_authenticfood));
            btnConciergeRecommend.setTag(itemView.getContext().getString(R.string.text_chat_bot_conciergerecommendation));
            btnMichelinStars.setTag(itemView.getContext().getString(R.string.text_chat_bot_michelinstars));
            btnNearbyRestaurant.setTag(itemView.getContext().getString(R.string.text_chat_bot_nearbyrestaurants));
            btnNewRestaurant.setTag(itemView.getContext().getString(R.string.text_chat_bot_newrestaurants));
        }

        @OnClick({R.id.btnYes, R.id.btnNo, R.id.btnCallUs, R.id.btnLiveChat,
                R.id.btnAvailableTables, R.id.btnAuthenticVietnameseFood, R.id.btnConciergeRecommend, R.id.btnMichelinStars,
        R.id.btnNearbyRestaurant, R.id.btnNewRestaurant})
        void onClick(View v){
            int idx = (int) v.getTag(R.string.key_button_idx);
            BotSay bot = data.get(position);
            bot.getButtons().get(idx).setSelected(true);
            v.setSelected(true);
            v.setPressed(true);

            if(chatbotClick!=null){
                chatbotClick.onButtonClick(v);
            }
        }

        public void setData(int pos, BotSay bot){
            position = pos;
            tvMessage.setText(bot.getText());
            //
            if(bot.isNoButtons()){
                llControllerButtons.setVisibility(View.GONE);
                llRecommendButtons.setVisibility(View.GONE);
            }
            else{
                if(bot.isRecommend()){
                    setUpRecommendButtons(bot.getButtons());
                    llControllerButtons.setVisibility(View.GONE);
                    llRecommendButtons.setVisibility(View.VISIBLE);
                }
                else{
                    setUpButtons(bot.getButtons());
                    llControllerButtons.setVisibility(View.VISIBLE);
                    llRecommendButtons.setVisibility(View.GONE);
                }
            }
        }

        private void setUpButtons(List<ChatButton>buttons){
            TextView[]btns = new TextView[]{btnYes, btnNo, btnCallUs, btnLiveChat};
            boolean clickable = position == getItemCount()-1;
            for (TextView btn : btns) {
                btn.setVisibility(View.GONE);
                btn.setClickable(clickable);
            }

            int idx = 0;
            for (ChatButton btn:buttons) {
                if(btn.isYes()){
                    btnYes.setVisibility(View.VISIBLE);
                    btnYes.setTag(R.string.key_button_idx, idx);
                    btnYes.setSelected(btn.isSelected());
                }
                else if(btn.isNo()){
                    btnNo.setVisibility(View.VISIBLE);
                    btnNo.setTag(R.string.key_button_idx, idx);
                    btnNo.setSelected(btn.isSelected());
                }
                else if(btn.isCallUs()){
                    btnCallUs.setVisibility(View.VISIBLE);
                    btnCallUs.setTag(R.string.key_button_idx, idx);
                    btnCallUs.setSelected(btn.isSelected());
                }
                else if(btn.isLiveChat()){
                    btnLiveChat.setVisibility(View.VISIBLE);
                    btnLiveChat.setTag(R.string.key_button_idx, idx);
                    btnLiveChat.setSelected(btn.isSelected());
                }
                else{
                    System.out.println(position + " btn no");
                }
                idx++;
            }
        }

        private void setUpRecommendButtons(List<ChatButton>buttons){
            TextView[]btns = new TextView[]{btnAvailableTables, btnAuthenticVietnameseFood, btnConciergeRecommend, btnMichelinStars, btnNearbyRestaurant, btnNewRestaurant};
            boolean clickable = position == getItemCount()-1;
            for (TextView btn : btns) {
                btn.setVisibility(View.GONE);
                btn.setClickable(clickable);
            }

            int idx = 0;
            for (ChatButton btn:buttons) {
                if(btn.isAvailableTables()){
                    btnAvailableTables.setVisibility(View.VISIBLE);
                    btnAvailableTables.setTag(R.string.key_button_idx, idx);
                    btnAvailableTables.setSelected(btn.isSelected());
                }
                else if(btn.isAuthenticVietnameseFood()){
                    btnAuthenticVietnameseFood.setVisibility(View.VISIBLE);
                    btnAuthenticVietnameseFood.setTag(R.string.key_button_idx, idx);
                    btnAuthenticVietnameseFood.setSelected(btn.isSelected());
                }
                else if(btn.isConciergeRecommendation()){
                    btnConciergeRecommend.setVisibility(View.VISIBLE);
                    btnConciergeRecommend.setTag(R.string.key_button_idx, idx);
                    btnConciergeRecommend.setSelected(btn.isSelected());
                }
                else if(btn.isMichelinStars()){
                    btnMichelinStars.setVisibility(View.VISIBLE);
                    btnMichelinStars.setTag(R.string.key_button_idx, idx);
                    btnMichelinStars.setSelected(btn.isSelected());
                }
                else if(btn.isNearbyRestaurants()){
                    btnNearbyRestaurant.setVisibility(View.VISIBLE);
                    btnNearbyRestaurant.setTag(R.string.key_button_idx, idx);
                    btnNearbyRestaurant.setSelected(btn.isSelected());
                }
                else if(btn.isNewRestaurants()){
                    btnNewRestaurant.setVisibility(View.VISIBLE);
                    btnNewRestaurant.setTag(R.string.key_button_idx, idx);
                    btnNewRestaurant.setSelected(btn.isSelected());
                }
                else{
                    System.out.println(position + " btn no");
                }
                idx++;
            }
        }
    }

    public class ChatbotImageViewHolder extends RecyclerView.ViewHolder{
        /*@BindView(R.id.rclView)
        RecyclerView rclView;*/
        @BindView(R.id.vPager)
        ViewPager viewPager;
        @BindView(R.id.imgArrowLeft)
        ImageView imgArrowLeft;
        @BindView(R.id.imgArrowRight)
        ImageView imgArrowRight;

        //ChatbotImageAdapter adpt;
        ChatbotRestsAdapter adpt2;
        int position;

        public ChatbotImageViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            /*rclView.setVisibility(View.GONE);
            viewPager.setVisibility(View.VISIBLE);

            LinearLayoutManager layoutManager = new LinearLayoutManager(context);
            layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
            rclView.setLayoutManager(layoutManager);

            adpt = new ChatbotImageAdapter(context);
            adpt.setRestaurantInfoClickListener(restaurantInfoClickListener);
            rclView.setAdapter(adpt);*/

            int[]size = AndroidDevice.getScreenSizeDemension(context);
            int itemWidth = context.getResources().getDimensionPixelSize(R.dimen.chatbot_image_item_size);
            int paddingRight = context.getResources().getDimensionPixelOffset(R.dimen.margin_20dp);

            int padding = (size[0] - itemWidth - paddingRight)/2;

            adpt2 = new ChatbotRestsAdapter(context);
            adpt2.setRestaurantInfoClickListener(restaurantInfoClickListener);
            viewPager.setAdapter(adpt2);
            viewPager.setClipChildren(false);
            viewPager.setClipToPadding(false);
            viewPager.setPadding(padding, 0, padding, 0);
            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    //System.out.println("position " + position + "; positionOffset " + positionOffset + "; positionOffsetPixels " + positionOffsetPixels);
                }

                @Override
                public void onPageSelected(int position) {
                    adpt2.setCurrentPageItemSelectedIndex(position);
                    //adpt2.notifyDataSetChanged();
                    int n = adpt2.getCount();
                    if(position>0)
                        setColorItemView(position-1, position);
                    setColorItemView(position, position);
                    if(position<n-1)
                        setColorItemView(position+1, position);

                    if(position>0)
                        imgArrowLeft.setVisibility(View.VISIBLE);
                    else {
                        imgArrowLeft.setVisibility(View.GONE);
                    }
                    if(position<n-1)
                        imgArrowRight.setVisibility(View.VISIBLE);
                    else
                        imgArrowRight.setVisibility(View.GONE);
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
        }

        private void setColorItemView(int id, int currentItem){
            ChatbotRestsAdapter.PageItem item = (ChatbotRestsAdapter.PageItem) viewPager.findViewById(id);
            if(item!=null)
                item.setColorCurrentItem(currentItem);
        }

        public void setData(int pos, BotSay bot){
            this.position = pos;
            /*adpt.setData(position, position == data.size()-1, bot.getData().getListData());
            adpt.notifyDataSetChanged();*/

            adpt2.setData(position, position == data.size()-1, bot.getData().getListData());
            adpt2.notifyDataSetChanged();
            imgArrowLeft.setVisibility(View.GONE);
            if(adpt2.getCount()<=1)
                imgArrowRight.setVisibility(View.GONE);
        }

        @OnClick({R.id.imgArrowLeft, R.id.imgArrowRight})
        void onArrowClick(View v){
            if(v.getId() == R.id.imgArrowLeft){
                if(viewPager.getCurrentItem()>0)
                    viewPager.setCurrentItem(viewPager.getCurrentItem()-1);
            }
            else{
                if(viewPager.getCurrentItem()<adpt2.getCount())
                    viewPager.setCurrentItem(viewPager.getCurrentItem()+1);
            }
        }

    }

}