//
//  GourmetRequestObject.h
//  ALC
//
//  Created by Hai NguyenV on 10/7/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "BaseBookingDetailsObject.h"
#import "PhotoResizedInfo.h"
#import "GourmetRequestDetailsObj.h"
#import "BaseBookingRequestObject.h"
@interface GourmetRequestObject : BaseBookingRequestObject
@property (strong, nonatomic) NSString* restaurantName;
@property (strong, nonatomic) NSString* reservationDate;
@property (strong, nonatomic) NSString* reservationTime;
@property (strong, nonatomic) NSString* reservationName;
@property (strong, nonatomic) NSString* BookingId;
@property (strong, nonatomic) NSString* MobileNumber;
@property (strong, nonatomic) NSString* EmailAddress;
@property (strong, nonatomic) NSString* BookingItemId;
@property (strong, nonatomic) NSString* Country;
@property (strong, nonatomic) NSString* City;
@property (strong, nonatomic) NSString* State;
@property (strong, nonatomic) NSString* foodAllergies;
@property (strong, nonatomic) NSMutableArray* Occasion;
@property (strong, nonatomic) NSString* SpecialRequirements;

@property ( nonatomic) BOOL isContactPhone;
@property ( nonatomic) BOOL isContactEmail;
@property ( nonatomic) BOOL isContactBoth;

@property ( nonatomic) BOOL isEdit;
@property (strong, nonatomic) NSString* EditType;

@property ( nonatomic) NSInteger adultPax;
@property ( nonatomic) NSInteger kidsPax;

@property (strong, nonatomic) PhotoResizedInfo* photo;

-(id) initFromDetails:(GourmetRequestDetailsObj*)detailsObj;
@end
