package com.aspire.android.russia.model.concierge;

import android.text.TextUtils;

import com.aspire.android.russia.R;
import com.aspire.android.russia.apiservices.b2c.ResponseModel.B2CGetRecentRequestResponse;
import com.aspire.android.russia.application.AppConstant;
import com.aspire.android.russia.application.AppContext;
import com.aspire.android.russia.utils.DateTimeUtil;

/**
 * Created by ThuNguyen on 10/26/2016.
 */

public class EventBookingDetailData extends MyRequestObject {
    private String EventName;
    private String EventCategory;
    private String EventDateTime;
    private String PreferredDate;
    private int NoOfTickets;
    private String CreateDate;
    private long createDateEpoc;

    private long eventDateEpoc;
    private long preferredDateEpoc;
    public EventBookingDetailData(B2CGetRecentRequestResponse recentRequestResponse) {
        super(recentRequestResponse);
        EventDateTime = recentRequestResponse.getEVENTDATE();
        EventName = recentRequestResponse.getEVENTNAME();
        try {
            NoOfTickets = Integer.parseInt(recentRequestResponse.getNUMBEROFADULTS());
        }catch(Exception e){
            NoOfTickets = 0;
        }
        CreateDate = recentRequestResponse.getCREATEDDATE();
        createDateEpoc = DateTimeUtil.shareInstance().convertToTimeStampFromGMT(CreateDate, AppConstant.DATE_FORMAT_YYYY_MM_DD_T_HH_MM_SS);
        // Booking history
        if(!TextUtils.isEmpty(EventName)) {
            eventDateEpoc = DateTimeUtil.shareInstance().convertToTimeStamp(EventDateTime, AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS);
            ItemTitle = EventName;
            ItemDescription = AppContext.getSharedInstance().getResources().getString(R.string.text_event_date) + DateTimeUtil.shareInstance().convertTime(EventDateTime, AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS,
                    AppContext.getSharedInstance().getResources().getString(R.string.text_my_request_reservation_date_format));
          //  ItemDescription = "Event date: " + DateTimeUtil.shareInstance().convertTime(EventDateTime, AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS, AppConstant.DATE_FORMAT_CONCIERGE_BOOKING).toUpperCase();
        }else{
            PreferredDate = recentRequestResponse.getDELIVERY();
            preferredDateEpoc = DateTimeUtil.shareInstance().convertToTimeStamp(PreferredDate, AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS);
            EventCategory = conciergeRequestDetail.getEventCategory();
//            ItemTitle = "- Get entertainment recommendation -";
            ItemTitle = AppContext.getSharedInstance().getResources().getString(R.string.text_get_entertainment_recommendation);
            ItemDescription = AppContext.getSharedInstance().getResources().getString(R.string.text_event_date_recommend) + DateTimeUtil.shareInstance().convertTime(PreferredDate, AppConstant.DATE_FORMAT_YYYY_MM_DD_HH_MM_SS, AppConstant.DATE_FORMAT_CONCIERGE_BOOKING);
        }
    }
    @Override
    protected void initRequestProperties() {
        // Booking type group
        BookingFilterGroupType = AppConstant.BOOKING_FILTER_TYPE.Entertainment;

    }

    @Override
    public boolean isBookNormalType() {
        return !TextUtils.isEmpty(EventName);
    }

    @Override
    public long getRequestStartDateEpoch() {
        if(!TextUtils.isEmpty(EventName)){
            return eventDateEpoc;
        }
        return preferredDateEpoc;
    }

    @Override
    public long getRequestEndDateEpoch() {
        return 0;
    }

    @Override
    public String getReservationName() {
        return conciergeRequestDetail.getReservationName();
    }

    public long getPreferredDateEpoc() {
        return preferredDateEpoc;
    }

    public void setPreferredDateEpoc(long preferredDateEpoc) {
        this.preferredDateEpoc = preferredDateEpoc;
    }

    public long getEventDateEpoc() {
        return eventDateEpoc;
    }

    public void setEventDateEpoc(long eventDateEpoc) {
        this.eventDateEpoc = eventDateEpoc;
    }

    public String getEventName() {
        return EventName;
    }

    public void setEventName(String eventName) {
        EventName = eventName;
    }

    public int getNoOfTickets() {
        return NoOfTickets;
    }

    public void setNoOfTickets(int noOfTickets) {
        NoOfTickets = noOfTickets;
    }

    public String getPreferredDate() {
        return PreferredDate;
    }

    public void setPreferredDate(String preferredDate) {
        PreferredDate = preferredDate;
    }

    public String getEventDateTime() {
        return EventDateTime;
    }

    public void setEventDateTime(String eventDateTime) {
        EventDateTime = eventDateTime;
    }

    public String getEventCategory() {
        return EventCategory;
    }

    public void setEventCategory(String eventCategory) {
        EventCategory = eventCategory;
    }
    public long getCreateDateEpoc() {
        return createDateEpoc;
    }
}
