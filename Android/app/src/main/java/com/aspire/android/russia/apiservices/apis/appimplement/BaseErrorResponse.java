package com.aspire.android.russia.apiservices.apis.appimplement;

public class BaseErrorResponse {
    private String message;
    private int status;
    private Throwable throwable;

    public BaseErrorResponse(){

    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Throwable getThrowable() {
        return throwable;
    }

    public void setThrowable(Throwable throwable) {
        this.throwable = throwable;
    }
}
