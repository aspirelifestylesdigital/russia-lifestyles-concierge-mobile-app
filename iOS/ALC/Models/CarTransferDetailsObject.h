//
//  CarTransferDetailsObject.h
//  ALC
//
//  Created by Anh Tran on 10/10/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "BaseBookingDetailsObject.h"

@interface CarTransferDetailsObject : BaseBookingDetailsObject
@property (strong, nonatomic) NSDate *pickUpDate;
@property (strong, nonatomic) NSDate* pickUpTime;
@property (strong, nonatomic) NSString* pickUpLocation;
@property (strong, nonatomic) NSString* dropOffLocation;
@property (nonatomic) NSInteger nbPassengers;
@property (strong, nonatomic) NSString* transportType;

-(id) initFromDict:(NSDictionary*) dict;
-(NSString*) transportKey;
-(NSString*) transportValue;
@end
