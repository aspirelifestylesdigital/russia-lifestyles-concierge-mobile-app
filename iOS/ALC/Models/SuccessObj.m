//
//  SuccessObj.m
//  ALC
//
//  Created by Hai NguyenV on 10/25/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "SuccessObj.h"

@implementation SuccessObj

-(id) initFromDict:(NSDictionary*) dict{
    //self = [super initFromDict:dict];
    
    _bookingID = [dict stringForKey:@"BookingId"];
    _bookingName = [dict stringForKey:@"BookingName"];
    return self;
}
@end
