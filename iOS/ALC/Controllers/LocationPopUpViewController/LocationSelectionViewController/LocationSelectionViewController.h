//
//  LocationSelectionViewController.h
//  ALC
//
//  Created by Tho Nguyen on 10/20/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, LocationSelectionType) {
    LocationSelectionType_Country,
    LocationSelectionType_City,
};

@protocol LocationSelectionViewControllerDelegate;

@interface LocationSelectionViewController : UIViewController

@property (nonatomic, weak) id<LocationSelectionViewControllerDelegate> delegate;

@property (nonatomic, strong) NSArray *datas;
@property (nonatomic) NSInteger indexSelection;
@property (nonatomic) LocationSelectionType type;

@end

@protocol LocationSelectionViewControllerDelegate <NSObject>

- (void)didSelectItem:(NSInteger)indexSelection andLocationSelectionType:(LocationSelectionType)type;

@end