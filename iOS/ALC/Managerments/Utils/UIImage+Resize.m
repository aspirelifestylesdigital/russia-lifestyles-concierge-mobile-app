//
//  UIImage+Resize.m
//  HungryGoWhere
//
//  Created by Linh Le on 11/25/12.
//  Copyright (c) 2012 Linh Le. All rights reserved.
//

#import "UIImage+Resize.h"
#import "PhotoResizedInfo.h"

@implementation UIImage (UIImage_Resize)

+(PhotoResizedInfo *)imageWithImage:(UIImage*)image scaledToMaxSize:(CGSize)maxSize maxBytesLength:(NSInteger)maxBytesLength extension:(NSString*)extension
{
    NSString *newExtenstion = extension;
    
    // calculate new size    
    CGSize oldImageSize = image.size;
    CGSize newImageSize = CGSizeZero;
    
    float widthRate = oldImageSize.width / maxSize.width;
    float heightRate = oldImageSize.height / maxSize.height;
    
    if(widthRate <= 1 && heightRate <= 1)
    {
        newImageSize = oldImageSize;
    }
    else if((widthRate >= 1 && heightRate <= 1) || ((widthRate - 1) > (heightRate - 1)))
    {
        newImageSize.width =  maxSize.width;
        newImageSize.height = maxSize.width * oldImageSize.height / oldImageSize.width;
    }
    else if((widthRate <= 1 && heightRate >= 1) || ((widthRate - 1) <= (heightRate - 1)))
    {
        newImageSize.height =  maxSize.height;
        newImageSize.width = maxSize.height * oldImageSize.width / oldImageSize.height;
    }
    
    // draw image with new size
    UIGraphicsBeginImageContextWithOptions(newImageSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newImageSize.width, newImageSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    CGSize imageNewSize = newImage.size;
    NSLog(@"%f, %f", imageNewSize.width, imageNewSize.height);
    
    // compress image if its size larger maxBytesLength
    
    CGFloat compression = 0.9f;
    CGFloat maxCompression = 0.1f;
    
    NSData *imageData;
    if ([extension isEqualToString: kJpegPhotoExt]) {
        imageData = UIImageJPEGRepresentation(newImage, compression);
        
        while ([imageData length] > maxBytesLength && compression > maxCompression)
        {
            compression -= 0.1;
            imageData = UIImageJPEGRepresentation(newImage, compression);
        }
    }
    else if([extension isEqualToString:kPngPhotoExt])
    {
        imageData = UIImagePNGRepresentation(newImage);
        
        if (imageData.length > maxBytesLength) {
            newExtenstion = kJpegPhotoExt;            
            imageData = UIImageJPEGRepresentation(newImage, compression);
            
            while ([imageData length] > maxBytesLength && compression > maxCompression)
            {
                compression -= 0.1;
                imageData = UIImageJPEGRepresentation(newImage, compression);
            }
        }
    }
    
    int imageSize = (int) imageData.length;
    NSLog(@"SIZE OF IMAGE: %i ", imageSize);
    
    PhotoResizedInfo *result = [[PhotoResizedInfo alloc]init];
    result.extension = newExtenstion;
    result.image = [UIImage imageWithData:imageData];
    result.imageData = imageData;
    
    return result;
}

@end
