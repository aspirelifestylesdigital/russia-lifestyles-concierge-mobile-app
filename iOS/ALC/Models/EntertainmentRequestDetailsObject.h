//
//  EntertainmentRequestDetailsObject.h
//  ALC
//
//  Created by Anh Tran on 12/7/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "BaseBookingDetailsObject.h"

@interface EntertainmentRequestDetailsObject : BaseBookingDetailsObject
@property (strong, nonatomic) NSString* eventName;
@property (strong, nonatomic) NSArray* eventCategory;
@property (strong, nonatomic) NSDate* preferDate;
@property (strong, nonatomic) NSString* noOfTickets;


-(id) initFromDict:(NSDictionary*) dict;

@end