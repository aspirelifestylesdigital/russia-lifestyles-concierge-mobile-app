//
//  HotelRequestDetailsObject.h
//  ALC
//
//  Created by Anh Tran on 10/7/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "BaseBookingDetailsObject.h"

@interface HotelRequestDetailsObject : BaseBookingDetailsObject
@property (strong, nonatomic) NSString* hotelName;
@property ( nonatomic) NSInteger startRatingValue;

@property (strong, nonatomic) NSString* startRatingKey;
@property (strong, nonatomic) NSString* minimumPrice;
@property (strong, nonatomic) NSString* maximumPrice;
@property (strong, nonatomic) NSString* anyRequest;

-(id) initFromDict:(NSDictionary*) dict;
@end
