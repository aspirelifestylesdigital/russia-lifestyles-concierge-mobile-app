//
//  DCRPrivilegeItemView.h
//  ALC
//
//  Created by Anh Tran on 3/29/17.
//  Copyright © 2017 Sunrise Software Solutions. All rights reserved.
//

#import "DCRSubItemWithIconView.h"

@interface DCRPrivilegeItemView : UIView
@property (strong, nonatomic) IBOutlet UIButton *btReadmore;
@property (strong, nonatomic) IBOutlet UILabel *lbTite;
@property (strong, nonatomic) IBOutlet UILabel *lbValue;
@property (strong, nonatomic) IBOutlet UIImageView *icon;
@property (strong, nonatomic) IBOutlet UIView *view;
@property (strong, nonatomic) IBOutlet UIView *viewReadMore;

-(void) showView:(NSString*)iconName withTitle:(NSString*) title value:(NSString*)value action:(void(^)())clicked;
@end
