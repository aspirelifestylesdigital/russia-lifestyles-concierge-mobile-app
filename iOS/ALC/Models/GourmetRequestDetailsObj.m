//
//  GourmetRequestDetailsObj.m
//  ALC
//
//  Created by Hai NguyenV on 10/7/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "GourmetRequestDetailsObj.h"

@implementation GourmetRequestDetailsObj

-(id) initFromDict:(NSDictionary*) dict{
    self = [super initFromDict:dict];
    _restaurantName = [self.requestDetails stringForKey:RESTAURANT_NAME_REQUIREMENT];
    _reservationsTime = self.reservationDateTime;
    _reservationName = self.guestName;
    _reservationDate = self.reservationDateTime;
    
    NSString* tempValue =[self.requestDetails stringForKey:OCCASION_REQUIREMENT];
    if([tempValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0){
        _occasion = convertStringToArray(tempValue);
    } else {
        _occasion = [NSArray array];
    }
    _foodAllergies = [self.requestDetails stringForKey:FoodAllergies];
    return self;
}


-(NSString*) privilegeName{
    return _restaurantName;
}
@end
