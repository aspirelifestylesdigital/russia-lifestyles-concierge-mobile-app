//
//  WSBase.h
//  MobileMap
//
//  Created by Huy Tran on 5/10/12.
//  Copyright (c) 2012 S3Corp. All rights reserved.
//
@class BaseResponseObject;
#import "HGWWebService.h"
#import "Common.h"
#import "SBJson.h"
#import "Constant.h"

@protocol DataLoadDelegate;


@interface WSBase : NSObject<WebServiceDelegate>
{
    enum WSTASK task;
    enum WS_SUB_TASK subTask;
    HGWWebService* service;
    id<DataLoadDelegate> __unsafe_unretained delegate;
    SBJsonParser *jsonParser;
}
@property (atomic, strong)  NSURLSessionDataTask *servicetask;
@property (unsafe_unretained) id<DataLoadDelegate>delegate;
@property (nonatomic, assign) enum WSTASK task;
@property (nonatomic, assign) enum WS_SUB_TASK subTask;
//@property (strong, nonatomic) SuccessObj* successObj;

+(BOOL)authenticatedHGWAPI;

-(void)authenticateHGWAPI:(enum WSTASK)aTask;
-(BOOL)parseHGWAuthentication:(NSDictionary*)dict;
-(void)startAPIAfterAuthenticate;
-(void)checkAuthenticate;
-(void) processDataResults:(NSDictionary*)jsonResult forTask:(NSInteger)task forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat;
-(void)processDataResultWithError:(NSError *)errorCode;

-(void)cancelRequest;

-(void)POST:(NSString*) url withParams:(NSMutableDictionary*)params;
-(void)GET:(NSString*) url withParams:(NSMutableDictionary*)params;

@end

@protocol DataLoadDelegate <NSObject>


//@optional
-(void)loadDataDoneFrom:(WSBase*)ws;
-(void)loadDataFailFrom:(BaseResponseObject*)result withErrorCode:(NSInteger)errorCode;
@optional
-(void)loadDataFailFrom:(WSBase *)ws withErrorCode:(NSInteger)errorCode errorMessage:(NSString *)message;

@end
