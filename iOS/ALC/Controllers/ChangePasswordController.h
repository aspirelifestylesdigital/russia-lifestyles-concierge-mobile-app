//
//  ChangePasswordController.h
//  ALC
//
//  Created by Anh Tran on 9/26/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "MainViewController.h"
#import "WSChangePassword.h"
@interface ChangePasswordController : MainViewController<DataLoadDelegate, UITextFieldDelegate>

@property(nonatomic,assign)BOOL isForceChange;
@end
