//
//  RequestFormBaseController.h
//  ALC
//
//  Created by Anh Tran on 3/16/17.
//  Copyright © 2017 Sunrise Software Solutions. All rights reserved.
//

#import "MainViewController.h"
#import "OptionalDetailsView.h"
#import "SubFilterController.h"
#import "DCRItemDetails.h"
#import "BaseBookingRequestObject.h"

#define NOT_AVAILABLE_LOCATION_VALUE @"Not Available"

@interface RequestFormBaseController : MainViewController<UITextFieldDelegate,OptionalDetailsViewDelegate>
@property (strong, nonatomic) NSMutableDictionary* dictIconErrorManager;
@property (strong, nonatomic) NSString* selectedCountry;
@property (strong, nonatomic) NSString* selectedCity;
@property (strong, nonatomic) NSString* selectedState;
@property (strong, nonatomic) NSString* privilegeId;
@property (strong, nonatomic) NSString* privilegeFullAddress;

@property (strong, nonatomic) DCRItemDetails* privilegeDetails;

@property (nonatomic) BOOL hideTopNavigator;

// Implement in Sub controller
- (UIStackView*) parentStack;
- (void) hideTopHeader;
- (void)keyboardWillShow;
- (void)keyboardWillHide;
- (void) closeAllKeyboardAndPicker;
- (void) updateScrollViewToSelectedView:(UIView*) selectedView;
- (void) hideAndFillPrivilegeToForm;
- (void) setUpSubView;
- (void) getRequestDetails;
// end


- (UIToolbar*) setUpToolBar;
- (NSString*)getTimebyDate:(NSDate*)pdate;

- (void) openFilterCountry:(id<SubFilterControllerDelegate>)filterDelegate;
- (void) openFilterState:(id<SubFilterControllerDelegate>)filterDelegate;
- (BOOL) isSpecialCountries;

- (void) openThankYouPage;

- (BaseBookingRequestObject*) addPrivilegeData:(BaseBookingRequestObject*) object;
- (void) showBaseBookingDetails:(BaseBookingDetailsObject*) object;
- (void) showBookingDetails:(BaseBookingDetailsObject*) details;
@end
