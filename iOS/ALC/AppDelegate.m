//
//  AppDelegate.m
//  ALC
//
//  Created by Hai NguyenV on 8/12/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "AppDelegate.h"
#import "StoreUserData.h"
#import "Constant.h"
#import <Google/Analytics.h>

static NSString *const kTrackingId = @"UA-89364046-3";
static NSString *const kAllowTracking = @"allowTracking";
@interface AppDelegate (){
    UserObject* loggedInUser;

}

@end

@implementation AppDelegate
@synthesize AuthToken,AuthExpDate, locationManager, currentGeo;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    // Configure tracker from GoogleService-Info.plist.
    NSError *configureError;
    [[GGLContext sharedInstance] configureWithError:&configureError];
    
    // Optional: configure GAI options.
    GAI *gai = [GAI sharedInstance];
    gai.trackUncaughtExceptions = YES;  // report uncaught exceptions
    //gai.logger.logLevel = kGAILogLevelVerbose;  // remove before app release
    

    // Override point for customization after application launch.
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    application.applicationSupportsShakeToEdit = YES;
    
    self.window.backgroundColor = [UIColor colorWithRed:(34.0/255.0f) green:(34.0/255.0f) blue:(34.0/255.0f) alpha:1.0];
    
   // [[self locationManager] startUpdatingLocation];
    
    if(!isNetworkAvailable())
    {
        showAlertOneButton([SlideNavigationController sharedInstance],ERROR_ALERT_TITLE, NSLocalizedString(@"There is no internet connection.", nil), NSLocalizedString(@"OK", nil));
    }
    
    return YES;
}


-(void)application:(UIApplication *)application didChangeStatusBarFrame:(CGRect)oldStatusBarFrame{
    
    NSLog(@" count windown %@",[UIApplication sharedApplication].windows);
    if ([UIApplication sharedApplication].statusBarFrame.size.height == 20) {
        //CGRect rect = [[UIScreen mainScreen] bounds];
        
        //[[UIApplication sharedApplication].keyWindow setFrame:CGRectMake(0, 20, rect.size.width, rect.size.height)];
      //  [[SlideNavigationController sharedInstance].view setFrame:CGRectMake(0, 20, rect.size.width, rect.size.height)];
        [[UIApplication sharedApplication].keyWindow setNeedsLayout];
    }else{
        CGRect rect = [[UIScreen mainScreen] bounds];
        
        //[[UIApplication sharedApplication].keyWindow setFrame:CGRectMake(0, 40, rect.size.width, rect.size.height-20)];
        [[SlideNavigationController sharedInstance].view setFrame:CGRectMake(0, 20, rect.size.width, rect.size.height)];
        [[UIApplication sharedApplication].keyWindow setNeedsLayout];
    }
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (UIStoryboard *)grabStoryboard {
    
    UIStoryboard *storyboard;
    
    // detect the height of our screen
    int width = [UIScreen mainScreen].bounds.size.width;
    
    if (width <= 320) {
        storyboard = [UIStoryboard storyboardWithName:@"MainSmall" bundle:nil];
        // NSLog(@"Device has a 3.5inch Display.");
    } else {
        storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        // NSLog(@"Device has a 4inch Display.");
    }
    
    return storyboard;
}

#pragma Location
- (CLLocationManager *)locationManager {
    
    if (locationManager != nil) {
        return locationManager;
    }
    
    locationManager = [[CLLocationManager alloc] init];
    
    [locationManager setDistanceFilter:100.0f];    
    [locationManager setDesiredAccuracy:kCLLocationAccuracyNearestTenMeters];
    [locationManager setDelegate:self];
    
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    
    return locationManager;
}

- (CLLocation*) requestUserCurrentLocation{
    [locationManager startUpdatingLocation];
    CLLocation *currentLocation = [locationManager location];
    return currentLocation;
}

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation {
    CLLocationCoordinate2D newCoordinate = [newLocation coordinate];
    if ( (newCoordinate.latitude == currentGeo.latitude) && (newCoordinate.longitude == currentGeo.longitude)) {
        // same coordinate, do nothing
    } else {
        currentGeo = [newLocation coordinate];
    }
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error {
    if ([error code] == kCLErrorDenied )
    {
        if(self.isCheckedLocation == false)
        {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@""
                                                                           message:NSLocalizedString(@"Allow Concierge to access your location while you use the app?", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"Ok"
                                                                  style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                      
                                                                      
                                                                  }];
            [alert addAction:firstAction];
            
            [[[self window] rootViewController] presentViewController:alert animated:YES completion:nil];
            
        }
    }
}


- (void)findCurrentCountryFromLocation:(CLLocation*)position
                     completionHandler:(void(^)(CLPlacemark *placemark, NSError *error))completionHandler
{
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:position completionHandler:^(NSArray *placemarks, NSError *error) {
        if (error) {
            NSLog(@"findCurrentCountryFromLocation error: %@", error.description);
            if (completionHandler) {
                completionHandler(nil, error);
            }
        }
        else {
            CLPlacemark *placemark = [placemarks lastObject];
            if ([placemark isKindOfClass:[NSNull class]]) {
                placemark = nil;
            }
            
            NSLog(@"findCurrentCountryFromLocation result: %@", placemark.country);
            if (completionHandler) {
                completionHandler(placemark, nil);
            }
        }
    }];
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if (status == kCLAuthorizationStatusAuthorizedAlways ||
        status == kCLAuthorizationStatusAuthorizedWhenInUse ) {
        NSLog(@"Location service status kCLAuthorizationStatusAuthorizedWhenInUse | kCLAuthorizationStatusAuthorizedAlways | kCLAuthorizationStatusAuthorized");
        
        //[[NSNotificationCenter defaultCenter] postNotificationName:LocationServiceAllowed
        //                                                    object:nil];
    }
    else if (status == kCLAuthorizationStatusDenied) {
        NSLog(@"Location service status kCLAuthorizationStatusDenied");
        
        //[[NSNotificationCenter defaultCenter] postNotificationName:LocationServiceDenied
        //                                                    object:nil];
    }
    else if (status == kCLAuthorizationStatusRestricted) {
        NSLog(@"Location service status kCLAuthorizationStatusRestricted");
        
        //[[NSNotificationCenter defaultCenter] postNotificationName:LocationServiceDenied
        //                                                    object:nil];
    }
    else if (status == kCLAuthorizationStatusNotDetermined) {
        NSLog(@"Location service status kCLAuthorizationStatusNotDetermined");
    }
    else {
        NSLog(@"Location service status others");
    }
}


#pragma  Token

- (NSString*) getUserEmail{
    return userAuthenticate;
}

- (NSString*) getUserPassword{
    return passAuthenticate;
}

- (BOOL) isLoggedIn{
    if(loggedInUser==nil || loggedInUser.userId==nil || loggedInUser.userId.length>0){
        loggedInUser = getUser();
    }
    
    return [loggedInUser isLoggedIn];
}

-(void) storeToken:(NSString*) token{
    AuthToken = token;
}

-(NSString*) getToken{
    return AuthToken;
}

-(void) logoutUser{
    loggedInUser = nil;
    clearUser();
    
}

-(UserObject*) getLoggedInUser{
    if(loggedInUser==nil || loggedInUser.userId==nil || loggedInUser.userId.length>0){
        loggedInUser = getUser();
    }
    
    return loggedInUser;
}

-(void) saveAccessToken:(NSString*) accesToken refreshToken:(NSString*) refreshToken expired:(NSString*) expiredTime{
    _ACCESS_TOKEN = accesToken;
    _REFRESH_TOKEN = refreshToken;
    if(expiredTime!=nil && expiredTime.length > 0){
        _B2C_ExpiredAt = [[NSDate date] dateByAddingTimeInterval:[expiredTime integerValue]];
    }
}

@end
