//
//  MyRequestObject.h
//  ALC
//
//  Created by Anh Tran on 8/30/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

@interface MyRequestObject : NSObject
@property (strong, nonatomic) NSString* requestId;
@property (strong, nonatomic) NSString* bookingItemId;
@property (strong, nonatomic) NSString* bookingTypeGroup;
@property (strong, nonatomic) NSString* bookingType;
@property (strong, nonatomic) NSString* functionality;
@property (nonatomic) BOOL isDeleted;
@property (strong, nonatomic) NSString* requestStatus;
@property (strong, nonatomic) NSString* requestMode;
@property (strong, nonatomic) NSString* itemTitle;
@property (nonatomic) BOOL notDisplayEditCancel;
@property (nonatomic) BOOL isHistory;
@property (strong, nonatomic) NSString* address;
@property (nonatomic) enum REQUEST_TYPE parsedRequestType;
@property (strong, nonatomic) NSDate *createDate;
@property (strong, nonatomic) NSDictionary *requestDetails;
@property (strong, nonatomic) NSString* city;
@property (strong, nonatomic) NSString* country;
@property (strong, nonatomic) NSString* stateValue;

@property (strong, nonatomic) NSDate* checkInDate;
@property (strong, nonatomic) NSDate* checkOutDate;
@property (strong, nonatomic) NSDate* reservationDateTime;
@property (strong, nonatomic) NSDate* eventDateTime;
@property (strong, nonatomic) NSDate* pickUpDateTime;
@property (strong, nonatomic) NSDate* dropOffDateTime;
@property (strong, nonatomic) NSDate* golfCourseDateTime;

@property (strong, nonatomic) NSDate* happenDateTime;

@property (strong, nonatomic) NSString* fullDescription;

-(id) initFromDict:(NSDictionary*) dict;
-(BOOL) notAllowEditCancel;
@end
