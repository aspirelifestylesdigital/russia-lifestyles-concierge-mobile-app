//
//  ForgotPasswordController.h
//  ALC
//
//  Created by Hai NguyenV on 8/17/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainViewController.h"
#import "WSForgotPassword.h"
#import "ErrorToolTip.h"

@interface ForgotPasswordController : MainViewController<DataLoadDelegate,ErrorToolTipDelegate>
{
    NSMutableDictionary* dictIconErrorManager;
}
@property (strong, nonatomic) IBOutlet ErrorToolTip *emailErrorToolTip;

@end
