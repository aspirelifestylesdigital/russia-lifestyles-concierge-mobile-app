package com.aspire.android.russia.apiservices.b2c.RequestModel;

import com.aspire.android.russia.BuildConfig;
import com.aspire.android.russia.apiservices.RequestModel.BaseRequest;
import com.aspire.android.russia.application.AppConstant;
import com.aspire.android.russia.utils.SharedPreferencesUtils;
import com.google.gson.annotations.Expose;

/**
 * Created by ThuNguyen on 10/26/2016.
 */

public class B2CGetConciergeRequestDetailRequest extends BaseRequest{
    @Expose
    private String AccessToken;
    @Expose
    private String ConsumerKey;
    @Expose
    private String EPCCaseID;
    @Expose
    private String Functionality;
    @Expose
    private String OnlineMemberId;
    @Expose
    private String TransactionID;

    public B2CGetConciergeRequestDetailRequest(){
        AccessToken = SharedPreferencesUtils.getPreferences(AppConstant.PRE_B2C_ACCESS_TOKEN, "");
        ConsumerKey = BuildConfig.B2C_CONSUMER_KEY;
        Functionality = "GetRecentRequests";
        OnlineMemberId = SharedPreferencesUtils.getPreferences(AppConstant.PRE_B2C_ONLINE_MEMBER_ID, "");
    }
    public B2CGetConciergeRequestDetailRequest build(String caseId, String transactionID){
        this.EPCCaseID = caseId;
        this.TransactionID = transactionID;
        return this;
    }

    public void setAccessToken(String accessToken) {
        AccessToken = accessToken;
    }
}
