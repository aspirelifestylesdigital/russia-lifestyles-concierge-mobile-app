//
//  GourmetPreferenceController.h
//  ALC
//
//  Created by HaiNguyen on 9/11/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SubFilterController.h"
#import "CustomTextField.h"
#import "WSCreateGourmetPre.h"
#import "WSGetMyPreferences.h"

@interface GourmetPreferenceController : MainViewController<SubFilterControllerDelegate,UITextFieldDelegate,DataLoadDelegate,UITextViewDelegate>
{
    NSMutableArray *selectedCuisines;
    NSMutableArray *selectedCuisinesKey;
    WSCreateGourmetPre* wsCreateGourmet;
    WSGetMyPreferences* wsGetMyPreference;
}
@property (weak, nonatomic) IBOutlet UIScrollView *pScrollView;
@property (strong, nonatomic)  IBOutlet UIButton *btCuisine;
@property (weak, nonatomic) IBOutlet CustomTextField *tfCuisine;
@property (nonatomic, strong)NSString* preferenceID;
@property (weak, nonatomic) IBOutlet UITextView *tfOrtherFood;
@property (weak, nonatomic) IBOutlet UILabel *lbHolder;
@property (weak, nonatomic) IBOutlet UILabel *diningTitleLbl;
@property (weak, nonatomic) IBOutlet UILabel *cuisinePreferLbl;
@property (weak, nonatomic) IBOutlet UILabel *otherCuisineLbl;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;

- (IBAction)actionSave:(id)sender;
- (IBAction)actionCancel:(id)sender;
- (IBAction)selectCuisine:(id)sender;

@end
