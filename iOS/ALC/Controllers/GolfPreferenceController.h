//
//  GolfPreferenceController.h
//  ALC
//
//  Created by HaiNguyen on 9/11/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SubFilterController.h"
#import "CustomTextField.h"
#import "WSCreateGoltPre.h"
#import "WSGetMyPreferences.h"

@interface GolfPreferenceController : MainViewController<SubFilterControllerDelegate,UITextFieldDelegate,DataLoadDelegate>
{
    NSMutableArray *selectedTee;
    NSMutableArray *selectedTeeKey;
    NSInteger ballersNumber;
    WSCreateGoltPre* wsCreateGolf;
    WSGetMyPreferences* wsGetMyPreference;
}
@property (weak, nonatomic) IBOutlet CustomTextField *tfCourseName;
@property (weak, nonatomic) IBOutlet UIButton *btnTee;
@property (nonatomic, strong)NSString* preferenceID;
@property (weak, nonatomic) IBOutlet UITextField *tfHandicap;
@property (weak, nonatomic) IBOutlet UILabel *golfTitleLbl;
@property (weak, nonatomic) IBOutlet UILabel *golfCourseLbl;
@property (weak, nonatomic) IBOutlet UILabel *teetimesLbl;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;

- (IBAction)actionTee:(id)sender;
- (IBAction)actionSave:(id)sender;
- (IBAction)actionCancel:(id)sender;
@end
