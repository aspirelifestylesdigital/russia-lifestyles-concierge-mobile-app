package com.aspire.android.russia.fragment;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.aspire.android.russia.R;
import com.aspire.android.russia.application.coreactivitys.BaseFragment;
import com.aspire.android.russia.utils.CommonUtils;
import com.aspire.android.russia.utils.FontUtils;
import com.aspire.android.russia.utils.IntentUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by anh.trinh on 9/13/2016.
 */
public class StaticWebFragment
        extends BaseFragment {

    public static final String PRE_TITLE_WEB = "title_web";
    public static final String PRE_LINK_WEB = "link_web";
    @BindView(R.id.ic_back)
    ImageView mIcBack;
    @BindView(R.id.tv_tool_bar_title)
    TextView mTvToolBarTitle;
    @BindView(R.id.webview)
    WebView mWebview;
    String title="";
    String path ="";


    @Override
    protected int layoutId() {
        return R.layout.fragment_static_web;
    }

    @Override
    protected void initView() {
        title= getArguments().getString(PRE_TITLE_WEB,
                                                       "");
        path= getArguments().getString(PRE_LINK_WEB,
                                        "");
        mTvToolBarTitle.setFilters(new InputFilter[]{ new InputFilter.LengthFilter(40) });
        mTvToolBarTitle.setText(title);
        CommonUtils.setFontForViewRecursive(mTvToolBarTitle,
                                            FontUtils.FONT_FILE_NAME_AVENIR_NEXT_DEMI_BOLD, Typeface.BOLD);

    }

    @Override
    protected void bindData() {
        mWebview.getSettings().setJavaScriptEnabled(true);
        mWebview.loadUrl(path);
        mWebview.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(final WebView view,
                                                    final String url) {
                if (url.startsWith("mailto:")) {
                    String mail = url.replace("mailto:",
                                              "");
                    IntentUtil intent = IntentUtil.getInstance(getActivity());
                    intent.sendMail(mail);
                    return true;
                } else {
                    return super.shouldOverrideUrlLoading(view,
                                                          url);
                }
            }

            @TargetApi(21)
            @Override

            public boolean shouldOverrideUrlLoading(final WebView view,
                                                    final WebResourceRequest request) {
                if (request.getUrl()
                           .getPath()
                           .toString()
                           .startsWith("mailto:")) {
                    String mail = request.getUrl()
                                         .getPath()
                                         .replace("mailto:",
                                                  "");
                    IntentUtil intent = IntentUtil.getInstance(getActivity());
                    intent.sendMail(mail);
                    return true;
                } else {
                    return super.shouldOverrideUrlLoading(view,
                                                          request);
                }
            }
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                showDialogProgress();
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                hideDialogProgress();
                super.onPageFinished(view, url);
            }
        });
    }

    @Override
    public void onResume() {

        super.onResume();
    }

    @Override
    public boolean onBack() {
        return false;
    }


    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater,
                                           container,
                                           savedInstanceState);
        ButterKnife.bind(this,
                         rootView);
        return rootView;
    }

    @OnClick(R.id.ic_back)
    public void onClick() {
        onBackPress();
    }
}
