package com.aspire.android.russia.apiservices.ResponseModel;

import com.aspire.android.russia.apiservices.apis.appimplement.BaseResponse;
import com.aspire.android.russia.model.concierge.OtherBookingDetailData;

/**
 * Created by ThuNguyen on 10/26/2016.
 */

public class OtherBookingDetailResponse extends BaseResponse {
    private OtherBookingDetailData Data;

    public OtherBookingDetailData getData() {
        return Data;
    }

    public void setData(OtherBookingDetailData data) {
        Data = data;
    }
}
