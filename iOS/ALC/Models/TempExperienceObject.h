//
//  TempExperienceObject.h
//  ALC
//
//  Created by Anh Tran on 9/12/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TempExperienceObject : NSObject
@property (strong, nonatomic) NSString* itemID;
@property (strong, nonatomic) NSString* itemName;
@property (strong, nonatomic) NSString* itemDescription;
@property (strong, nonatomic) NSString* siteURL;
@property (strong, nonatomic) NSString* logoSource;
@property (strong, nonatomic) NSString* backgroundImg;
@property ( nonatomic) Boolean isInterested;

-(id) initFromDict:(NSDictionary*) dict;
//-(NSString*) getDistance:(CLLocation*) currentLocation;
@end