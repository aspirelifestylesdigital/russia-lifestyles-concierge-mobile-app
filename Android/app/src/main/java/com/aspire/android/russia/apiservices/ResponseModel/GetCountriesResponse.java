package com.aspire.android.russia.apiservices.ResponseModel;

import com.aspire.android.russia.apiservices.apis.appimplement.BaseResponse;
import com.aspire.android.russia.model.CountryObject;

import java.util.List;


public class GetCountriesResponse
        extends BaseResponse {
    private List<CountryObject> Data;

    public List<CountryObject> getData() {
        return Data;
    }

    public void setData(List<CountryObject> data) {
        this.Data = data;
    }
}
