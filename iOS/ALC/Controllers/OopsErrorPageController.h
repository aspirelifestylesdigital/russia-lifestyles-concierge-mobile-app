//
//  OopsErrorPageController.h
//  ALC
//
//  Created by Hai NguyenV on 11/10/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "MainViewController.h"

@interface OopsErrorPageController : MainViewController

@property(nonatomic, strong)NSString* contentError;
@property (strong, nonatomic) IBOutlet UILabel *lbErrorMsg;
- (IBAction)actionOK:(id)sender;
@end
