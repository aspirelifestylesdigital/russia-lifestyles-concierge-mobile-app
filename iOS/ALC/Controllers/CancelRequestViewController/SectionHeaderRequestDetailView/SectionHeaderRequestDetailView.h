//
//  SectionHeaderRequestDetailView.h
//  ALC
//
//  Created by Tho Nguyen on 10/17/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString *const kNotificationToOpenRequestDetailCell;

@interface SectionHeaderRequestDetailView : UIView

@property (nonatomic) BOOL isRotate;

- (void)setTitleForHeaderView:(NSInteger)requestType;

@end
