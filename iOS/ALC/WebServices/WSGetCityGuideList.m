//
//  WSGetCityGuideList.m
//  ALC
//
//  Created by Anh Tran on 9/14/16.
//  Copyright © 2016 Sunrise Software Solutions. All rights reserved.
//

#import "WSGetCityGuideList.h"
#import "HGWGetBaseRequest.h"
#import "BaseResponseObject.h"
#import "CityGuideObject.h"
#import "UserObject.h"


@interface WSGetCityGuideList(){
    NSInteger total;
    NSInteger pageIndex;
    NSInteger pageSize;
    NSMutableDictionary *options;
}

@end

@implementation WSGetCityGuideList
-(id)initWithPageSize:(NSInteger)size{
    self = [super init];
    pageSize = size;
    pageIndex = 1;
    return self;
}

-(id)init{
    self = [super init];
    pageSize = 500;
    pageIndex = 1;
    return self;
}

-(void)getCityGuideList{
    [self checkAuthenticate];
}

-(void) nextPage{
    if(pageIndex*pageSize < total){
        pageIndex++;
        [self checkAuthenticate];
    }
}

-(void)startAPIAfterAuthenticate{
    task = WS_GET_CITY_GUIDE_LIST;
    subTask = WS_ST_NONE;
    
    HGWGetBaseRequest *request = [[HGWGetBaseRequest alloc]init];
    request.dictKeyValues = [self buildRequestParams];
    
    request.requestUrl = [API_URL stringByAppendingString:GetCityGuideList];
    
    [service invokeMethod:kHTTPGET andObject:request forTask:task forSubTask:subTask];
    
}
-(void)processDataResults:(NSDictionary *)jsonResult forTask:(NSInteger)task forSubTask:(NSInteger)subT returnFormat:(NSInteger)returnFormat{
    if(jsonResult){
        BaseResponseObject *response = [[BaseResponseObject alloc] initFromDict:jsonResult];
        response.task = self.task;
        if(self.delegate != nil && [response isSuccess])
        {
            NSDictionary* data = [jsonResult dictionaryForKey:@"Data"];
            total = [data integerForKey:@"TotalRecords"];
            NSArray* rests = [data arrayForKey:@"ListData"];
            self.itemDatas = [NSMutableArray array];
            for (int i = 0; i < rests.count; i++) {
                [self.itemDatas addObject:[[CityGuideObject alloc] initFromDict:[rests objectAtIndex:i]]];
            }
            [self.delegate loadDataDoneFrom:self];
        } else if(self.delegate != nil){
            [self.delegate loadDataFailFrom:response withErrorCode:response.status];
        }
    }
    else if(self.delegate != nil)
    {
        [self.delegate loadDataFailFrom:nil withErrorCode:returnFormat];
    }
    
}
-(void)processDataResultWithError:(NSError *)error
{
    [delegate loadDataFailFrom:nil withErrorCode:error.code];
}

-(NSMutableDictionary*) buildRequestParams{
    NSMutableDictionary *dictKeyValues = [[NSMutableDictionary alloc]init];
  
    if(_country!=nil){
        [dictKeyValues setObject:_country forKey:@"Country"];
    }
    
    if(_region!=nil){
        [dictKeyValues setObject:_country forKey:@"Region"];
    }
    
    [dictKeyValues setObject:[NSNumber numberWithInteger:pageIndex] forKey:@"Page"];
    [dictKeyValues setObject:[NSNumber numberWithInteger:pageSize] forKey:@"RecordPerPage"];
    
    AppDelegate* appdele=(AppDelegate*) [[UIApplication sharedApplication] delegate];
    if([appdele isLoggedIn]){
        [dictKeyValues setObject:((UserObject*)[appdele getLoggedInUser]).userId  forKey:@"UserID"];
    }
    
    return dictKeyValues;
}
@end
